import React, { useEffect } from 'react';
import { DotIndicator } from 'react-native-indicators';
import { theme } from '../theme/themeConfig';
import Fade from '../theme/Fade';
import { useOwnedUnitsMeters } from '../Meters/api';
import { QUERY_STATUS } from '../reactQuery/queryClientConfig';

function SplashSyncStatus({ onSyncCompleted }) {
	// list of heavy hooks that should make a cache of splash
	const { status: ownedUnitMeters } = useOwnedUnitsMeters();

	useEffect(() => {
		!!onSyncCompleted &&
			onSyncCompleted(ownedUnitMeters === QUERY_STATUS.success);
	}, [onSyncCompleted, ownedUnitMeters]);
	return (
		<Fade
			height={50}
			position="absolute"
			style={{
				bottom: theme.dimensions.screenHeight / 2.5,
				left: 0,
				right: 0,
			}}>
			<DotIndicator
				size={8}
				count={4}
				color={theme.paletteAuto.background.primary}
			/>
		</Fade>
	);
}

export default SplashSyncStatus;
