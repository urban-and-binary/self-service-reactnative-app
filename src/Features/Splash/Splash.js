import React, { useCallback, useEffect, useState } from 'react';
import { StyleSheet } from 'react-native';
import { inject, observer } from 'mobx-react';
import { Screen, View, Logo, LogoText } from '../theme';
import { theme } from '../theme/themeConfig';
import { useSession } from './api';
import Fade from '../theme/Fade';
import SplashSyncStatus from './SplashSyncStatus';

const SplashScreen = inject('store')(
	observer(({ store: { auth, assign, isLoaded } }) => {
		const { refetch, data } = useSession(!!auth?.token);
		const [isSynced, setIsSynced] = useState(false);

		// eslint-disable-next-line react-hooks/exhaustive-deps
		const handleSyncApp = useCallback(
			sessionResponse => {
				assign({
					isLoaded: true,
					auth: { ...auth, ...sessionResponse },
				});
			},
			[assign, auth],
		);

		const handleSetIsSynced = () => {
			setIsSynced(true);
		};

		const handleLoadApp = useCallback(() => {
			assign({
				isLoaded: true,
			});
		}, [assign]);

		useEffect(() => {
			if (isSynced) {
				handleSyncApp(data);
			}
		}, [data, handleSyncApp, isSynced]);

		useEffect(() => {
			const persistanceTimeout = setTimeout(() => {
				if (auth?.token && !isLoaded) {
					refetch();
				} else {
					handleLoadApp();
				}
			}, 1000); // temporary timeout for simulation splash and for loading persist

			return () => clearTimeout(persistanceTimeout);
		}, [assign, auth, handleLoadApp, isLoaded, refetch]);
		return (
			<Screen
				translucent
				barColor="transparent"
				barStyle="light-content"
				unsafe>
				<View
					gradientColors={[
						theme.paletteAuto.gradient.start,
						theme.paletteAuto.gradient.middle,
						theme.paletteAuto.gradient.end,
					]}
					flex
					center>
					<Fade center delay={0}>
						<Logo />
						<LogoText style={defautlStyles.logoText} />
					</Fade>
				</View>
				{data?.email && (
					<SplashSyncStatus onSyncCompleted={handleSetIsSynced} />
				)}
			</Screen>
		);
	}),
);

const defautlStyles = StyleSheet.create({
	logoText: {
		marginTop: 10,
	},
});

export default SplashScreen;
