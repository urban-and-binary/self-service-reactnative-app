import { useQuery } from '../reactQuery/hooks';

export const useSession = (shouldCall, option = {}) =>
	useQuery(!!shouldCall && 'useSession', 'session', option);
