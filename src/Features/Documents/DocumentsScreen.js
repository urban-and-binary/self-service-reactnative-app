import React from 'react';
import { Screen, Text } from '../theme';
import { theme } from '../theme/themeConfig';
import DocumentsItem from './DocumentsItem';
import { documentList } from './documentsFakeData';

function DocumentsScreen() {
	const renderTask = (item, index) => (
		<DocumentsItem key={index} document={item} />
	);
	return (
		<Screen
			preset="scroll"
			translucent
			barStyle="dark-content"
			barColor="transparent"
			paddingHorizontal={theme.pads.horizontalPad}
			topBar>
			<Text variant={theme.typo.fontVariants.h1Bold} paddingBottom={20}>
				screens.documents.title
			</Text>
			{documentList.map(renderTask)}
		</Screen>
	);
}
export default DocumentsScreen;
