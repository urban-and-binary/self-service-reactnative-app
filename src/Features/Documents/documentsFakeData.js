export const documentList = [
	{
		title: 'Bendro naudojimo buitinių nuotėkų sistemos apžiūra',
		from: '2022 - 01 - 10',
		to: '2022 - 02 - 30',
		address: 'Aukštaičių g. 18 - 01, Kauno m., Kauno m. sav',
		tag: 'In progress',
		moreItems: [
			{
				label: 'Reason',
				value: 'Privalomieji reikalavimai',
			},
			{
				label: 'Sum with PVM',
				value: 'Trūkusio vamzdžio keitimas	200,00 Eur',
			},
			{
				label: 'Description',
				value:
					'Bendro naudojimo buitinių nuotekų sistemos apžiūra.\n' +
					'Trūkusio vamzdžio pakeitimas.',
			},
		],
	},
	{
		title: 'Bendro naudojimo buitinių nuotėkų sistemos apžiūra',
		from: '2022 - 01 - 10',
		to: '2022 - 02 - 30',
		address: 'Aukštaičių g. 18 - 01, Kauno m., Kauno m. sav',
		tag: 'Planed',
		moreItems: [
			{
				label: 'Reason',
				value: 'Privalomieji reikalavimai',
			},
			{
				label: 'Sum with PVM',
				value: 'Trūkusio vamzdžio keitimas	200,00 Eur',
			},
			{
				label: 'Description',
				value:
					'Bendro naudojimo buitinių nuotekų sistemos apžiūra.\n' +
					'Trūkusio vamzdžio pakeitimas.',
			},
		],
	},
	{
		title: 'Bendro naudojimo buitinių nuotėkų sistemos apžiūra',
		from: '2022 - 01 - 10',
		to: '2022 - 02 - 30',
		address: 'Aukštaičių g. 18 - 01, Kauno m., Kauno m. sav',
		tag: 'Done',
		moreItems: [
			{
				label: 'Reason',
				value: 'Privalomieji reikalavimai',
			},
			{
				label: 'Sum with PVM',
				value: 'Trūkusio vamzdžio keitimas	200,00 Eur',
			},
			{
				label: 'Description',
				value:
					'Bendro naudojimo buitinių nuotekų sistemos apžiūra.\n' +
					'Trūkusio vamzdžio pakeitimas.',
			},
		],
	},
];
