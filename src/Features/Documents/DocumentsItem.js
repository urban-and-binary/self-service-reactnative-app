import React, { useState } from 'react';
import Modal from 'react-native-modal';
import { StyleSheet } from 'react-native';
import Feather from 'react-native-vector-icons/Feather';
import { Button, Card, Text, View } from '../theme';
import { theme } from '../theme/themeConfig';

function DocumentsItem({ document, disabled, ...props }) {
	const [showModal, setShowModal] = useState(false);
	const handleShowModal = () => {
		setShowModal(true);
	};
	const handleHideModal = () => {
		setShowModal(false);
	};
	return (
		<Card
			onPress={!disabled && handleShowModal}
			backgroundColor={
				document?.tag === 'Planed'
					? theme.palette.primaryColors.color5
					: theme.palette.baseColors.base7
			}
			shadow={
				document?.tag === 'Planed'
					? theme.shadows.none
					: theme.shadows.deep
			}
			{...props}>
			<View
				row
				justifyContent="space-between"
				alignItems="flex-start"
				marginBottom={15}>
				<Text variant={theme.typo.fontVariants.smallBold} flex>
					{document?.from}
				</Text>
				<View row>
					<Feather
						color={theme.palette.baseColors.base4}
						size={theme.typo.fontSize.semiSmall}
						name="eye"
					/>
					<Feather
						color={theme.palette.baseColors.base4}
						size={theme.typo.fontSize.semiSmall}
						name="download-cloud"
						style={defaultStyles.endIcon}
					/>
				</View>
			</View>
			<Text variant={theme.typo.fontVariants.smallBold}>
				{document?.title || 'Unknown title'}
			</Text>

			<Text
				paddingTop={20}
				paddingBottom={10}
				variant={theme.typo.fontVariants.smallRegular}>
				{document?.address || 'Unknown address'}
			</Text>
			<DocumentDownloadModal
				document={document}
				visible={showModal}
				onRequestClose={handleHideModal}
			/>
		</Card>
	);
}

function DocumentDownloadModal({
	visible,
	onRequestClose,
	children,
	document,
	...props
}) {
	return (
		<Modal
			onRequestClose={onRequestClose}
			onBackButtonPress={onRequestClose}
			onBackdropPress={onRequestClose}
			onDismiss={onRequestClose}
			isVisible={visible}
			hasBackdrop
			backdropColor="black"
			statusBarTranslucent
			animationInTiming={500}
			animationOutTiming={500}
			backdropTransitionInTiming={750}
			backdropTransitionOutTiming={500}
			useNativeDriverForBackdrop
			style={defaultStyles.view}
			{...props}>
			<View paddingHorizontal={theme.pads.horizontalPad}>
				<DocumentsItem disabled document={document} />
				<Button label="Download" marginVertical={20} />
				<Button label="Overview" />
			</View>
		</Modal>
	);
}

const defaultStyles = StyleSheet.create({
	view: {
		justifyContent: 'center',
		margin: 0,
	},
	endIcon: { marginLeft: 10 },
});

export default DocumentsItem;
