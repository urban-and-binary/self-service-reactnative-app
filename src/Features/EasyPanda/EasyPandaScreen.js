import React from 'react';
import { Screen, Text, View } from '../theme';

export default function EasyPandaScreen() {
	return (
		<Screen>
			<View flex center>
				<Text>Easy Panda Screen</Text>
			</View>
		</Screen>
	);
}
