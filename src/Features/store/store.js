import { types, applySnapshot } from 'mobx-state-tree';
import { createContext, useContext } from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { persist } from 'mst-persist';

import { storage } from './storageHelper';

const { model, string, maybe, optional, boolean, number, array } = types;

const Auth = model('Auth', {
	token: maybe(string),
	email: maybe(string),
	isOnboarded: maybe(boolean),
	verifiedAt: maybe(string),
	permissions: maybe(array(string)),
	units: maybe(array(string)),
	_id: maybe(string),
	country: maybe(string),
	countryCode: maybe(string),
	rulesAgree: maybe(boolean),
	invoiceAgree: maybe(boolean),
	newsAgree: maybe(boolean),
	createdAt: maybe(string),
	updatedAt: maybe(string),
	address: maybe(string),
	name: maybe(string),
	phone: maybe(string),
});

const ToastMessage = {
	message: string,
	duration: maybe(number),
	type: maybe(string),
};

const Root = model('Root', {
	auth: optional(Auth, () => ({
		token: '',
		email: '',
		isOnboarded: false,
		verifiedAt: '',
		permissions: [],
	})),
	isLoaded: optional(boolean, () => false),
	toastMessage: maybe(model('ToastMessage', ToastMessage)),

	locale: optional(string, () => 'en'),
}).actions(self => ({
	set: (key, value) => Object.assign(self, { [key]: value }),
	assign: obj => Object.assign(self, obj),
	setToast: (message, duration = 5, type = 'error') =>
		(self.toastMessage = { message, duration, type }),
	clearToast: () =>
		(self.toastMessage = { message: '', duration: 5, type: 'error' }),
	logout: () =>
		storage.clear().then(() => applySnapshot(self, getInitialStore(self))),

	setToken: token => (self.auth.token = token),
}));

const getInitialStore = store => ({
	...store,
	isLoaded: false,
	toastMessage: {
		message: '',
		duration: 0,
		type: '',
	},
	auth: {
		token: '',
		email: '',
		isOnboarded: false,
		verifiedAt: '',
		permissions: [],
	},
	locale: 'en',
});

const store = Root.create();

persist('store', store, {
	storage: AsyncStorage,
	jsonify: true,
	whitelist: ['auth', 'locale'],
});

export default store;

const RootStoreContext = createContext(null);

export const { Provider } = RootStoreContext;

export const useMst = () => useContext(RootStoreContext);
