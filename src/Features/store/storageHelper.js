import AsyncStorage from '@react-native-async-storage/async-storage';
import { ON_BOARDING_STORAGE_KEY } from '../Onboarding/additionalInfoConfig';

export const storage = {
	set: async (key, value) =>
		await AsyncStorage.setItem(key, value ? String(value) : value),
	get: async key => await AsyncStorage.getItem(key),
	remove: async key => await AsyncStorage.removeItem(key),
	setJson: async (key, value) =>
		await AsyncStorage.setItem(key, JSON.stringify(value)),
	getJson: async key => JSON.parse(await AsyncStorage.getItem(key)),

	clear: async () => {
		const asyncStorageKeys = await AsyncStorage.getAllKeys();
		const keysToRemove = asyncStorageKeys.filter(
			key => key !== ON_BOARDING_STORAGE_KEY,
		);

		if (!keysToRemove.length) {
			return;
		}
		return AsyncStorage.multiRemove(keysToRemove);
	},
};
