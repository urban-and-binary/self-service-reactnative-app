import React from 'react';
import { View } from '../theme';
import { mockPollsData } from './polls.mock';
import PollItem from './PollItem';

function Polls() {
	const renderPollsItem = (item, index) => (
		<PollItem key={index} item={item} index={index} />
	);
	return <View>{mockPollsData.map(renderPollsItem)}</View>;
}

export default Polls;
