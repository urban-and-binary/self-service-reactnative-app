import React from 'react';
import { View } from '../theme';
import { mockVotingsData } from './polls.mock';
import PollItem from './PollItem';

function Votings() {
	const renderVotingItems = (item, index) => (
		<PollItem isVoting key={index} item={item} index={index} />
	);
	return <View>{mockVotingsData.map(renderVotingItems)}</View>;
}

export default Votings;
