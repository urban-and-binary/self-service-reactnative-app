import React, { useState } from 'react';
import { theme } from '../theme/themeConfig';
import {
	Button,
	Card,
	HorizontalLine,
	Options,
	Tag,
	Text,
	View,
} from '../theme';
import { CheckBox } from '../theme/CheckBox';

export function PollItem({ item, index, isVoting }) {
	const [selectedVote, setSelectedVote] = useState([]);
	const [submitedVote, setSubmittedVote] = useState([]); // temporary, for UI purpose
	const [acceptedRules, setAcceptedRules] = useState(false);

	const handleSubmitVote = () => setSubmittedVote(selectedVote);
	return (
		<Card
			marginTop={
				index === 0
					? theme.pads.sectionVerticalPad
					: theme.pads.screenVerticalPad
			}>
			<Text variant={theme.typo.fontVariants.baseBold}>
				{item.question}
			</Text>
			<Text
				paddingTop={5}
				variant={theme.typo.fontVariants.xsRegular}
				color={theme.palette.baseColors.grey}>
				Created date: {item.date}
			</Text>
			<View alignItems="flex-start">
				<Tag marginTop={10} status="success" label="Active" />
			</View>
			<Text
				variant={theme.typo.fontVariants.smallRegular}
				paddingVertical={20}>
				{item.unit}
			</Text>
			<View row>
				<Text variant={theme.typo.fontVariants.smallBold}>Votes: </Text>
				<Text variant={theme.typo.fontVariants.smallRegular}>
					{item.votes}
				</Text>
			</View>
			<Options radio onSelect={setSelectedVote} data={item.options} />
			{isVoting && (
				<View paddingVertical={10} paddingRight={30} marginLeft={-10}>
					<CheckBox
						onChangeValue={setAcceptedRules}
						alignItems="flex-start"
						label={
							<Text
								color={theme.paletteAuto.text.primary}
								variant={theme.typo.fontVariants.smallRegular}>
								{t('screens.polls.conditions')}
							</Text>
						}
					/>
				</View>
			)}
			{submitedVote.length === 0 && (
				<Button
					onPress={handleSubmitVote}
					disabled={
						selectedVote.length === 0 ||
						(isVoting && !acceptedRules)
					}
					marginTop={theme.pads.screenVerticalPad}
					label="screens.polls.vote"
				/>
			)}
			{submitedVote.length > 0 && (
				<>
					<HorizontalLine />
					<Text
						weight={600}
						color={theme.palette.statusColors.success}>
						Your vote has been accepted!
					</Text>
				</>
			)}
		</Card>
	);
}

export default PollItem;
