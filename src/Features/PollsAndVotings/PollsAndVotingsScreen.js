import React, { useState } from 'react';
import { Screen, Text } from '../theme';
import { theme } from '../theme/themeConfig';
import { TabBar } from '../theme/TabBar';
import { pollsAndVotingsTabs } from './pollsAndVotingsConfig';
import Polls from './Polls';
import Votings from './Votings';

function PollsAndVotingsScreen() {
	const [activeTab, setActiveTab] = useState(pollsAndVotingsTabs[0]);
	return (
		<Screen
			preset="scroll"
			topBar
			paddingHorizontal={theme.pads.horizontalPad}>
			<Text
				paddingBottom={theme.pads.sectionVerticalPad}
				variant={theme.typo.fontVariants.h1Bold}>
				screens.polls.title
			</Text>
			<TabBar
				tabs={pollsAndVotingsTabs}
				activeTab={activeTab}
				onChange={setActiveTab}
			/>
			{activeTab === pollsAndVotingsTabs[0] && <Polls />}
			{activeTab === pollsAndVotingsTabs[1] && <Votings />}
		</Screen>
	);
}

export default PollsAndVotingsScreen;
