export const mockPollsData = [
	{
		question: 'Ar pritariate etapinei renovacijai?',
		date: '2021 - 11 - 12',
		unit: 'Antakalnio g. 112 - 12, Vilniaus m., Vilniaus m. sav',
		votes: '8/61',
		options: ['Pritariu', 'Nepritariu', 'Neturiu nuomonės'],
	},
	{
		question: 'Ar jaučiatės saugūs savo kiemo laiptinėje?',
		date: '2021 - 11 - 12',
		unit: 'Antakalnio g. 112 - 12, Vilniaus m., Vilniaus m. sav',
		votes: '8/61',
		options: ['Pritariu', 'Nepritariu', 'Neturiu nuomonės'],
	},
];

export const mockVotingsData = [
	{
		question: 'Ar pritariate etapinei renovacijai?',
		date: '2021 - 11 - 12',
		unit: 'Antakalnio g. 112 - 12, Vilniaus m., Vilniaus m. sav',
		votes: '8/61',
		options: ['Pritariu', 'Nepritariu', 'Neturiu nuomonės'],
	},
	{
		question: 'Ar jaučiatės saugūs savo kiemo laiptinėje?',
		date: '2021 - 11 - 12',
		unit: 'Antakalnio g. 112 - 12, Vilniaus m., Vilniaus m. sav',
		votes: '8/61',
		options: ['Pritariu', 'Nepritariu', 'Neturiu nuomonės'],
	},
];
