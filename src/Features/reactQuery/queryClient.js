import { QueryClient, setLogger } from 'react-query';

setLogger({
	warn: () => {},
	error: () => {},
});

export const queryClient = new QueryClient({
	defaultOptions: {
		queries: {
			retry: false,
			refetchInactive: true,
			refetchOnWindowFocus: true,
			cacheTime: 6000,
			// staleTime: 6000000,
		},
	},
});
