import {
	useInfiniteQuery as $useInfiniteQuery,
	useMutation as $useMutation,
	useQuery as $useQuery,
} from 'react-query';

import superagent from 'superagent';
import { useMst } from '../store/store';

const BASE_URL = 'https://staging.my.civinity.lt/api/web/';
const isJson = str => {
	try {
		JSON.parse(str);
	} catch (e) {
		return false;
	}
	return true;
};

const useErrorHandler = () => {
	const { logout, setToast, auth } = useMst();
	return ({ status, message }) => {
		setToast(
			isJson(message)
				? `errors.${JSON.parse(message)?.message}`
				: message || 'unknown',
			5,
		);
		status === 401 && !!auth?.token && logout();
		return {};
	};
};

export const useQuery = (
	name,
	endpoint,
	// eslint-disable-next-line no-unused-vars
	{ query = {}, onError = () => {}, ...options } = {},
	method = 'GET',
	isInfinite,
) => {
	const errorHandler = useErrorHandler();
	const {
		auth: { token },
	} = useMst();

	const agent = superagent(method, BASE_URL + endpoint);

	agent.query(query);

	if (token) {
		// set JWT
		agent.set('Authorization', `Bearer ${token}`);
	}

	if (isInfinite) {
		return $useInfiniteQuery(name, () => agent.then(d => d.body), options);
	}

	return $useQuery(
		name,
		() => !!endpoint && agent.then(d => d.body).catch(errorHandler),
		options,
	);
};

export const useMutation = (endpoint, { ...options } = {}, method = 'POST') => {
	const {
		auth: { token },
	} = useMst();
	const errorHandler = useErrorHandler();

	const agent = superagent(method, BASE_URL + endpoint);

	if (token) {
		agent.set('Authorization', `Bearer ${token}`);
	}

	return $useMutation(
		payload =>
			agent
				.on('error', err => {
					errorHandler(err);
				})
				.send(payload),
		options,
	);
};

export const useInfiniteQuery = (
	name,
	endpoint,
	{ query = {}, ...options } = {},
) => {
	const {
		auth: { token },
	} = useMst();

	const infiniteFetch = ({ pageParam = 0 }) =>
		superagent
			.get(BASE_URL + endpoint)
			.query({ ...query, page: query?.page || pageParam })
			.set('Authorization', `Bearer ${token}`)
			.then(d => d.body);

	return $useInfiniteQuery(name, infiniteFetch, options);
};
