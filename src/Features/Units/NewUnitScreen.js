import React, { useEffect, useRef, useState } from 'react';
import { useNavigation } from '@react-navigation/native';
import { inject, observer } from 'mobx-react';
import { useQueryClient } from 'react-query';
import {
	Text,
	BackButton,
	Card,
	Screen,
	View,
	Button,
	DropDown,
	ConfirmationModal,
} from '../theme';
import { theme } from '../theme/themeConfig';
import { useAddUnit, useInfiniteBuildings, useUnitsInBuilding } from './api';

const PAGE_SIZE = 30;
const NewUnitScreen = inject('store')(
	observer(({ store: { setToast } }) => {
		const navigation = useNavigation();
		const [errors, setErrors] = useState({});
		const [isConfirmationModalVisible, setIsConfirmationModalVisible] =
			useState(false);
		const [buildings, setBuildings] = useState([]);
		const [units, setUnits] = useState([]);
		const [lastBuildingPageItem, setLastBuildingPageItem] = useState({});
		const [selectedBuilding, setSelectedBuilding] = useState({});
		const [selectedUnit, setSelectedUnit] = useState({});
		const [buildingSearch, setBuildingSearch] = useState('');
		const queryClient = useQueryClient();
		const unitsDropDownRef = useRef();
		const {
			data: buildingData,
			refetch: buildingRefetch,
			fetchNextPage: buildingFetchNextPage,
			isLoading: isBuildingLoading,
			isFetched: isBuildingFetched,
		} = useInfiniteBuildings({
			query: {
				pageSize: PAGE_SIZE,
				lastPageItemId: lastBuildingPageItem?._id ?? undefined,
				lastPageItemTitle: lastBuildingPageItem?.title ?? undefined,
				search: buildingSearch || undefined,
			},
		});

		const { data: unitsData, refetch: unitsRefetch } = useUnitsInBuilding(
			selectedBuilding?.value,
		);

		const addUnit = useAddUnit({
			onSuccess: () => {
				setToast('Unit added successfully', 5, 'success');

				// todo: we should check and optimise invalidations
				queryClient.invalidateQueries('useOwnedUnit').then(() => {
					navigation.goBack();
				});
			},
		});

		useEffect(() => {
			const nextPageData = (buildingData || {})?.pages
				?.flat()
				.map(item => ({
					label: item?.title || item?.name || 'Untitled',
					value: item?.externalId,
					...item,
				}));
			isBuildingFetched &&
				setBuildings(prevState => [
					...(prevState || []),
					...nextPageData,
				]);
		}, [buildingData, isBuildingFetched]);

		useEffect(() => {
			setUnits(
				(unitsData || [])?.map(item => ({
					label: item?.unitNumber?.toString(),
					value: item?.id,
					...item,
				})) || [],
			);
		}, [selectedBuilding, unitsData]);

		const buildingsDropdownProps = {
			onRefresh: () => buildingRefetch(),
			refreshing: isBuildingLoading,
			onEndReachedThreshold: 0.01,
			onEndReached: () => {
				setLastBuildingPageItem(
					buildingData?.pages?.flat().slice(-1)[0],
				);
				buildingFetchNextPage();
			},
			extraData: buildings,
		};
		//
		// useEffect(() => {
		// 	selectedUnit?.value && unitsRefetch();
		// }, [unitsRefetch, selectedUnit?.value]);

		const handleBuildingSearch = newSearchValue => {
			setBuildings([]);
			setBuildingSearch(newSearchValue);
			setLastBuildingPageItem({});
		};

		const handleSelectBuilding = building => {
			setErrors(prevState => ({ ...prevState, building: undefined }));
			setSelectedBuilding(building);
			setSelectedUnit({});
			unitsDropDownRef.current.clear();
			unitsRefetch(building?.value);
		};

		const handleSelectUnit = unit => {
			setErrors(prevState => ({ ...prevState, unit: undefined }));
			setSelectedUnit(unitsData.find(item => item.id === unit.value));
		};

		const labelGenerator = () =>
			`${selectedBuilding?.name} - ${selectedUnit?.unitNumber || ''}${
				selectedUnit?.unitLetter || ''
			}, ${
				selectedBuilding?.municipality
					? `${selectedBuilding?.municipality}, `
					: ''
			}${selectedBuilding?.city}`;

		const handleAddUnit = () => {
			setIsConfirmationModalVisible(false);
			if (isFormValidated()) {
				const label = labelGenerator();
				addUnit.mutate({
					building: selectedBuilding?._id,
					buildingExtId: selectedBuilding?.externalId,
					unitExtId: selectedUnit?.id,
					title: label,
					label,
				});
			}
		};

		const isFormValidated = () => {
			const currentErrors = {
				building: !selectedBuilding?._id
					? 'screens.newUnit.buildingError'
					: undefined,
				unit: !selectedUnit?.id
					? 'screens.newUnit.unitError'
					: undefined,
			};

			JSON.stringify(currentErrors) !== JSON.stringify(errors) &&
				setErrors(currentErrors);
			return !currentErrors?.building && !currentErrors.unit;
		};

		const handleCancel = () => navigation.goBack();

		const handleCloseConfirmationModal = () =>
			setIsConfirmationModalVisible(false);
		const handleShowConfirmationModal = () => {
			if (isFormValidated()) setIsConfirmationModalVisible(true);
		};
		return (
			<Screen
				translucent
				barStyle="dark-content"
				barColor="transparent"
				paddingHorizontal={theme.pads.horizontalPad}>
				<BackButton showLabel marginBottom={20} />
				<Card
					marginTop={10}
					title="screens.newUnit.title"
					showTitleHorizontalLine={false}>
					<Text
						paddingBottom={theme.pads.sectionVerticalPad}
						variant={theme.typo.fontVariants.smallRegular}
						color={theme.palette.baseColors.grey}>
						screens.newUnit.description
					</Text>
					<View marginBottom={theme.pads.sectionVerticalPad}>
						<DropDown
							showExteriorLabel
							error={errors?.building}
							onChange={handleSelectBuilding}
							modalHeight="80%"
							listData={buildings}
							listProps={buildingsDropdownProps}
							label="screens.newUnit.addBuilding"
							searchEnabled
							onSearchTextChanged={handleBuildingSearch}
						/>
					</View>
					<View marginBottom={theme.pads.sectionVerticalPad}>
						<DropDown
							ref={unitsDropDownRef}
							showExteriorLabel
							pressEnabled={
								Object.keys(selectedBuilding).length > 0
							}
							error={errors?.unit}
							onChange={handleSelectUnit}
							modalHeight="80%"
							listData={units}
							itemLabelProperty="name"
							label="screens.newUnit.addUnit"
						/>
						{Object.keys(selectedBuilding || {}).length > 0 &&
							Object.keys(selectedUnit || {}).length > 0 && (
								<>
									<Text
										paddingTop={20}
										variant={
											theme.typo.fontVariants.smallRegular
										}
										color={theme.palette.baseColors.base2}>
										{labelGenerator()}
									</Text>
									<ConfirmationModal
										onRequestClose={
											handleCloseConfirmationModal
										}
										isVisible={isConfirmationModalVisible}
										title="screens.newUnit.unitConfirmation"
										description={labelGenerator()}
										primaryButtonAction={handleAddUnit}
										secondaryButtonAction={
											handleCloseConfirmationModal
										}
									/>
								</>
							)}
					</View>

					<Button
						disabled={!(selectedBuilding?._id && selectedUnit.id)}
						loading={addUnit.isLoading}
						onPress={handleShowConfirmationModal}
						marginBottom={15}
						label="screens.newUnit.addUnit"
					/>
					<Button
						onPress={handleCancel}
						outline
						label="general.form.cancel"
					/>
				</Card>
			</Screen>
		);
	}),
);
export default NewUnitScreen;
