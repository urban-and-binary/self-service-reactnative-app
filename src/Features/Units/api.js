import { useInfiniteQuery, useMutation, useQuery } from '../reactQuery/hooks';

export const useInfiniteBuildings = (option = {}) =>
	useInfiniteQuery(
		['useInfiniteBuildings', option.query],
		'buildings',
		option,
	);
export const useUnitsInBuilding = (buildingId, option = {}) =>
	useQuery(
		!!buildingId && ['useUnitsInBuilding', buildingId],
		`buildings/${buildingId}/units`,
		option,
	);
export const useAddUnit = (option = {}) => useMutation('owned-unit', option);
export const useOwnedUnits = (option = {}) =>
	useQuery('useOwnedUnit', 'owned-unit', option);
