import React from 'react';
import { useFocusEffect, useNavigation } from '@react-navigation/native';
import { Button, Card, HorizontalLine } from '../theme';
import { UnitsListItem } from './UnitsListItem';
import { useOwnedUnits } from './api';
import { unitsStatus } from './unitsConfig';

function ProfileUnit() {
	const { data, refetch } = useOwnedUnits();
	const navigation = useNavigation();

	useFocusEffect(
		React.useCallback(() => {
			refetch();
		}, [refetch]),
	);

	const handleNavigateToNewUnit = () => navigation.navigate('newUnit');
	const renderFailedUnitItem = (item, index) => (
		<UnitsListItem
			key={index}
			pending
			marginBottom={20}
			address={item?.label}
		/>
	);
	const renderUnitItem = (item, index) => (
		<UnitsListItem key={index} marginBottom={20} address={item?.label} />
	);
	const pendingUnits = (data || []).filter(
		item => item?.status !== unitsStatus.approved,
	);
	const approvedUnits = (data || []).filter(
		item => item?.status === unitsStatus.approved,
	);
	return (
		<Card defaultExpand={false} expandable title="Units">
			{approvedUnits.map(renderUnitItem)}
			{approvedUnits.length > 0 && pendingUnits.length > 0 && (
				<HorizontalLine />
			)}
			{pendingUnits.map(renderFailedUnitItem)}
			<Button
				marginTop={30}
				marginBottom={10}
				label="+ Add new unit"
				onPress={handleNavigateToNewUnit}
			/>
		</Card>
	);
}
export default ProfileUnit;
