import React from 'react';
import Feather from 'react-native-vector-icons/Feather';
import { View, Text, Tag } from '../theme';
import { theme } from '../theme/themeConfig';

export function UnitsListItem({ title, address, pending, ...props }) {
	const fontColor = pending
		? theme.palette.baseColors.base3
		: theme.palette.baseColors.base1;
	return (
		<View
			row
			borderRadius={theme.roundness.large}
			backgroundColor={theme.palette.baseColors.base6}
			paddingVertical={theme.pads.horizontalPad}
			paddingHorizontal={theme.pads.horizontalPad}
			{...props}>
			<View
				backgroundColor={
					pending
						? theme.palette.baseColors.base5
						: theme.palette.primaryColors.color3
				}
				borderRadius={theme.roundness.medium}
				center
				width={50}
				height={50}>
				<Feather
					name="home"
					color={fontColor}
					size={theme.typo.fontSize.large}
				/>
			</View>
			<View
				alignItems="flex-start"
				paddingLeft={theme.pads.horizontalPad}
				paddingRight={theme.pads.horizontalPad * 2}>
				{!!title && (
					<Text
						color={fontColor}
						variant={theme.typo.fontVariants.xsBold}>
						{title}
					</Text>
				)}
				{!!address && (
					<Text
						color={fontColor}
						variant={theme.typo.fontVariants.smallRegular}>
						{address}
					</Text>
				)}
				{pending && (
					<Tag marginTop={10} label="screens.profile.unitsPending" />
				)}
			</View>
		</View>
	);
}
