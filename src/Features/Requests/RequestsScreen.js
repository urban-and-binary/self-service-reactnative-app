import React from 'react';
import { Screen, Text, View } from '../theme';

export default function RequestsScreen() {
	return (
		<Screen>
			<View flex center>
				<Text>Requests Screen</Text>
			</View>
		</Screen>
	);
}
