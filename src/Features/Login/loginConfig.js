export const loginTabs = ['general.form.email', 'general.form.phone'];
export const loginTypes = {
	email: 'email',
	phone: 'phone',
};
