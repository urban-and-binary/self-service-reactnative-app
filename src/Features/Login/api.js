import { useMutation } from '../reactQuery/hooks';

export const useLogin = (option = {}) => useMutation('auth/login', option);
export const useSendCode = (option = {}) =>
	useMutation('auth/send-code', option);
export const useLogout = (option = {}) => useMutation('auth/logout', option);
