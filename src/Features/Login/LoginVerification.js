import React, { createRef, useEffect, useRef, useState } from 'react';
import {
	ActivityIndicator,
	Platform,
	StyleSheet,
	TextInput,
} from 'react-native';
import { inject, observer } from 'mobx-react';
import { useRoute } from '@react-navigation/native';
import { BackButton, Breadcumb, Screen, Text, View } from '../theme';
import { theme } from '../theme/themeConfig';
import { useLogin, useSendCode } from './api';
import { useTranslation } from '../translation/useTranslation';
import { useSignup } from '../Signup/api';
import { loginTypes } from './loginConfig';

const CODE_LENGTH = 6;
// This screen will use on signup with email and signup with phone
const LoginVerificationScreen = inject('store')(
	observer(({ store: { assign, setToast, auth } }) => {
		const t = useTranslation();
		const { params } = useRoute();
		const { target, password, isEmailConfirmation } = params || {};
		const [code, setCode] = useState([...Array(CODE_LENGTH).fill('')]);
		const [otpArr, setOtpArr] = useState([]);

		// for focusing on next text view
		const charRefs = useRef([]);
		charRefs.current = code.map(
			(_, i) => charRefs.current[i] ?? createRef(),
		);

		useEffect(() => {
			otpArr.length === 0 &&
				code.forEach((item, index) => {
					const otpCharObj = {
						placeholder: '-',
						ref: charRefs.current[index],
						code: '',
					};
					setOtpArr(prevState => [...prevState, otpCharObj]);
				});
		}, [code, otpArr]);
		const handleSaveToken = token => {
			assign({
				isLoaded: false, // to load splash again and sync app with token
				auth: { ...auth, token },
			});
		};

		const emailVerify = useLogin({
			onSuccess: ({ body }) => {
				if (body?.token) {
					handleSaveToken(body.token);
				} else {
					assign({
						toastMessage: {
							message: 'general.errors.unknown',
							duration: 5,
						},
					});
				}
			},
		});

		const resendVerificationCodeEmail = useSignup({
			onSuccess: () => {
				setToast('screens.loginVerification.resendMessageEmail', 5);
			},
		});

		const resendVerificationCodePhone = useSendCode({
			onSuccess: () => {
				setToast('screens.loginVerification.resendMessagePhone', 5);
			},
		});

		const handleResend = () => {
			if (isEmailConfirmation) {
				resendVerificationCodeEmail.mutate({ email: target, password });
				return;
			}
			resendVerificationCodePhone.mutate({
				phone: target,
				loginType: loginTypes.phone,
			});
		};

		const handleLogin = codeArray => {
			emailVerify.mutate({
				email: isEmailConfirmation ? target : '',
				password,
				phone: !isEmailConfirmation ? target : '',
				verificationCode: codeArray.join(''),
				loginType: isEmailConfirmation
					? loginTypes.email
					: loginTypes.phone,
			});
		};

		const handleCode = (char, index) => {
			if (char === '' && index !== code.length - 1) {
				return;
			}

			const oldCode = [...code];

			oldCode[index] = char;
			setCode(oldCode);

			if (index < code.length - 1 && char) {
				otpArr[index + 1].ref.current.focus();
			} else if (index === code.length - 1 && char) handleLogin(oldCode);
		};

		const handleKeyPress = (index, event) => {
			const { key } = event;
			if (key === 'Backspace') {
				if (
					(index > 0 && index !== code.length - 1) ||
					(index === code.length - 1 && code[index] === '')
				) {
					otpArr[index - 1].ref.current.focus();
				}
			}
		};

		const handleFocus = index => {
			const oldCode = [...code];
			if (oldCode[index]) {
				oldCode[index] = '';
				setCode(oldCode);
			}
		};

		return (
			<Screen
				translucent={false}
				horizontalPad
				verticalPad
				barColor={theme.palette.primaryColors.color2}
				barStyle={
					Platform.OS === 'ios' ? 'dark-content' : 'light-content'
				}>
				<Breadcumb
					leftComponent={<BackButton />}
					rightComponent={<View />}
				/>
				<View flex center>
					<Text variant={theme.typo.fontVariants.h2Bold}>
						{isEmailConfirmation
							? 'screens.loginVerification.titleEmail'
							: 'screens.loginVerification.titlePhone'}
					</Text>
					<Text
						paddingTop={40}
						textAlign="center"
						variant={theme.typo.fontVariants.xsRegular}>
						{t(
							isEmailConfirmation
								? 'screens.loginVerification.emailDescription'
								: 'screens.loginVerification.phoneDescription',
						)}
						<Text
							textAlign="center"
							color={theme.palette.primaryColors.color2}
							variant={theme.typo.fontVariants.xsBold}>
							{` ${target || ''}`}
						</Text>
					</Text>
					<View row paddingTop={30}>
						{otpArr.map((item, index) => (
							<TextInput
								disabled={
									emailVerify.isLoading ||
									resendVerificationCodeEmail.isLoading
								}
								key={index.toString()}
								value={code[index]}
								ref={item.ref}
								placeholderTextColor="white"
								style={defaultStyle.passInput}
								placeholder={item.placeholder}
								keyboardType="number-pad"
								underlineColorAndroid="transparent"
								onFocus={() => handleFocus(index)}
								caretHidden
								maxLength={1}
								onChangeText={char => handleCode(char, index)}
								onKeyPress={({ nativeEvent }) => {
									handleKeyPress(index, nativeEvent);
								}}
							/>
						))}
					</View>
					{(emailVerify.isLoading ||
						resendVerificationCodeEmail.isLoading) && (
						<ActivityIndicator />
					)}
					{!emailVerify.isLoading &&
						!resendVerificationCodeEmail.isLoading && (
							<>
								<Text
									variant={
										theme.typo.fontVariants.smallRegular
									}
									color={theme.palette.baseColors.base3}
									paddingTop={20}>
									screens.loginVerification.notReceived
								</Text>
								<Text
									onPress={handleResend}
									variant={
										theme.typo.fontVariants.smallRegular
									}
									color={theme.palette.primaryColors.color2}>
									screens.loginVerification.resend
								</Text>
							</>
						)}
				</View>
			</Screen>
		);
	}),
);

const defaultStyle = StyleSheet.create({
	passInput: {
		borderRadius: theme.roundness.small,
		color: theme.palette.baseColors.base1,
		borderColor: theme.palette.baseColors.base4,
		borderStyle: 'solid',
		borderWidth: 1,
		fontSize: theme.typo.fontSize.medium,
		marginRight: 10,
		width: 40,
		justifyContent: 'center',
		alignItems: 'center',
		textAlign: 'center',
		height: 55,
	},
});

export default LoginVerificationScreen;
