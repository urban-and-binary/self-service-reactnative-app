import React, { useState } from 'react';
import { Platform } from 'react-native';
import { inject, observer } from 'mobx-react';
import { useNavigation } from '@react-navigation/native';
import { Breadcumb, Button, Screen, Text, View } from '../theme';
import { theme } from '../theme/themeConfig';
import { BreadcumbLanguage } from '../Breadcrumbs/BreadcumbLanguage';
import { TabBar } from '../theme/TabBar';
import { loginTabs, loginTypes } from './loginConfig';
import { useLogin, useSendCode } from './api';
import { LoginWithPhone } from './LoginWithPhone';
import { LoginWithEmail } from './LoginWithEmail';
import { isEmailValidated } from '../theme/validationUtil';

const LoginScreen = inject('store')(
	observer(({ store: { assign, auth } }) => {
		const [activeTab, setActiveTab] = useState(loginTabs[0]);
		const [errors, setErrors] = useState({});
		const [email, setEmail] = useState('');
		const [phone, setPhone] = useState('');
		const [password, setPassword] = useState('');
		const navigation = useNavigation();
		const handleSaveToken = token => {
			assign({
				isLoaded: false, // to load splash again and sync app with token
				auth: { ...auth, token },
			});
		};
		const isEmailAuthActive = activeTab === loginTabs[0];
		const navigateToSignupWithEmail = () => {
			navigation.navigate('signUpWithEmail');
		};

		const login = useLogin({
			onSuccess: ({ body }) => {
				if (body?.verifiedAt === null) {
					navigation.navigate('loginVerification', {
						target: email,
						password,
						isEmailConfirmation: true,
						codeLength: body?.verificationCode?.length,
						userId: body?._id,
					});
				} else {
					!!body?.token && handleSaveToken(body.token);
				}
			},
		});

		const handleChangeTab = newActiveTab => {
			setPhone('');
			setEmail('');
			setActiveTab(newActiveTab);
		};

		const handlePhoneChange = newPhone => {
			setPhone(newPhone);
			errors?.phone && setErrors({ ...errors, phone: undefined });
		};

		const sendCode = useSendCode({
			onSuccess: ({ body }) => {
				navigation.navigate('loginVerification', {
					target: phone,
					password,
					isEmailConfirmation: false,
					codeLength: body?.verificationCode?.length,
					userId: body?._id,
				});
			},
		});

		const formValidation = (shouldHandleErrors = false) => {
			if (isEmailAuthActive) {
				shouldHandleErrors &&
					setErrors({
						email: !isEmailValidated(email)
							? 'screens.login.errorEmail'
							: '',
						password: !password
							? 'screens.login.errorPassword'
							: '',
					});
				return isEmailValidated(email) && !!password;
			}
			shouldHandleErrors &&
				setErrors({
					phone: !phone ? 'screens.login.errorPhone' : '',
				});
			return !!phone;
		};

		const doLogin = () => {
			if (isEmailAuthActive) {
				formValidation(true) &&
					login.mutate({
						email,
						password,
						loginType: loginTypes.email,
					});
				return;
			}
			formValidation(true) &&
				sendCode.mutate({
					phone,
					loginType: loginTypes.phone,
				});
		};

		return (
			<Screen
				preset="scroll"
				flex
				contentFlexGrow
				translucent={false}
				horizontalPad
				verticalPad
				barColor={theme.palette.primaryColors.color2}
				barStyle={
					Platform.OS === 'ios' ? 'dark-content' : 'light-content'
				}>
				<View>
					<Breadcumb
						leftComponent={<View />}
						rightComponent={
							<BreadcumbLanguage currentLanguage="en" />
						}
					/>
					<Text variant={theme.typo.fontVariants.h2Bold}>
						screens.login.title
					</Text>
					<Text variant={theme.typo.fontVariants.xsRegular}>
						screens.login.choiceDescription
					</Text>
					<View paddingVertical={20}>
						<TabBar
							onChange={handleChangeTab}
							tabs={loginTabs}
							activeTab={activeTab}
						/>
					</View>
				</View>
				{activeTab === loginTabs[1] && (
					<LoginWithPhone
						errors={errors}
						onPhoneChanged={handlePhoneChange}
						onSubmitPressed={doLogin}
					/>
				)}
				{activeTab === loginTabs[0] && (
					<LoginWithEmail
						errors={errors}
						navigation={navigation}
						onEmailChanged={setEmail}
						onPasswordChanged={setPassword}
						onSubmitPressed={doLogin}
					/>
				)}
				<View justifyContent="flex-end">
					<Button
						disabled={!formValidation()}
						loading={sendCode.isLoading || login.isLoading}
						onPress={doLogin}
						label={
							activeTab === loginTabs[0]
								? 'general.form.login'
								: 'screens.login.sendVerification'
						}
					/>
				</View>
				<View row paddingVertical={30} center>
					<Text variant={theme.typo.fontVariants.smallRegular}>
						screens.login.noAccount
					</Text>
					<Text
						paddingHorizontal={5}
						variant={theme.typo.fontVariants.smallBold}
						onPress={navigateToSignupWithEmail}>
						general.form.signup
					</Text>
				</View>
			</Screen>
		);
	}),
);

export default LoginScreen;
