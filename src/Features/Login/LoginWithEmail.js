import { StyleSheet } from 'react-native';
import React, { useRef } from 'react';
import { useTranslation } from '../translation/useTranslation';
import { Text, TextInput, View } from '../theme';
import { theme } from '../theme/themeConfig';
import HideByKeyboard from '../keyboard/HideByKeyboard';
import { TextInputEmail } from '../theme/TextInputEmail';

export function LoginWithEmail({
	navigation,
	errors,
	onEmailChanged,
	onPasswordChanged,
	onSubmitPressed,
}) {
	const t = useTranslation();

	const navigateToForgetPassword = () => {
		navigation.navigate('forgetPassword');
	};
	const passwordRef = useRef();
	return (
		<View>
			<HideByKeyboard distance={50}>
				<Text
					paddingVertical={10}
					variant={theme.typo.fontVariants.smallRegular}
					color={theme.palette.baseColors.base3}>
					screens.login.phoneDescription
				</Text>
			</HideByKeyboard>
			<TextInputEmail
				error={errors?.email}
				onChangeText={onEmailChanged}
				isRequired
				label="general.form.email"
				placeholder={t('screens.login.emailPlaceholder')}
				returnKeyType="next"
				returnKeyLabel="Next"
				onSubmitEditing={() => passwordRef.current?.focus()}
			/>
			<TextInput
				forwardedRef={passwordRef}
				error={errors?.password}
				marginTop={15}
				onChangeText={onPasswordChanged}
				isRequired
				hideUnderline
				secureTextEntry
				autoCorrect={false}
				autoCapitalize="none"
				clearButtonMode="while-editing"
				label="general.form.password"
				placeholder={t('general.form.password')}
				containerStyle={defaultStyles.passwordInput}
				returnKeyType="done"
				onSubmitEditing={onSubmitPressed}
			/>

			<View
				paddingVertical={20}
				row
				justifyContent="flex-end"
				alignItems="center">
				<Text
					onPress={navigateToForgetPassword}
					variant={theme.typo.fontVariants.xsRegular}>
					screens.login.forgetPassword
				</Text>
			</View>
		</View>
	);
}

const defaultStyles = StyleSheet.create({
	passwordInput: {
		marginTop: 20,
	},
});
