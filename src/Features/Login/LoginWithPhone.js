import React from 'react';
import { Text, View } from '../theme';
import { theme } from '../theme/themeConfig';
import { useKeyboard } from '../keyboard/useKeyboard';
import { TextInputPhone } from '../theme/TextInputPhone';

export function LoginWithPhone({ onPhoneChanged, onSubmitPressed, errors }) {
	const keyboard = useKeyboard();
	return (
		<View style={{ flex: keyboard.isActive ? 0 : 1 }} marginBottom={15}>
			<Text
				paddingVertical={10}
				variant={theme.typo.fontVariants.smallRegular}
				color={theme.palette.baseColors.base3}>
				screens.login.phoneDescription
			</Text>
			<TextInputPhone
				error={errors?.phone}
				isRequired
				label="general.form.phone"
				placeholder="+370"
				onChangeText={onPhoneChanged}
				onSubmitEditing={onSubmitPressed}
			/>
		</View>
	);
}
