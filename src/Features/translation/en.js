/* eslint-disable */
export default {
	general: {
		errors: {
			unknown: 'Something went wrong',
			tryAgain: 'Try again',
			signUpEmail: 'Please enter your email.',
			signUpPassword: 'Please enter your password',
			signUpRePassword: 'Passwords are not matched',
			signUpTerms: 'Please review and accept Terms and Conditions',
			incorrectPhone: 'Your phone number is incorrect!',
			incorrectEmail: 'Your email address is incorrect!',
		},
		form: {
			rememberMe: 'Remember me',
			login: 'Log In',
			signup: 'Sign Up',
			save: 'Save',
			saveChange: 'Save change',
			phone: 'Phone',
			phoneNumber: 'Phone number',
			email: 'Email',
			emailAddress: 'Email address',
			password: 'Password',
			retypePassword: 'Retype password',
			terms: 'Terms and Conditions',
			tryAgain: 'Try Again',
			or: 'Or',
			fullName: 'Full Name',
			contactEmail: 'Contact email',
			contactPhone: 'Contact phone',
			cancel: 'Cancel',
			next: 'Next',
			back: 'Back',
			confirm: 'Confirm',
			declare: 'Declare',
			date: 'Date',
			edit: 'Edit',
			description: 'Description',
			confirmWithSignature: 'Confirm with signature',
			country: 'Country',
			city: 'City',
			address: 'Address',
		},
	},
	screens: {
		login: {
			title: 'Log In',
			choiceDescription: 'Please choose how you want to Log In:',
			phoneDescription:
				'Please enter your phone number so that we can send you a verification code',
			forgetPassword: 'Forgot your password?',
			emailPlaceholder: 'Your email',
			// eslint-disable-next-line
			noAccount: "Don't have an account?",
			sendVerification: 'Send verification code',
			errorEmail: 'Please write your email.',
			errorPassword: 'Please write your password.',
			errorPhone: 'Please write your phone.',
		},
		loginVerification: {
			titlePhone: 'Phone verification',
			titleEmail: 'Email verification',
			phoneDescription:
				'Enter the code that we sent to your phone number:',
			emailDescription:
				'Enter the code that we sent to your Email address:',
			notReceived: 'Did not receive any code?',
			resend: 'Resend code',
			resendMessageEmail:
				'New verification code sent to your Email address!',
			resendMessagePhone:
				'New verification code sent to your Phone number!',
		},
		signUp: {
			title: 'Sign Up',
			description: 'Please choose how you want to sign up',
			haveAccount: 'Already have an account?',
			condition: 'By creating an account I accept Civinity Namai',
		},
		emailConfirmation: {
			title: 'Account confirmation',
			description: 'We sent a confirmation message to your email: ',
			notReceived: 'If you did not get it, Please try again',
		},
		forgetPassword: {
			title: 'Forgot password',
			description:
				'Please enter your email or phone number so we can send you a verification code to reset your password',
			verificationCode: 'Send verification code',
			verificationLink: 'Send verification link',
			verificationBtn: 'Send verification',
		},
		forgetPasswordCode: {
			title: 'Password reset',
			description: 'Enter the 6-digit code that we sent to',
		},
		changePassword: {
			title: 'Change password',
			description:
				'To change your current password, please enter your new password twice.',
		},
		additionalInfo: {
			title: 'Additional information',
			p1: {
				description:
					'Please, check it out and renew your account information. \nDont be afraid, you can always change it later.',
				fullNamePlaceholder: 'Name, surname',
				emailPlaceholder: 'vardenis@gmail.com',
				phonePlaceholder: '+370',
			},
			p2: {
				address: 'Address for correspondence (unnecessary)',
				addressRequired: 'Address for correspondence',
				addressPlaceholder: 'Antakalnio g. 112 - 12, Vilnius',
				privacyPolicy:
					"I've understood The Privacy Policy and Web and App usage terms and I agree to comply",
				personalData:
					"By creating this account I agree that 'Civinity Namai' would manage all of my agreement data for the purpose self-service.",
				userRules:
					"I've understood The user terms and I agree to comply",
				bills: 'I wish to receive only e-bills',
				advertisment:
					'I agree to receive the latest notifications to my email about Civinity Home or supplier services.',
			},
			p3: {
				description:
					'Here you can add your first unit. If you dont have any, or you dont want to add at this step, only press the button "Done" and go to homepage.\nYou can always add your unit at your profile.',
				addUnit: 'Add a unit',
				addUnitPlaceholder: '+ Add a unit',
			},
		},
		profile: {
			unitsPending: 'Waiting for approval',
			personalInformation: 'Personal information',
			address: 'Address',
			addressSubtitle: 'Address for correspondence',
			changeSettingsText:
				'To cancel subscription to information sent by mail, you can go to settings',
			addressLinkToSettings: 'or press here',
			currentPassword: 'Current password',
			newPassword: 'New password',
			retypePassword: 'Repeat new password',
			passwordRules: 'Passwords must contains:',
			passwordOneLetter: 'At least one capital letter',
			passwordOneNumber: 'At least one capital letter',
			passwordsMustMatch: 'Passwords must mach',
			newPasswordError: 'You cannot use current password',
		},
		newUnit: {
			title: 'Add new unit',
			description: 'Here you can add...',
			addBuilding: 'Add a building',
			addUnit: 'Add a unit',
			unitConfirmation:
				'Are you sure you want to add this unit as your property?',
			buildingError: 'Select a building first',
			unitError: 'You should select the unit to continue',
		},
		meters: {
			title: 'Meter Declarations',
			headerScale: 'Scale',
			headerQuantity: 'Quantity (from - to)',
			chooseAUnit: 'Choose a unit',
			addReadingToast: 'Meters reading submitted successfully!',
			emptyState: 'You have no units in your account',
			emptyStateActions: 'Add a unit',
		},
		announcements: {
			title: 'Announcements',
			private: 'Private',
			common: 'Common',
			day: 'Announcement day',
		},
		polls: {
			title: 'Polls and votings',
			polls: 'Polls',
			votings: 'Votings',
			vote: 'Vote',
			conditions:
				'Pasirašydami šį biuletenį patvirtinate, kad susipažinote su balsavimo taisyklėmis ir privatumo politika.',
		},
		documents: {
			title: 'Documents',
		},
	},
};
