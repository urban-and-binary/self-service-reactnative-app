import get from 'lodash/get';
import { useMst } from '../store/store';
import locales from './index';

export const useTranslation = () => {
	const store = useMst();
	const { locale } = store || {};

	const fn = path => {
		if (!path || !locales[locale || 'en']) {
			return;
		}

		return get(locales[locale || 'en'], path, path);
	};

	return fn;
};
