import React from 'react';
import SwipeAnimation from '../theme/SwipeAnimation';
import { useKeyboard } from './useKeyboard';

export default function HideByKeyboard({ children, distance, ...props }) {
	const keyboard = useKeyboard();
	return (
		<SwipeAnimation
			distance={distance ?? keyboard.height}
			delay={0}
			duration={300}
			visible={!keyboard.isActive}
			{...props}>
			{children}
		</SwipeAnimation>
	);
}
