import React from 'react';
import moment from 'moment';
import { Card, HorizontalLine, Text, View } from '../theme';
import { theme } from '../theme/themeConfig';
import { metersConfig } from './metersConfig';

export function UnitMetersItem({ unit, ...props }) {
	const renderItem = (item, index) => (
		<MeterItem
			key={index}
			item={item}
			lastItem={index === unit.meters.length - 1}
		/>
	);
	return (
		<Card
			title={unit?.label || 'Untitled'}
			showTitleHorizontalLine={false}
			{...props}>
			<Text
				variant={theme.typo.fontVariants.baseRegular}
				color={theme.palette.baseColors.grey}>
				{unit.unitExtId}
			</Text>
			<View row marginTop={20}>
				<Text
					paddingRight={10}
					variant={theme.typo.fontVariants.smallBold}
					color={theme.palette.baseColors.grey}>
					general.form.date
				</Text>
				<Text
					variant={theme.typo.fontVariants.smallBold}
					color={theme.palette.baseColors.base1}>
					{moment(unit.createdAt).format('yyyy - MM - DD')}
				</Text>
			</View>
			<View row marginTop={20} marginBottom={10}>
				<View width="50%">
					<Text
						variant={theme.typo.fontVariants.smallBold}
						color={theme.palette.baseColors.grey}>
						screens.meters.headerScale
					</Text>
				</View>
				<View width="50%">
					<Text
						variant={theme.typo.fontVariants.smallBold}
						color={theme.palette.baseColors.grey}>
						screens.meters.headerQuantity
					</Text>
				</View>
			</View>
			{unit?.meters.map(renderItem)}
		</Card>
	);
}
function MeterItem({ item, lastItem }) {
	return (
		<>
			<View row marginVertical={10}>
				<View width="50%" row>
					{!!metersConfig[item?.localType]?.id && (
						<View marginRight={10}>
							{metersConfig[item.localType].icon}
						</View>
					)}
					<Text
						variant={theme.typo.fontVariants.smallRegular}
						color={theme.palette.baseColors.base1}>
						{item?.type}
					</Text>
				</View>
				<View width="50%">
					<Text
						variant={theme.typo.fontVariants.smallRegular}
						color={theme.palette.baseColors.base1}>
						{item?.valueFrom} {item.measurementUnits} -{' '}
						{item?.valueTo} {item.measurementUnits}
					</Text>
				</View>
			</View>
			{!lastItem && <HorizontalLine />}
		</>
	);
}
