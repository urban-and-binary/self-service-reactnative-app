import React, { useState } from 'react';
import { useNavigation, useRoute } from '@react-navigation/native';
import { inject, observer } from 'mobx-react';
import {
	BackButton,
	Button,
	Card,
	Screen,
	Text,
	TextInput,
	View,
	DropDown,
} from '../theme';
import { theme } from '../theme/themeConfig';
import { metersConfig } from './metersConfig';
import { useMetersReading } from './api';
import { queryClient } from '../reactQuery/queryClient';

const MetersDeclareScreen = inject('store')(
	observer(({ store: { setToast } }) => {
		const {
			params: { ownedUnitMeters },
		} = useRoute();
		const navigation = useNavigation();
		const declareMeters = useMetersReading({
			onSuccess: () => {
				queryClient.invalidateQueries('useOwnedUnitsMeters');
				setToast('screens.meters.addReadingToast', 5, 'success');
				navigation.goBack();
			},
		});
		const [currentUnit, setCurrentUnit] = useState(ownedUnitMeters[0]);
		const [readingData, setReadingData] = useState({});
		const handleChangeReadingData = (id, valueTo) => {
			setReadingData(prevState => ({ ...prevState, [id]: valueTo }));
		};

		const renderMeter = item => (
			<Meter
				key={item.id}
				meter={item}
				onChange={handleChangeReadingData}
			/>
		);
		const handleSubmit = () => {
			const meters = Object.keys(readingData)
				.map(id => ({
					id,
					valueTo: readingData[id],
				}))
				.filter(meter => !!meter.valueTo);
			declareMeters.mutate({ meters });
		};

		return (
			<Screen
				preset="scroll"
				paddingHorizontal={theme.pads.horizontalPad}>
				<View paddingVertical={theme.pads.screenVerticalPad}>
					<BackButton
						paddingTopVertical={100}
						showLabel
						label="screens.meters.title"
					/>
					<DropDown
						onChange={setCurrentUnit}
						initialValue={currentUnit.label}
						marginTop={theme.pads.sectionVerticalPad}
						label="screens.meters.chooseAUnit"
						listData={ownedUnitMeters}
						showExteriorLabel
					/>

					{currentUnit.meters.map(renderMeter)}
					<Button
						loading={declareMeters.loading}
						onPress={handleSubmit}
						marginTop={theme.pads.sectionVerticalPad}
						label="general.form.declare"
					/>
				</View>
			</Screen>
		);
	}),
);

function Meter({ meter, onChange }) {
	const item = metersConfig[meter.localType];

	// valueTo in previous meters cycle should be valueFrom in new cycle
	const nextCycleValueFrom = meter.valueTo;
	const [to, setTo] = useState(nextCycleValueFrom);

	const checkTo = () => {
		if (parseInt(to, 10) < nextCycleValueFrom) setTo(nextCycleValueFrom);
	};

	const handleChangeTo = value => {
		setTo(value);
		onChange(
			item.id,
			parseInt(value, 10) > nextCycleValueFrom ? value : undefined,
		);
	};

	return (
		<Card marginTop={20}>
			<View row justifyContent="space-between" alignItems="center">
				<View row alignItems="flex-start">
					<View marginTop={5} marginRight={10}>
						{item.icon}
					</View>
					<View>
						<Text variant={theme.typo.fontVariants.baseBold}>
							{meter.type}
						</Text>
					</View>
				</View>

				<Text
					color={theme.palette.baseColors.grey}
					variant={theme.typo.fontVariants.smallRegular}>
					({meter.name})
				</Text>
			</View>
			<View>
				<View
					paddingVertical={20}
					row
					alignItems="center"
					justifyContent="space-between">
					<Text
						variant={theme.typo.fontVariants.smallBold}
						color={theme.palette.baseColors.grey}>
						Quantity (from)
					</Text>
					<Text
						variant={theme.typo.fontVariants.baseRegular}
						color={theme.palette.baseColors.base1}>
						{nextCycleValueFrom} {meter.measurementUnits}
					</Text>
				</View>
				<View
					paddingVertical={20}
					row
					alignItems="center"
					justifyContent="space-between">
					<Text
						variant={theme.typo.fontVariants.smallBold}
						color={theme.palette.baseColors.grey}>
						Quantity (to)
					</Text>
					<View flex marginLeft={30}>
						<TextInput
							onEndEditing={checkTo}
							onChangeText={handleChangeTo}
							unit={meter.measurementUnits}
							value={to.toString()}
							keyboardType="numeric"
							placeholder="to"
						/>
					</View>
				</View>
				<View
					paddingVertical={20}
					row
					alignItems="center"
					justifyContent="space-between">
					<Text
						variant={theme.typo.fontVariants.smallBold}
						color={theme.palette.baseColors.grey}>
						Difference
					</Text>
					<Text
						variant={theme.typo.fontVariants.baseRegular}
						color={theme.palette.baseColors.base1}>
						{Math.max(0, to - nextCycleValueFrom)}{' '}
						{meter.measurementUnits}
					</Text>
				</View>
			</View>
		</Card>
	);
}

export default MetersDeclareScreen;
