import React from 'react';
import { useNavigation } from '@react-navigation/native';
import { DotIndicator } from 'react-native-indicators';
import { Button, Screen, Text, View } from '../theme';
import { theme } from '../theme/themeConfig';
import { UnitMetersItem } from './UnitMetersItem';
import { useOwnedUnitsMeters } from './api';
import { useOwnedUnits } from '../Units/api';
import GeneralEmptyState from '../../assets/images/empty-states/GeneralEmptyState';

export default function MetersScreen() {
	const navigation = useNavigation();
	const handleOpenDeclareScreen = () =>
		navigation.navigate('MetersDeclare', { ownedUnitMeters });
	const handleOpenUnitsScreen = () => navigation.navigate('newUnit');
	const { data: ownedUnitMeters = [] } = useOwnedUnitsMeters();
	const { data: ownedUnits = [] } = useOwnedUnits();
	const renderOwnedUnitMeters = item => (
		<UnitMetersItem
			key={item._id}
			marginTop={theme.pads.sectionVerticalPad}
			unit={item}
		/>
	);
	if (ownedUnits.length === 0) {
		return (
			<Screen
				preset="scroll"
				topBar
				paddingHorizontal={theme.pads.horizontalPad}>
				<Text
					paddingBottom={theme.pads.sectionVerticalPad}
					variant={theme.typo.fontVariants.h1Bold}>
					screens.meters.title
				</Text>
				<View marginTop="10%" width="100%" center>
					<View opacity={0.2}>
						<GeneralEmptyState />
					</View>
					<Text color={theme.palette.baseColors.base4}>
						screens.meters.emptyState
					</Text>
					<Text paddingVertical={10} onPress={handleOpenUnitsScreen}>
						screens.meters.emptyStateActions
					</Text>
				</View>
			</Screen>
		);
	}
	return (
		<Screen
			preset="scroll"
			topBar
			paddingHorizontal={theme.pads.horizontalPad}>
			<Text
				paddingBottom={theme.pads.sectionVerticalPad}
				variant={theme.typo.fontVariants.h1Bold}>
				screens.meters.title
			</Text>
			{ownedUnitMeters.length === 0 && (
				<View marginTop="50%" width="100%" center>
					<DotIndicator
						size={8}
						count={4}
						color={theme.palette.primaryColors.color2}
					/>
				</View>
			)}
			{ownedUnitMeters.length > 0 && (
				<Button
					onPress={handleOpenDeclareScreen}
					label="general.form.declare"
				/>
			)}
			{ownedUnitMeters.map(renderOwnedUnitMeters)}
		</Screen>
	);
}
