import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { defaultScreenOptions } from '../Navigation/navigationConfig';
import MetersScreen from './MetersScreen';
import MetersDeclareScreen from './MetersDeclareScreen';
import { drawerScreensArray } from '../Drawer/DrawerNavigation';

const Navigation = createNativeStackNavigator();

function MetersNavigation() {
	return (
		<Navigation.Navigator screenOptions={defaultScreenOptions}>
			<Navigation.Screen name="MetersUnits" component={MetersScreen} />
			<Navigation.Screen
				name="MetersDeclare"
				component={MetersDeclareScreen}
			/>
			{drawerScreensArray.map(item => item)}
		</Navigation.Navigator>
	);
}
export default MetersNavigation;
