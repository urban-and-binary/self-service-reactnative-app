import { useMutation, useQuery } from '../reactQuery/hooks';

export const useOwnedUnitsMeters = (option = {}) =>
	useQuery('useOwnedUnitsMeters', 'owned-unit/meters', option);
export const useMetersReading = (option = {}) =>
	useMutation('meters/reading', option);
