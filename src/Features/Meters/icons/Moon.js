import * as React from 'react';
import Svg, { Path } from 'react-native-svg';

function MoonIcon(props) {
	return (
		<Svg
			width={16}
			height={16}
			viewBox="0 0 16 16"
			fill="none"
			xmlns="http://www.w3.org/2000/svg"
			{...props}>
			<Path
				d="M14.75 8.592A6.75 6.75 0 117.407 1.25a5.25 5.25 0 007.343 7.342v0z"
				stroke="#005AC8"
				strokeWidth={1.5}
				strokeLinecap="round"
				strokeLinejoin="round"
			/>
		</Svg>
	);
}

export default MoonIcon;
