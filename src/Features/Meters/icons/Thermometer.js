import * as React from 'react';
import Svg, { Path } from 'react-native-svg';

function ThermometerIcon(props) {
	return (
		<Svg
			width={18}
			height={18}
			viewBox="0 0 18 18"
			fill="none"
			xmlns="http://www.w3.org/2000/svg"
			{...props}>
			<Path
				d="M8.438 14.625a1.406 1.406 0 100-2.812 1.406 1.406 0 000 2.812zM8.438 11.813V8.437M14.906 3.375v2.25M12.766 4.93l2.14.695M13.584 7.445l1.322-1.82M16.229 7.445l-1.323-1.82M17.046 4.93l-2.14.695"
				stroke="#005AC8"
				strokeWidth={1.5}
				strokeLinecap="round"
				strokeLinejoin="round"
			/>
			<Path
				d="M6.188 10.338V3.375a2.25 2.25 0 114.5 0v6.963-.001a3.657 3.657 0 11-4.501 0h0z"
				stroke="#005AC8"
				strokeWidth={1.5}
				strokeLinecap="round"
				strokeLinejoin="round"
			/>
		</Svg>
	);
}

export default ThermometerIcon;
