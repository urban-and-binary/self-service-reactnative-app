import * as React from 'react';
import Svg, { Path } from 'react-native-svg';

function SunIcon(props) {
	return (
		<Svg
			width={18}
			height={18}
			viewBox="0 0 18 18"
			fill="none"
			xmlns="http://www.w3.org/2000/svg"
			{...props}>
			<Path
				d="M9 13.219A4.219 4.219 0 109 4.78a4.219 4.219 0 000 8.438zM9 2.531V1.125M4.426 4.426l-.994-.994M2.531 9H1.125M4.426 13.574l-.994.995M9 15.469v1.406M13.574 13.574l.995.995M15.469 9h1.406M13.574 4.426l.995-.994"
				stroke="#FFC400"
				strokeWidth={1.5}
				strokeLinecap="round"
				strokeLinejoin="round"
			/>
		</Svg>
	);
}

export default SunIcon;
