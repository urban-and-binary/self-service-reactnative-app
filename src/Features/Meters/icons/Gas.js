import * as React from 'react';
import Svg, { Path } from 'react-native-svg';

function GasIcon(props) {
	return (
		<Svg
			width={13}
			height={18}
			viewBox="0 0 13 18"
			fill="none"
			xmlns="http://www.w3.org/2000/svg"
			{...props}>
			<Path
				d="M6.625 16.5C3.519 16.5 1 14.563 1 11.25v-.066c0-1.588.836-2.859 2.11-3.684a3.934 3.934 0 001.757-3.75L4.516 1.5l1.303.596c2.349 1.073 4.304 2.934 5.572 5.3a7.319 7.319 0 01.859 3.452v.402c0 1.171-.315 2.171-.86 2.974"
				stroke="#00BE73"
				strokeWidth={1.5}
				strokeLinecap="round"
				strokeLinejoin="round"
			/>
			<Path
				d="M6.625 16.5c-1.036 0-1.875-1.075-1.875-2.4 0-1.05.635-1.89 1.194-2.661l.681-.939.681.939C7.866 12.21 8.5 13.05 8.5 14.1c0 1.325-.84 2.4-1.875 2.4z"
				stroke="#00BE73"
				strokeWidth={1.5}
				strokeLinecap="round"
				strokeLinejoin="round"
			/>
		</Svg>
	);
}

export default GasIcon;
