import * as React from 'react';
import Svg, { Path } from 'react-native-svg';

function ThermometerHotIcon(props) {
	return (
		<Svg
			width={24}
			height={24}
			viewBox="0 0 24 24"
			fill="none"
			xmlns="http://www.w3.org/2000/svg"
			{...props}>
			<Path
				d="M11.25 19.5a1.875 1.875 0 100-3.75 1.875 1.875 0 000 3.75zM11.25 15.75V4.5M22.527 7.5a1.875 1.875 0 01-2.652 0 1.875 1.875 0 00-2.652 0M22.527 11.25a1.875 1.875 0 01-2.652 0 1.875 1.875 0 00-2.652 0"
				stroke="#EB5757"
				strokeWidth={1.5}
				strokeLinecap="round"
				strokeLinejoin="round"
			/>
			<Path
				d="M8.25 13.784V4.5a3 3 0 116 0v9.284-.001a4.875 4.875 0 11-6 0h0z"
				stroke="#EB5757"
				strokeWidth={1.5}
				strokeLinecap="round"
				strokeLinejoin="round"
			/>
		</Svg>
	);
}

export default ThermometerHotIcon;
