import * as React from 'react';
import Svg, { Path } from 'react-native-svg';

function Electricity(props) {
	return (
		<Svg
			width={14}
			height={18}
			viewBox="0 0 14 18"
			fill="none"
			xmlns="http://www.w3.org/2000/svg"
			{...props}>
			<Path
				d="M4.75 16.875l1.125-5.625-4.5-1.688L9.25 1.126 8.125 6.75l4.5 1.688-7.875 8.437z"
				stroke="#00BE73"
				strokeWidth={1.5}
				strokeLinecap="round"
				strokeLinejoin="round"
			/>
		</Svg>
	);
}

export default Electricity;
