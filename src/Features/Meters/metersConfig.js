import React from 'react';
import ThermometerIcon from './icons/Thermometer';
import GasIcon from './icons/Gas';
import ThermometerHotIcon from './icons/ThermometerHot';
import Electricity from './icons/Electricity';

export const metersConfig = {
	coldWater: {
		id: 'coldWater',
		title: 'Cold Water',
		unit: 'm3',
		icon: <ThermometerIcon />,
	},
	hotWater: {
		id: 'hotWater',
		title: 'Hot Water',
		unit: 'm3',
		icon: <ThermometerHotIcon />,
	},
	electricity: {
		id: 'electricity',
		title: 'Electricity',
		unit: 'kWh',
		icon: <Electricity />,
	},
	heating: {
		id: 'heating',
		title: 'Heating',
		unit: 'm3',
		icon: <GasIcon />,
	},
};
