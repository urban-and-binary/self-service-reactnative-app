import React, { useEffect, useState } from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { NavigationContainer } from '@react-navigation/native';
import { inject, observer, Provider as StoreProvider } from 'mobx-react';
import { QueryClientProvider } from 'react-query';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { defaultScreenOptions } from '../Navigation/navigationConfig';
import { queryClient } from '../reactQuery/queryClient';
import rootStore, { Provider } from '../store/store';
import AuthNavigation from '../Navigation/AuthNavigation';
import RootNavigation from '../Navigation/RootNavigation';
import SplashScreen from '../Splash/Splash';
import ErrorBoundary from '../ErrorBondary/ErrorBondary';
import { Toast } from '../theme';
import {
	ON_BOARDING_STORAGE_KEY,
	onBoardingStatuses,
} from '../Onboarding/additionalInfoConfig';

const AppNavigator = createNativeStackNavigator();

const App = inject('store')(
	observer(({ store }) => {
		const { auth, isLoaded, toastMessage } = store;
		const [isOnboarded, setIsOnboarded] = useState(
			onBoardingStatuses.notOnboarded,
		);

		useEffect(() => {
			const checkOnboardingState = async () => {
				try {
					AsyncStorage.getItem(ON_BOARDING_STORAGE_KEY).then(
						value => {
							setIsOnboarded(value);
						},
					);
				} catch (e) {
					console.warn('reading onboarded state failed');
				}
			};
			checkOnboardingState();
		}, []);
		return (
			<>
				<AppNavigator.Navigator screenOptions={defaultScreenOptions}>
					{!isLoaded || isOnboarded === 'false' ? (
						<AppNavigator.Screen
							name="Splash"
							component={SplashScreen}
						/>
					) : auth?.token ? (
						<AppNavigator.Screen
							name="Main"
							component={RootNavigation}
						/>
					) : (
						<AppNavigator.Screen
							name="Login"
							component={AuthNavigation}
							initialParams={{ isOnboarded }}
						/>
					)}
				</AppNavigator.Navigator>
				{toastMessage?.message.length > 0 && (
					<Toast message={toastMessage?.message} />
				)}
			</>
		);
	}),
);

function Root() {
	return (
		<SafeAreaProvider>
			<ErrorBoundary>
				<StoreProvider store={rootStore}>
					<QueryClientProvider client={queryClient}>
						<Provider value={rootStore}>
							<NavigationContainer>
								<App />
							</NavigationContainer>
						</Provider>
					</QueryClientProvider>
				</StoreProvider>
			</ErrorBoundary>
		</SafeAreaProvider>
	);
}

export default Root;
