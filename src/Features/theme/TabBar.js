import React from 'react';
import {
	ViewPropTypes,
	TouchableOpacity,
	StyleSheet,
	Appearance,
} from 'react-native';
import PropTypes from 'prop-types';

import { theme } from './themeConfig';
import { View } from './View';
import { Text } from './Text';

const colorScheme = Appearance.getColorScheme();

export function TabBar({
	activeTab,
	tabs,
	onChange,
	style,
	disabledIndexes = [],
	...props
}) {
	return (
		<View
			style={style}
			borderRadius={theme.roundness.full}
			row
			width="100%"
			backgroundColor={theme.paletteAuto.background.secondary}
			justifyContent="center"
			{...props}>
			{tabs.map((item, index) => (
				<TabItem
					key={index}
					disabled={disabledIndexes.includes(index)}
					onPress={() => onChange(item)}
					isActive={item.toLowerCase() === activeTab?.toLowerCase()}
					title={item}
				/>
			))}
		</View>
	);
}

function TabItem({ title, isActive, onPress, disabled }) {
	return (
		<TouchableOpacity
			disabled={disabled}
			onPress={onPress}
			style={[
				isActive
					? styles.activeComponentText
					: styles.inactiveComponentText,
				styles.itemContainer,
			]}>
			<Text
				color={
					disabled
						? 'gray'
						: isActive
						? theme.palette.baseColors.base7
						: theme.palette.baseColors.base3
				}
				variant={theme.typo.fontVariants.smallBold}>
				{title}
			</Text>
		</TouchableOpacity>
	);
}

const styles = StyleSheet.create({
	container: {},
	inactiveComponentText: {},
	activeComponentText: {
		backgroundColor: theme.palette.primaryColors.color2,
		borderRadius: theme.roundness.full,
		...theme.shadows.deep,
	},
	itemContainer: {
		flex: 1,
		alignItems: 'center',
		paddingVertical: 10,
	},
	underlineInactive: {
		borderBottomColor: theme.palette[colorScheme].border.primary,
	},
	underlineActive: {
		borderBottomColor: theme.palette[colorScheme].text.secondary,
	},
});

TabBar.propTypes = {
	onChange: PropTypes.func,
	tabs: PropTypes.arrayOf(PropTypes.string),
	style: ViewPropTypes.style,
	activeTab: PropTypes.string,
};
