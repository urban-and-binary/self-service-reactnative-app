import React from 'react';
import { StyleSheet } from 'react-native';
import { Modal, Button, Card, Text } from '.';
import { theme } from './themeConfig';

export function ConfirmationModal({
	isVisible,
	onRequestClose,
	primaryButtonLabel = 'general.form.confirm',
	primaryButtonAction,
	secondaryButtonLabel = 'general.form.cancel',
	secondaryButtonAction,
	title,
	description,
	style,
	contentContainerProps,
	...props
}) {
	const modalStyle = { ...defaultStyle.modal, ...style };
	return (
		<Modal
			isVisible={isVisible}
			onRequestClose={onRequestClose}
			style={modalStyle}
			{...props}>
			<Card {...contentContainerProps}>
				<Text
					variant={theme.typo.fontVariants.baseBold}
					textAlign="center">
					{title}
				</Text>
				<Text
					paddingVertical={20}
					variant={theme.typo.fontVariants.smallRegular}
					textAlign="center">
					{description}
				</Text>
				<Button
					onPress={primaryButtonAction}
					label={primaryButtonLabel}
					marginBottom={15}
				/>
				<Button
					onPress={secondaryButtonAction}
					label={secondaryButtonLabel}
					outline
				/>
			</Card>
		</Modal>
	);
}
const defaultStyle = StyleSheet.create({
	modal: {
		justifyContent: 'flex-end',
		marginBottom: '20%',
	},
});
