import React, { useState } from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { View, Text } from './index';
import { theme } from './themeConfig';
import { HorizontalLine } from './HorizontalLine';

export function Card({
	title,
	showTitleHorizontalLine = true,
	expandable,
	defaultExpand = true,
	children,
	shadow = theme.shadows.xxDeep,
	backgroundColor = theme.palette.baseColors.base7,
	...props
}) {
	const [expanded, setExpanded] = useState(defaultExpand);
	const handleExpansion = () => setExpanded(!expanded);
	return (
		<View
			style={{ ...defaultStyles.container, ...shadow }}
			backgroundColor={backgroundColor}
			{...props}>
			{title && (
				<TouchableOpacity
					disabled={!expandable}
					onPress={handleExpansion}>
					<View
						row
						alignItems="center"
						justifyContent="space-between">
						<Text variant={theme.typo.fontVariants.baseBold}>
							{title}
						</Text>
						{expandable && (
							<View
								onPress={handleExpansion}
								paddingVertical={10}
								paddingHorizontal={10}>
								<Ionicons
									color={theme.palette.baseColors.base1}
									size={theme.typo.fontSize.xxLarge}
									name={
										expanded ? 'chevron-up' : 'chevron-down'
									}
								/>
							</View>
						)}
					</View>
				</TouchableOpacity>
			)}
			{((expandable && expanded) || !expandable) && (
				<>
					{!!title && showTitleHorizontalLine && <HorizontalLine />}
					{children}
				</>
			)}
		</View>
	);
}

const defaultStyles = StyleSheet.create({
	container: {
		borderRadius: theme.roundness.large,
		paddingHorizontal: theme.pads.horizontalPad,
		paddingVertical: theme.pads.horizontalPad,
		marginBottom: 20,
	},
});
