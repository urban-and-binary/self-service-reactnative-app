import React, { useEffect } from 'react';
import {
	View as RNView,
	LayoutAnimation,
	ViewPropTypes,
	TouchableOpacity,
	Platform,
} from 'react-native';
import PropTypes from 'prop-types';
import LinearGradient from 'react-native-linear-gradient';
import Ripple from 'react-native-material-ripple';
import { theme } from './themeConfig';

export function View({
	rippleColor = theme.palette.primaryColors.color2,
	rippleOpacity = 0.1,
	children,
	animated,
	flex,
	center,
	backgroundColor,
	row,
	style,
	justifyContent,
	alignItems,
	paddingTop,
	paddingBottom,
	paddingLeft,
	paddingRight,
	paddingVertical,
	paddingHorizontal,
	marginTop,
	marginBottom,
	marginLeft,
	marginRight,
	marginVertical,
	marginHorizontal,
	width,
	height,
	borderRadius,
	onPress,
	onPressDisabled,
	position,
	gradientColors = [],
	...props
}) {
	const styleProp = {
		justifyContent: (!!center && 'center') || justifyContent || undefined,
		alignItems: (!!center && 'center') || alignItems || undefined,
		paddingTop,
		paddingBottom,
		paddingLeft,
		paddingRight,
		paddingVertical,
		paddingHorizontal,
		marginTop,
		marginBottom,
		marginLeft,
		marginRight,
		marginVertical,
		marginHorizontal,
		width,
		height,
		borderRadius,
		position,
		backgroundColor,
		flex: flex ? 1 : undefined,
		flexDirection: row ? 'row' : undefined,
		gradientColors,
		...style,
	};

	useEffect(() => {
		animated && LayoutAnimation.spring();
	}, [animated]);

	if (onPress && Platform.OS === 'android') {
		return (
			<Ripple
				disabled={onPressDisabled}
				rippleColor={rippleColor}
				rippleOpacity={rippleOpacity}
				style={{
					borderRadius: styleProp?.borderRadius || undefined,
				}}
				onPress={onPress}>
				<ContentView
					colors={gradientColors}
					style={styleProp}
					{...props}>
					{children}
				</ContentView>
			</Ripple>
		);
	}

	if (onPress && Platform.OS === 'ios') {
		return (
			<TouchableOpacity disabled={onPressDisabled} onPress={onPress}>
				<ContentView
					colors={gradientColors}
					style={styleProp}
					{...props}>
					{children}
				</ContentView>
			</TouchableOpacity>
		);
	}
	return (
		<ContentView colors={gradientColors} style={styleProp} {...props}>
			{children}
		</ContentView>
	);
}

function ContentView({ children, colors, ...props }) {
	if (colors?.length !== 0) {
		return (
			<LinearGradient colors={colors} {...props}>
				{children}
			</LinearGradient>
		);
	}
	return <RNView {...props}>{children}</RNView>;
}

View.propTypes = {
	style: ViewPropTypes.style,
	animated: PropTypes.bool,
	children: PropTypes.node,
	flex: PropTypes.bool,
	center: PropTypes.bool,
	row: PropTypes.bool,
	gradientColors: PropTypes.arrayOf(PropTypes.string),
};
