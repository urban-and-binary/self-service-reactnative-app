import React from 'react';
import { useTranslation } from '../translation/useTranslation';
import { TextInputControlled } from './TextInputControlled';
import { isPhoneValidated } from './validationUtil';

export function TextInputPhone({ error, placeholder, ...props }) {
	const t = useTranslation();

	return (
		<TextInputControlled
			validator={isPhoneValidated}
			hideUnderline
			autoComplete="tel"
			keyboardType="phone-pad"
			autoCorrect={false}
			autoCapitalize="none"
			clearButtonMode="while-editing"
			spellCheck={false}
			validationError="general.errors.incorrectPhone"
			placeholder={placeholder ?? t('general.form.phoneNumber')}
			{...props}
		/>
	);
}
