import React, { useRef, useEffect, useState } from 'react';
import { Animated } from 'react-native';
import { View } from './View';

export function SwipeAnimation({
	onPress,
	children,
	duration = 1000,
	visible = true,
	delay = 10,
	distance,
	...props
}) {
	const swipeRef = useRef(new Animated.Value(0)).current;
	const [animating, setAnimating] = useState(0);

	useEffect(() => {
		Animated.timing(swipeRef, {
			toValue: visible === false ? 0 : 1,
			duration,
			useNativeDriver: true,
			delay: delay || 0,
		}).start();
	}, [delay, duration, swipeRef, visible]);

	useEffect(() => {
		swipeRef.addListener(({ value }) => {
			setAnimating(value);
		});

		return () => {
			swipeRef.removeAllListeners();
		};
	}, [swipeRef]);

	return (
		<Animated.View
			onPress={onPress}
			style={{
				opacity: swipeRef,
				marginTop: animating * distance - distance,
			}}>
			<View {...props}>{children}</View>
		</Animated.View>
	);
}
export default SwipeAnimation;
