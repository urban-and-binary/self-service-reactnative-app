export function isEmailValidated(email) {
	const validEmailRegex =
		/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
	return email.toLowerCase().match(validEmailRegex);
}

export function isPhoneValidated(phone) {
	const validPhoneRegex = /^(?:[+].*\d|\d)$/;
	return phone.toLowerCase().match(validPhoneRegex);
}

export function hasOneCapitalLetter(text) {
	const regex = /[A-Z]+/;
	return regex.test(text);
}

export function hasOneNumber(text) {
	const regex = /[0-9]+/;
	return regex.test(text);
}
