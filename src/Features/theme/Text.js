import React from 'react';
import { Appearance, Text as RNText, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import { isLightinColorValid, theme } from './themeConfig';
import { useTranslation } from '../translation/useTranslation';

export function Text({
	children,
	style,
	color,
	weight,
	size,
	variant,
	paddingTop,
	paddingBottom,
	paddingLeft,
	paddingRight,
	paddingVertical,
	paddingHorizontal,
	onPress,
	textAlign,
	flex,
	width,
	height,
	...props
}) {
	const colorScheme = Appearance.getColorScheme();
	const t = useTranslation();
	const styleProp = {
		color:
			(color &&
				(isLightinColorValid(color) ? color[colorScheme] : color)) ||
			(!!onPress && theme.palette.primaryColors.color2) ||
			style?.color ||
			theme.paletteAuto.text.primary,
		fontSize:
			style?.fontSize ||
			size ||
			variant?.fontSize ||
			theme.typo.fontSize.small,
		fontFamily: weightGenerator(
			weight || variant?.weight || theme.typo.fontWeight.medium,
		),
		lineHeight: variant?.lineHeight || undefined,
		flex: flex ? 1 : 0,
		width,
		height,
		paddingLeft,
		paddingRight,
		paddingTop,
		paddingBottom,
		paddingVertical,
		paddingHorizontal,
		textAlign,
		...style,
	};

	if (onPress) {
		return (
			<TouchableOpacity onPress={onPress}>
				<RNText style={[style, styleProp]} {...props}>
					{(React.isValidElement(children) && children) ||
						t(children)}
				</RNText>
			</TouchableOpacity>
		);
	}

	return (
		<RNText style={[style, styleProp]} {...props}>
			{(React.isValidElement(children) && children) || t(children)}
		</RNText>
	);
}

// Based on https://github.com/facebook/react-native/issues/26193 issue
// waiting for official release https://github.com/facebook/react-native/pull/29117
// this is a temporary solution
const weightGenerator = weight => {
	switch (weight) {
		case theme.typo.fontWeight.normal:
			return theme.typo.fontFamily.regular;
		case theme.typo.fontWeight.bold:
			return theme.typo.fontFamily.bold;
		case 100:
			return theme.typo.fontFamily.thin;
		case 200:
			return theme.typo.fontFamily.extraLight;
		case 300:
			return theme.typo.fontFamily.light;
		case 400:
			return theme.typo.fontFamily.regular;
		case 500:
			return theme.typo.fontFamily.medium;
		case 600:
			return theme.typo.fontFamily.semiBold;
		case 700:
			return theme.typo.fontFamily.bold;
		case 800:
			return theme.typo.fontFamily.extraBold;
		case 900:
			return theme.typo.fontFamily.black;
		default:
			return theme.typo.fontFamily.semiBold;
	}
};

Text.propTypes = {
	size: PropTypes.number,
	color: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
	children: PropTypes.node,
	weight: PropTypes.oneOf(['bold', 'normal', 300, 400, 500, 600, 700]),
	// style: ViewPropTypes.style,//color is not included in
};
