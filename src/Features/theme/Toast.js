import React, { useEffect } from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { useMst } from '../store/store';
import { Text, View } from './index';
import { theme } from './themeConfig';

export function Toast({ style }) {
	const { assign, toastMessage, clearToast } = useMst();

	useEffect(() => {
		const clearToastTimeout = setTimeout(
			() => clearToast(),
			toastMessage.duration * 1000,
		);
		return () => clearTimeout(clearToastTimeout);
	}, [clearToast, toastMessage.duration]);

	let iconName;
	let iconColor;
	switch (toastMessage.type) {
		case 'success':
			iconName = 'checkmark-circle';
			iconColor = theme.palette.statusColors.success;
			break;
		case 'warning':
			iconName = 'information-circle';
			iconColor = theme.palette.statusColors.warning;
			break;
		case 'error':
			iconName = 'close-circle';
			iconColor = theme.palette.statusColors.danger;
			break;
		case 'info':
			iconName = 'information-circle';
			iconColor = theme.palette.primaryColors.color2;
			break;
		default:
			iconName = 'close-circle';
			iconColor = theme.palette.statusColors.danger;
			break;
	}

	return (
		<View
			style={{
				...styles.container,
				...style,
			}}>
			<Ionicons
				size={theme.typo.fontSize.large}
				style={styles.icon}
				name={iconName}
				color={iconColor}
			/>
			<Text
				variant={theme.typo.fontVariants.smallRegular}
				color={theme.palette.baseColors.base3}
				style={styles.message}
				desc>
				{toastMessage.message}
			</Text>
			<TouchableOpacity
				onPress={() =>
					assign({ toastMessage: { message: '', duration: 5 } })
				}
				style={styles.closeContainer}>
				<Ionicons
					name="close"
					color={theme.palette.baseColors.base3}
					size={theme.typo.fontSize.large}
				/>
			</TouchableOpacity>
		</View>
	);
}

const styles = StyleSheet.create({
	container: {
		borderRadius: theme.roundness.medium,
		position: 'absolute',
		top: 80,
		right: theme.pads.screenHorizontalPad,
		left: theme.pads.screenHorizontalPad,
		flexDirection: 'row',
		alignItems: 'flex-start',
		backgroundColor: theme.paletteAuto.background.primary,
		...theme.shadows.normal,
	},
	message: {
		flex: 1,
		paddingTop: 15,
		paddingBottom: 15,
	},
	closeContainer: {
		padding: 15,
	},
	icon: {
		padding: 15,
		...theme.shadows.deep,
	},
});
