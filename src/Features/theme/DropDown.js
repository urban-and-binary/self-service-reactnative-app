import React, {
	forwardRef,
	useEffect,
	useImperativeHandle,
	useState,
} from 'react';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { FlatList, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import { CircleButton, Text, TextInput, View, Modal } from './index';
import { theme } from './themeConfig';

export const DropDown = forwardRef(
	(
		{
			error,
			label,
			initialValue,
			listData,
			listProps = {},
			onChange,
			searchEnabled,
			onSearchTextChanged,
			modalHeight,
			pressEnabled = true,
			showExteriorLabel = false,
			exteriorLabel,
			itemLabelProperty = 'label',
			...props
		},
		ref,
	) => {
		const [value, setValue] = useState(initialValue);
		const [expanded, setExpanded] = useState(false);
		useImperativeHandle(ref, () => ({
			clear() {
				setValue(initialValue);
			},
		}));
		const handleToggleModal = () => setExpanded(!expanded);
		const handleCloseModal = () => setExpanded(false);
		const handleSelectItem = item => {
			setValue(item[itemLabelProperty]);
			!!onChange && onChange(item);
			setExpanded(false);
		};
		return (
			<View {...props}>
				{showExteriorLabel && (
					<Text
						variant={theme.typo.fontVariants.smallBold}
						color={theme.palette.baseColors.base1}>
						{exteriorLabel || label}
					</Text>
				)}
				<View
					onPress={pressEnabled && handleToggleModal}
					row
					style={error ? defaultStyles.errorContainer : {}}
					justifyContent="space-between"
					borderRadius={theme.roundness.large}
					paddingHorizontal={20}
					paddingVertical={17}
					backgroundColor={theme.palette.baseColors.base6}>
					<Text
						variant={theme.typo.fontVariants.smallBold}
						color={
							value
								? theme.palette.baseColors.base1
								: theme.palette.baseColors.grey
						}>
						{value || label}
					</Text>
					<Ionicons
						color={theme.palette.baseColors.base1}
						size={theme.typo.fontSize.large}
						name={expanded ? 'chevron-up' : 'chevron-down'}
					/>
					<DropDownModal
						{...{
							itemLabelProperty,
							modalHeight,
							searchEnabled,
							onSearchTextChanged,
							listProps,
							listData,
						}}
						onChange={handleSelectItem}
						onRequestClose={handleCloseModal}
						visible={expanded}
					/>
				</View>
				{!!error && (
					<Text
						paddingTop={2}
						variant={theme.typo.fontVariants.xsRegular}
						color={theme.palette.statusColors.danger}>
						{error}
					</Text>
				)}
			</View>
		);
	},
);

function DropDownModal({
	visible,
	onRequestClose,
	children,
	task,
	listData,
	listProps,
	showCloseButton = true,
	onChange,
	searchEnabled,
	onSearchTextChanged,
	modalHeight,
	itemLabelProperty,
	...props
}) {
	const [filteredData, setFilteredData] = useState(listData || []);
	const renderListItem = ({ item }) => {
		if (listProps?.renderItem) return listProps.renderItem(item, onChange);
		return (
			<View
				onPress={() => !!onChange && onChange(item)}
				backgroundColor={theme.palette.baseColors.base6}
				borderRadius={theme.roundness.medium}
				marginTop={theme.pads.screenVerticalPad}
				paddingVertical={theme.pads.pressablePad * 2}
				paddingHorizontal={theme.pads.pressablePad}>
				<Text>{item[itemLabelProperty]}</Text>
			</View>
		);
	};

	useEffect(() => {
		setFilteredData(listData);
	}, [listData]);

	const handleSearchChanged = value => {
		if (onSearchTextChanged) {
			onSearchTextChanged(value);
		} else {
			setFilteredData(
				(listData || [])?.filter(item =>
					item?.label.includes(value.toString()),
				),
			);
		}
	};

	useEffect(() => {
		!visible && setFilteredData(listData);
	}, [listData, visible]);

	return (
		<Modal
			onRequestClose={onRequestClose}
			isVisible={visible}
			style={defaultStyles.dropdownModal}
			{...props}>
			{showCloseButton && (
				<CircleButton
					onPress={onRequestClose}
					size={theme.typo.fontSize.medium}
					marginBottom={10}
					style={{ alignSelf: 'flex-end' }}
				/>
			)}

			<View
				height={modalHeight}
				backgroundColor={theme.palette.baseColors.base7}
				borderRadius={theme.roundness.medium}
				paddingHorizontal={theme.pads.horizontalPad}
				paddingVertical={theme.pads.horizontalPad}>
				{searchEnabled && (
					<TextInput
						placeholder="Search"
						onChangeText={handleSearchChanged}
					/>
				)}
				<FlatList
					{...listProps}
					data={filteredData}
					renderItem={renderListItem}
					showsVerticalScrollIndicator={false}
				/>
			</View>
		</Modal>
	);
}
const defaultStyles = StyleSheet.create({
	dropdownModal: {
		justifyContent: 'center',
		margin: 0,
		flex: 1,
		paddingVertical: theme.pads.horizontalPad * 3,
		paddingHorizontal: theme.pads.horizontalPad,
	},
	errorContainer: {
		borderColor: theme.palette.statusColors.danger,
		borderWidth: 1,
		borderStyle: 'solid',
	},
});

DropDown.propTypes = {
	listData: PropTypes.arrayOf(
		PropTypes.shape({
			label: PropTypes.string,
			value: PropTypes.string,
		}),
	),
};
