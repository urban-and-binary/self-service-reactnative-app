import React from 'react';
import { Platform, StyleSheet } from 'react-native';
import { View } from './View';
import { theme } from './themeConfig';

export function HorizontalLine() {
	return <View style={defaultStyles.line} />;
}

const defaultStyles = StyleSheet.create({
	line: {
		width: '100%',
		borderBottomWidth: 1,
		borderBottomColor: theme.palette.baseColors.base4,
		borderStyle: Platform.OS === 'android' ? 'dashed' : 'solid',
		paddingBottom: 15,
		marginBottom: 20,
	},
});
