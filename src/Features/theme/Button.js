import React, { useCallback } from 'react';
import { ActivityIndicator, StyleSheet, ViewPropTypes } from 'react-native';
import PropTypes from 'prop-types';
import { useNavigation } from '@react-navigation/native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Feather from 'react-native-vector-icons/Feather';
import { Text, View } from './index';
import { theme } from './themeConfig';

export function Button({
	label,
	style,
	textStyle,
	disabled,
	loading,
	styled = true,
	children,
	outline,
	minWidth,
	marginTop,
	marginBottom,
	marginLeft,
	marginRight,
	marginVertical,
	marginHorizontal,
	secondary,
	rippleColor = theme.paletteAuto.background.primary,
	...props
}) {
	const containerBackgroundColor = secondary
		? theme.palette.baseColors.base7
		: theme.palette.primaryColors.color2;

	const backgroundGenerator = useCallback(
		() =>
			(!!styled && disabled && theme.paletteAuto.background.disabled) ||
			(!!styled && outline && 'transparent') ||
			containerBackgroundColor,
		[containerBackgroundColor, disabled, outline, styled],
	);

	const styles = !styled
		? style
		: {
				...defaultStyles.container,
				...style,
				minWidth,
				marginTop,
				marginBottom,
				marginLeft,
				marginRight,
				marginVertical,
				marginHorizontal,
				borderColor: disabled
					? theme.paletteAuto.background.disabled
					: containerBackgroundColor,
		  };
	const backgroundColor = backgroundGenerator();

	return (
		<View
			rippleOpacity={0.5}
			rippleColor="white"
			backgroundColor={backgroundColor}
			style={styles}
			onPressDisabled={disabled}
			{...props}>
			{loading ? (
				<ActivityIndicator
					color={
						outline ? theme.paletteAuto.text.secondary : rippleColor
					}
					size="small"
				/>
			) : label ? (
				<Text
					color={
						(outline && containerBackgroundColor) ||
						(disabled && theme.paletteAuto.background.secondary) ||
						(secondary && theme.palette.primaryColors.color2) ||
						theme.paletteAuto.background.secondary
					}
					variant={theme.typo.fontVariants.baseBold}
					style={{
						...defaultStyles.label,
						...textStyle,
					}}>
					{label}
				</Text>
			) : (
				children
			)}
		</View>
	);
}

export function BackButton({
	showLabel,
	label = 'general.form.back',
	...props
}) {
	const navigation = useNavigation();
	const handleBack = () => navigation.goBack();

	return (
		<View
			row
			onPress={handleBack}
			paddingTop={15}
			alignItems="center"
			{...props}>
			<AntDesign
				name="left"
				size={15}
				color={theme.palette.baseColors.base1}
			/>
			{showLabel && (
				<Text
					paddingLeft={20}
					variant={theme.typo.fontVariants.baseBold}>
					{label}
				</Text>
			)}
		</View>
	);
}

export function TextAction({
	label = 'general.form.edit',
	featherIcon = 'edit-2',
	...props
}) {
	return (
		<View row {...props}>
			{!!featherIcon && (
				<Feather
					color={theme.palette.primaryColors.color3}
					name={featherIcon}
					size={15}
				/>
			)}
			{label && (
				<Text
					color={theme.palette.primaryColors.color3}
					paddingLeft={10}>
					{label}
				</Text>
			)}
		</View>
	);
}

export function CircleButton({
	size = 15,
	iconName = 'close',
	backgroundColor = theme.palette.baseColors.base7,
	badge,
	...props
}) {
	return (
		<View
			backgroundColor={backgroundColor}
			style={defaultStyles.circle}
			width={size * 2}
			center
			borderRadius={size}
			height={size * 2}
			{...props}>
			<Ionicons
				name={iconName}
				size={size}
				color={theme.palette.baseColors.base1}
			/>
			{!!badge && (
				<View
					position="absolute"
					top={-size / 3}
					right={-size / 3}
					width={size}
					height={size}
					center
					backgroundColor={theme.palette.statusColors.danger}
					borderRadius={10}>
					<Text size={10} color={theme.palette.primaryColors.color5}>
						{badge}
					</Text>
				</View>
			)}
		</View>
	);
}

const defaultStyles = StyleSheet.create({
	container: {
		height: 48,
		minWidth: 164,
		alignItems: 'center',
		justifyContent: 'center',
		borderWidth: 1,
		borderRadius: theme.roundness.full,
	},
	actionIcon: {
		borderRadius: theme.roundness.large * 2,
		width: 40,
		height: 40,
		justifyContent: 'center',
		alignItems: 'center',
	},
	containerDisabled: {
		borderColor: theme.paletteAuto.background.disabled,
	},
	label: {
		paddingHorizontal: 12,
		textAlign: 'center',
	},
	labelContainer: {
		width: '90%',
		alignItems: 'center',
		paddingHorizontal: 10,
		justifyContent: 'space-between',
	},
	circle: {
		...theme.shadows.normal,
	},
});
Button.propTypes = {
	onPress: PropTypes.oneOfType([PropTypes.func, PropTypes.bool]),
	label: PropTypes.string,
	children: PropTypes.node,
	style: ViewPropTypes.style,
	minWidth: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
	disabled: PropTypes.bool,
	loading: PropTypes.bool,
	styled: PropTypes.bool,
};
