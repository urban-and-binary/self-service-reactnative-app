import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { TextInput } from './TextInput';

export function TextInputControlled({
	value,
	error,
	onEndEditing,
	validator,
	validationError,
	...props
}) {
	const [localError, setLocalError] = useState('');
	useEffect(() => {
		setLocalError('');
	}, [value]);

	const handleValidate = ({ nativeEvent: { text } }) => {
		// errors should be hidden if the text input erased
		if (text !== '' && !!validator && !validator(text))
			setLocalError(validationError);

		!!onEndEditing && onEndEditing();
	};

	return (
		<TextInput
			value={value}
			error={error || localError}
			onEndEditing={handleValidate}
			{...props}
		/>
	);
}

TextInputControlled.propTypes = {
	error: PropTypes.string,
	validationError: PropTypes.string.isRequired,
	validator: PropTypes.func,
	onEndEditing: PropTypes.func,
	onChangeText: PropTypes.func,
};
