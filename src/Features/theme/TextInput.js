import React, { useState, useRef, useEffect } from 'react';
import {
	Appearance,
	StyleSheet,
	TextInput as RNTextInput,
	ViewPropTypes,
} from 'react-native';

import PropTypes from 'prop-types';

import Ionicons from 'react-native-vector-icons/Ionicons';
import { isLightinColorValid, theme } from './themeConfig';
import { View } from './View';
import { Text } from './Text';

const MIN_HEIGHT = 30.5;
export function TextInput({
	style,
	containerStyle = {},
	disabled,
	uneditable = false,
	children,
	color,
	width,
	error,
	onEditableChanged,
	label,
	isRequired,
	marginTop,
	marginBottom,
	unit,
	forwardedRef,
	...props
}) {
	const ref = useRef();
	const [editable, setEditable] = useState(false);
	const handleEditable = () => {
		setEditable(true);
		!!onEditableChanged && onEditableChanged(true);
	};

	useEffect(() => {
		editable && ref?.current?.focus();
	}, [editable, ref]);

	return (
		<View style={{ ...containerStyle, marginBottom, marginTop }}>
			{!!label && (
				<View row>
					<Text
						paddingBottom={10}
						variant={theme.typo.fontVariants.smallBold}>
						{label}
					</Text>
					{!!isRequired && (
						<Text
							size={18}
							color={theme.palette.statusColors.danger}>
							*
						</Text>
					)}
				</View>
			)}
			<View
				width={width}
				backgroundColor={theme.paletteAuto.background.secondary}
				borderRadius={theme.roundness.large}
				paddingVertical={12}
				paddingHorizontal={16}>
				<View row alignItems="center">
					<RNTextInput
						placeholderTextColor={theme.paletteAuto.text.disabled}
						ref={forwardedRef ?? ref}
						onBlur={() => setEditable(false)}
						editable={(!disabled || editable) && !uneditable}
						selectTextOnFocus={!disabled}
						style={[
							defaultStyles.input,
							style,
							{
								color: color
									? isLightinColorValid(color)
										? color[colorScheme]
										: color
									: theme.palette[colorScheme].text.primary,
							},
							disabled && defaultStyles.disabled,
						]}
						{...props}>
						{children}
					</RNTextInput>
					{!!unit && (
						<View row alignItems="center">
							<View
								marginRight={10}
								width={1}
								minHeight={MIN_HEIGHT}
								backgroundColor={theme.palette.baseColors.base4}
							/>
							<Text variant={theme.typo.fontVariants.baseRegular}>
								{unit}
							</Text>
						</View>
					)}
					{!uneditable && disabled && !editable && (
						<View
							style={defaultStyles.editIconContainer}
							onPress={handleEditable}>
							<Ionicons
								name="create-outline"
								color={theme.palette.primaryColors.color2}
								size={theme.typo.fontSize.medium}
							/>
						</View>
					)}
					{!!error && (
						<View style={defaultStyles.editIconContainer}>
							<Ionicons
								name="close-circle"
								color={theme.palette.statusColors.danger}
								size={theme.typo.fontSize.xxLarge}
							/>
						</View>
					)}
				</View>
			</View>
			{!!error && typeof error === 'string' && (
				<Text
					paddingVertical={5}
					color={theme.palette.statusColors.danger}>
					{error}
				</Text>
			)}
		</View>
	);
}
const colorScheme = Appearance.getColorScheme();

const defaultStyles = StyleSheet.create({
	editIconContainer: {
		minHeight: 30.5,
	},
	input: {
		flex: 1,
		fontFamily: theme.typo.fontFamily.regular,
		fontSize: 18,
		margin: 1,
		padding: 0,
		minHeight: MIN_HEIGHT,
	},
	disabled: {
		borderColor: 'gray',
	},
});

TextInput.propTypes = {
	placeholder: PropTypes.string,
	children: PropTypes.node,
	style: ViewPropTypes.style,
	disabled: PropTypes.bool,
	uneditable: PropTypes.bool,
	color: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
};
