import React, { useState } from 'react';
import Feather from 'react-native-vector-icons/Feather';
import { View } from './View';
import { Text } from './Text';
import { theme } from './themeConfig';

export function CheckBox({
	initialValue,
	alignItems = 'center',
	onChangeValue,
	label,
	stateless,
	...props
}) {
	const [localValue, setLocalValue] = useState(!!initialValue);
	const checked = stateless ? initialValue : localValue;
	const handleCheck = () => {
		!!onChangeValue && onChangeValue(!checked);
		!stateless && setLocalValue(!checked);
	};
	return (
		<View
			row
			alignItems={alignItems}
			justifyContent="flex-start"
			{...props}>
			<View
				onPress={handleCheck}
				paddingHorizontal={10}
				paddingVertical={10}>
				<Feather
					color={
						checked
							? theme.palette.statusColors.success
							: theme.paletteAuto.text.disabled
					}
					name={checked ? 'check-circle' : 'circle'}
					size={20}
				/>
			</View>
			<Text
				variant={theme.typo.fontVariants.xsRegular}
				color={theme.paletteAuto.text.primary}>
				{label}
			</Text>
		</View>
	);
}
