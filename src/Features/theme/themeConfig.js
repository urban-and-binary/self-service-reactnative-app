import { Dimensions } from 'react-native';

const primaryColors = {
	color1: '#005AC8',
	color2: '#499CFB',
	color3: '#94C6FF',
	color4: '#C7E1FF',
	color5: '#E8F3FF',
};
const secondaryColors = {
	color1: '#048A55',
	color2: '#00BE73',
	color3: '#07D785',
	color4: '#C5FBE1',
	color5: '#E8FCF2',
};
const baseColors = {
	base1: '#151515',
	base2: '#393939',
	base3: '#989898',
	grey: '#989898',
	base4: '#cccccc',
	base5: '#eaeaea',
	base6: '#F5F7F9',
	base7: '#fefefe',
};

export const lightColors = {
	background: {
		primary: baseColors.base7,
		secondary: baseColors.base6,
		disabled: baseColors.base5,
	},

	gradient: { start: '#0B1B3C', middle: '#496DB2', end: '#889EC9' },

	text: {
		primary: baseColors.base1,
		secondary: baseColors.base2,
		disabled: baseColors.base4,
	},
	border: {
		primary: baseColors.base3,
		secondary: baseColors.base4,
	},
	semiTransparent: 'rgba(21,21,21,0.2)', // base1
};

const statusColors = {
	warning: '#FFC400',
	danger: '#EB5757',
	success: secondaryColors.color2,
	lightViolet: '#D9CFFF',
	lightRed: '#FFA9A9',
	lightYellow: '#FFEED3',
};

const fontFamily = {
	thin: 'Metropolis-Thin',
	extraLight: 'Metropolis-ExtraLight',
	light: 'Metropolis-Light',
	regular: 'Metropolis-Regular',
	medium: 'Metropolis-Medium',
	semiBold: 'Metropolis-SemiBold',
	bold: 'Metropolis-Bold',
	extraBold: 'Metropolis-ExtraBold',
	black: 'Metropolis-Black',
};

const fontVariants = {
	xsRegular: {
		fontSize: 14,
		lineHeight: 20,
		weight: 400,
	},
	xsMedium: {
		fontSize: 14,
		lineHeight: 20,
		weight: 500,
	},
	xsBold: {
		fontSize: 14,
		lineHeight: 20,
		weight: 700,
	},
	smallRegular: {
		fontSize: 16,
		lineHeight: 22,
		weight: 400,
	},
	smallMedium: {
		fontSize: 16,
		lineHeight: 22,
		weight: 500,
	},
	smallBold: {
		fontSize: 16,
		lineHeight: 22,
		weight: 700,
	},
	baseRegular: {
		fontSize: 19,
		lineHeight: 27,
		weight: 400,
	},
	baseMedium: {
		fontSize: 19,
		lineHeight: 27,
		weight: 500,
	},
	baseBold: {
		fontSize: 19,
		lineHeight: 27,
		weight: 700,
	},
	h3Regular: {
		fontSize: 23,
		lineHeight: 31,
		weight: 400,
	},
	h3Medium: {
		fontSize: 23,
		lineHeight: 31,
		weight: 500,
	},
	h3Bold: {
		fontSize: 23,
		lineHeight: 31,
		weight: 700,
	},
	h2Medium: {
		fontSize: 29,
		lineHeight: 37,
		weight: 500,
	},
	h2Bold: {
		fontSize: 29,
		lineHeight: 37,
		weight: 700,
	},
	h1Bold: {
		fontSize: 36,
		lineHeight: 43,
		weight: 700,
	},
};

const fontSize = {
	xxSmall: 10,
	xSmall: 12,
	small: 14,
	semiSmall: 16,
	medium: 18,
	semiLarge: 20,
	large: 22,
	xLarge: 24,
	xxLarge: 26,
};

const fontWeight = {
	thin: 100,
	extraLight: 200,
	light: 300,
	regular: 400,
	medium: 500,
	semiBold: 600,
	bold: 700,
	extraBold: 800,
	black: 900,
};

const roundness = {
	full: 100,
	large: 20,
	medium: 10,
	small: 6,
};

const pads = {
	screenHorizontalPad: 33,
	screenVerticalPad: 15,
	pressablePad: 10,
	horizontalPad: 20,
	sectionVerticalPad: 33,
};
const horizontalPadLess = { marginHorizontal: -20 };
const dimensions = {
	screenWidth: Dimensions.get('window').width,
	screenHeight: Dimensions.get('window').height,
};

const shadows = {
	none: {},
	normal: {
		shadowColor: baseColors.base1,
		shadowOffset: {
			width: 0,
			height: 1,
		},
		shadowOpacity: 0.25,
		shadowRadius: 2.22,

		elevation: 3,
	},
	deep: {
		shadowColor: baseColors.base1,
		shadowOffset: {
			width: 0,
			height: 5,
		},
		shadowOpacity: 0.25,
		shadowRadius: 6.27,

		elevation: 10,
	},
	xxDeep: {
		shadowColor: baseColors.base1,
		shadowOffset: {
			width: 0,
			height: 10,
		},
		shadowOpacity: 0.25,
		shadowRadius: 13.16,

		elevation: 20,
	},
};

export const theme = {
	// theme will support dark colors by adding darkColors object
	palette: {
		dark: lightColors,
		light: lightColors,
		statusColors,
		primaryColors,
		secondaryColors,
		baseColors,
	},
	paletteAuto: lightColors,
	typo: { fontFamily, fontSize, fontWeight, fontVariants },
	styles: { horizontalPadLess },
	roundness,
	pads,
	dimensions,
	shadows,
};

export const isLightinColorValid = colorsObj =>
	typeof colorsObj === 'object' &&
	Object.keys(colorsObj).includes('light') &&
	Object.keys(colorsObj).includes('dark');
