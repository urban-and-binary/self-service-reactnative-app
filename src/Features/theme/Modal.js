import React from 'react';
import RNModal from 'react-native-modal';

export function Modal({ onRequestClose, isVisible, children, ...props }) {
	return (
		<RNModal
			onRequestClose={onRequestClose}
			onBackButtonPress={onRequestClose}
			onBackdropPress={onRequestClose}
			onDismiss={onRequestClose}
			isVisible={isVisible}
			hasBackdrop
			backdropColor="black"
			statusBarTranslucent
			animationInTiming={500}
			animationOutTiming={500}
			backdropTransitionInTiming={750}
			backdropTransitionOutTiming={500}
			useNativeDriverForBackdrop
			{...props}>
			{children}
		</RNModal>
	);
}
