import * as React from 'react';
import {
	Appearance,
	StatusBar,
	View as RNView,
	ViewPropTypes,
} from 'react-native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import PropTypes from 'prop-types';
import { TopBar, View, ScrollView } from './index';
import { theme } from './themeConfig';

const isNonScrolling = preset =>
	!preset || !presets[preset] || preset === 'fixed';

const presets = {
	fixed: {
		outer: {
			flex: 1,
			height: '100%',
		},
		inner: {
			justifyContent: 'flex-start',
			alignItems: 'stretch',
			height: '100%',
			width: '100%',
		},
	},

	scroll: {
		outer: {
			width: '100%',
			height: '100%',
		},
		inner: {
			justifyContent: 'flex-start',
			alignItems: 'stretch',
		},
	},
};

const colorScheme = Appearance.getColorScheme();

function CustomStatusBar({
	barStyle = colorScheme === 'dark' ? 'light-content' : 'dark-content',
	translucent = true,
	animated = true,
	backgroundColor = 'transparent',
	hidden = false,
}) {
	return (
		<StatusBar
			{...{ barStyle, translucent, animated, backgroundColor, hidden }}
		/>
	);
}

export function Screen({
	preset,
	style,
	backgroundColor,
	unsafe,
	barColor = 'transparent',
	barStyle = 'dark-content',
	topBar,
	imageHeader,
	children,
	onClose,
	onBack,
	horizontalPad,
	paddingHorizontal,
	paddingVertical = 0,
	translucent,
	flex,
	contentFlexGrow,
	...props
}) {
	const insets = useSafeAreaInsets();

	const isFixed = isNonScrolling(preset);
	const presetStyles = isFixed ? presets.fixed : presets.scroll;

	const insetStyle = {
		paddingTop: unsafe || topBar ? paddingVertical : insets.top,
	};
	const insetInScrollStyle = {
		paddingTop: unsafe || topBar ? insets.top : 0,
		paddingBottom: unsafe || topBar ? theme.dimensions.screenHeight / 4 : 0,
	};

	return (
		<View
			backgroundColor={
				backgroundColor || theme.paletteAuto.background.primary
			}
			style={[presetStyles.outer, { paddingBottom: 0 }]}>
			<CustomStatusBar
				translucent={translucent}
				backgroundColor={barColor}
				barStyle={barStyle}
			/>
			{topBar && !imageHeader && (
				<TopBar
					insetPad={insets.top}
					backgroundColor={backgroundColor}
				/>
			)}
			<RNView
				style={[
					horizontalPad &&
						isFixed && {
							paddingHorizontal: theme.pads.screenHorizontalPad,
						},
					!horizontalPad &&
						!!paddingHorizontal &&
						isFixed && {
							paddingHorizontal,
						},
					isFixed ? presetStyles.inner : presetStyles.outer,
					insetStyle,
				]}>
				{isFixed && children}
				{!isFixed && (
					<ScrollView
						showsVerticalScrollIndicator={false}
						style={presetStyles.outer}
						paddingHorizontal={
							(horizontalPad && theme.pads.screenHorizontalPad) ||
							paddingHorizontal ||
							undefined
						}
						contentContainerStyle={{
							...insetInScrollStyle,
							...presetStyles.inner,
							...style,
						}}
						flex={flex}
						flexGrow={contentFlexGrow}
						keyboardShouldPersistTaps={
							props.keyboardShouldPersistTaps || 'handled'
						}>
						{children}
					</ScrollView>
				)}
			</RNView>
		</View>
	);
}

Screen.propTypes = {
	preset: PropTypes.oneOf(['fixed', 'scroll']),
	style: ViewPropTypes.style,
	backgroundColor: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
	unsafe: PropTypes.bool,
	children: PropTypes.node,
	topBar: PropTypes.bool,
	barStyle: PropTypes.oneOf(['dark-content', 'light-content']),
	imageHeader: PropTypes.string,
};
