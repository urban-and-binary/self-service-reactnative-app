import React, { useState } from 'react';
import PropTypes from 'prop-types';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { Text, View } from '.';
import { theme } from './themeConfig';

export function OptionsItem({ item, onPress, radio, isActive }) {
	const IoniconsName = isActive
		? radio
			? 'md-radio-button-on'
			: 'checkmark-circle-outline'
		: 'ellipse-outline';

	const textColor = isActive
		? theme.palette.primaryColors.color2
		: theme.paletteAuto.text.primary;

	const iconColor = isActive
		? theme.palette.primaryColors.color2
		: theme.paletteAuto.text.disabled;

	return (
		<View
			onPress={() => onPress(item?.id || item)}
			row
			justifyContent="space-between"
			alignItems="center"
			marginVertical={12}>
			<Text
				size={theme.typo.fontSize.semiSmall}
				weight={isActive ? 700 : 400}
				color={textColor}>
				{item?.label ?? item}
			</Text>
			<Ionicons name={IoniconsName} size={22} color={iconColor} />
		</View>
	);
}

export function Options({ data, onSelect, radio, initialSelected = [] }) {
	const [selected, setSelected] = useState([...initialSelected]);
	const handleCheck = item => {
		const newSelected = radio
			? [item]
			: selected.some(option => isItemsSame(option, item))
			? removeSelected(item)
			: [...selected, item];
		setSelected(newSelected);
		onSelect && onSelect(newSelected);
	};

	const removeSelected = option =>
		selected.filter(s => !isItemsSame(s, option));

	const isItemsSame = (a, b) => a === b || a?.id === b?.id;

	if (!data) {
		return <View />;
	}
	const renderOption = (item, index) => {
		const isActive = selected.some(option => isItemsSame(option, item));
		return (
			<OptionsItem
				key={index.toString()}
				item={item}
				onPress={handleCheck}
				radio={radio}
				isActive={isActive}
			/>
		);
	};

	return <View>{data.map(renderOption)}</View>;
}

Options.propTypes = {
	data: PropTypes.arrayOf(
		PropTypes.oneOfType([
			PropTypes.string,
			PropTypes.shape({
				id: PropTypes.string.isRequired,
				label: PropTypes.string.isRequired,
			}),
		]),
	).isRequired,
	onSelect: PropTypes.func,
};
