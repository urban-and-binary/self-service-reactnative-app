import React from 'react';
import PropTypes from 'prop-types';
import { View, Text } from './index';
import { theme } from './themeConfig';

export function Tag({
	label,
	status,
	backgroundColor = theme.palette.statusColors.danger,
	textColor = theme.palette.baseColors.base7,
	...props
}) {
	let tagBackgroundColor;
	let tagTextColor;
	switch (status) {
		case 'disabled':
			tagBackgroundColor = theme.palette.baseColors.base6;
			tagTextColor = theme.palette.baseColors.base2;
			break;

		case 'warning':
			tagBackgroundColor = theme.palette.statusColors.warning;
			tagTextColor = theme.palette.baseColors.base1;
			break;

		case 'success':
			tagBackgroundColor = theme.palette.statusColors.success;
			tagTextColor = theme.palette.baseColors.base7;
			break;

		case 'info':
			tagBackgroundColor = theme.palette.primaryColors.color2;
			tagTextColor = theme.palette.baseColors.base7;
			break;
		case 'danger':
			tagBackgroundColor = theme.palette.statusColors.danger;
			tagTextColor = theme.palette.baseColors.base7;
			break;
		default:
			tagBackgroundColor = backgroundColor;
			tagTextColor = textColor;
			break;
	}

	return (
		<View
			paddingHorizontal={20}
			paddingVertical={8}
			borderRadius={theme.roundness.small}
			backgroundColor={tagBackgroundColor}
			center
			{...props}>
			<Text color={tagTextColor}>{label}</Text>
		</View>
	);
}

Tag.propTypes = {
	status: PropTypes.oneOf([
		'danger',
		'warning',
		'success',
		'info',
		'disabled',
	]),
};
