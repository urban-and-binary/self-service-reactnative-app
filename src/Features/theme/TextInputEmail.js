import React from 'react';
import { useTranslation } from '../translation/useTranslation';
import { isEmailValidated } from './validationUtil';
import { TextInputControlled } from './TextInputControlled';

export function TextInputEmail({ error, placeholder, ...props }) {
	const t = useTranslation();

	return (
		<TextInputControlled
			hideUnderline
			validator={isEmailValidated}
			autoComplete="email"
			keyboardType="email-address"
			autoCorrect={false}
			autoCapitalize="none"
			clearButtonMode="while-editing"
			spellCheck={false}
			validationError="general.errors.incorrectEmail"
			placeholder={placeholder ?? t('general.form.emailAddress')}
			{...props}
		/>
	);
}
