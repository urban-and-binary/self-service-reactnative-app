import React, { useEffect, useState } from 'react';
import { FlatList } from 'react-native';
import { Screen, Text, View } from '../theme';
import { theme } from '../theme/themeConfig';
import { TabBar } from '../theme/TabBar';
import { announcementsQueryTypes } from './announcementsConfig';
import { AnnouncementListItem } from './AnnouncementListItem';
import { useQueryAnnouncements } from './api';

function AnnouncementsScreen() {
	const [activeTab, setActiveTab] = useState(announcementsQueryTypes.all.id);
	const [page, setPage] = useState(1);
	const PAGE_SIZE = 20;
	const {
		data: { data = [], total },
		refetch,
		isLoading,
	} = useQueryAnnouncements({
		query: {
			page,
			pageSize: PAGE_SIZE,
			type: activeTab.toLowerCase(),
		},
		keepPreviousData: true,
		staleTime: 1000,
	});

	useEffect(() => {
		page === 1 && refetch();
	}, [page, refetch, activeTab]);

	const handleChangeTab = tab => {
		setActiveTab(tab);
		setPage(1);
	};

	const renderAnnouncementsItem = ({ item }) => (
		<View marginHorizontal={theme.pads.horizontalPad}>
			<AnnouncementListItem key={item._id} item={item} />
		</View>
	);
	const renderSeparator = () => (
		<View height={theme.pads.sectionVerticalPad} />
	);

	const handleAddMore = () => {
		if (total < PAGE_SIZE * page) return;
		setPage(prevState => prevState + 1);
	};

	const handleRefetch = () => {
		setPage(1);
	};

	return (
		<Screen
			translucent
			barStyle="dark-content"
			barColor="transparent"
			topBar>
			<View paddingTop={20} paddingHorizontal={theme.pads.horizontalPad}>
				<Text
					variant={theme.typo.fontVariants.h1Bold}
					paddingBottom={20}>
					Announcements
				</Text>
				<TabBar
					style={{ elevation: 30 }}
					tabs={Object.keys(announcementsQueryTypes).map(item =>
						item.toUpperCase(),
					)}
					activeTab={activeTab.toUpperCase()}
					onChange={handleChangeTab}
				/>
			</View>
			<FlatList
				progressViewOffset={50}
				onRefresh={handleRefetch}
				refreshing={isLoading}
				onEndReachedThreshold={0.01}
				onEndReached={handleAddMore}
				extraData={data}
				ItemSeparatorComponent={renderSeparator}
				ListHeaderComponent={renderSeparator}
				ListFooterComponent={renderSeparator}
				data={data}
				renderItem={renderAnnouncementsItem}
			/>
		</Screen>
	);
}
export default AnnouncementsScreen;
