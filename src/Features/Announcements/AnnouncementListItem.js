import React from 'react';
import moment from 'moment';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { useNavigation } from '@react-navigation/native';
import { Card, Text, View } from '../theme';
import { theme } from '../theme/themeConfig';

export function AnnouncementListItem({ item, fullDetails }) {
	const navigation = useNavigation();
	const handleOpenDetails = () =>
		navigation.navigate('announcementDetails', { item });
	return (
		<Card
			onPress={!fullDetails && handleOpenDetails}
			backgroundColor={
				item?.read || fullDetails
					? theme.palette.baseColors.base7
					: theme.palette.primaryColors.color5
			}>
			<View row>
				<View flex>
					<Text
						paddingBottom={20}
						variant={theme.typo.fontVariants.smallBold}>
						{item.title}
					</Text>
					<View paddingBottom={20}>
						{fullDetails && (
							<Text
								variant={theme.typo.fontVariants.smallBold}
								color={theme.palette.baseColors.grey}>
								screens.announcements.day
							</Text>
						)}
						<Text variant={theme.typo.fontVariants.smallRegular}>
							{moment(item.createdAt).format('yyyy - MM - DD')}
						</Text>
					</View>
					{fullDetails && (
						<Text
							paddingBottom={10}
							variant={theme.typo.fontVariants.smallBold}
							color={theme.palette.baseColors.grey}>
							general.form.description
						</Text>
					)}
					<Text
						variant={theme.typo.fontVariants.smallRegular}
						numberOfLines={!fullDetails ? 2 : undefined}>
						{item.description}
					</Text>
				</View>
				{!fullDetails && (
					<Ionicons
						size={theme.typo.fontSize.semiLarge}
						name="chevron-forward"
					/>
				)}
			</View>
		</Card>
	);
}
