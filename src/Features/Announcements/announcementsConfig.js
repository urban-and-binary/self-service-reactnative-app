export const announcementsConfig = [
	'screens.announcements.private',
	'screens.announcements.common',
];
export const announcementsQueryTypes = {
	all: {
		id: 'all',
		label: 'All',
	},
	private: {
		id: 'private',
		label: 'screens.announcements.private',
	},
	common: {
		id: 'common',
		label: 'screens.announcements.common',
	},
};
