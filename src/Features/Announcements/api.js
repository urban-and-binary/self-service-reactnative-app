import { useMutation, useQuery } from '../reactQuery/hooks';

export const useReadAnnouncement = (id, option = {}) =>
	useMutation(`announcements/${id}/read`, option);
export const useQueryAnnouncements = (option = {}) =>
	useQuery('useQueryAnnouncements', 'announcements', option);
