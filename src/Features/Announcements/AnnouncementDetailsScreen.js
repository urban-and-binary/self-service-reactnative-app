import React from 'react';
import { useRoute } from '@react-navigation/native';
import { useQueryClient } from 'react-query';
import { BackButton, Screen, View } from '../theme';
import { theme } from '../theme/themeConfig';
import { AnnouncementListItem } from './AnnouncementListItem';
import { useReadAnnouncement } from './api';

function AnnouncementsScreen() {
	const {
		params: { item },
	} = useRoute();
	const queryClient = useQueryClient();
	const readAnnouncements = useReadAnnouncement(item?._id, {
		onSuccess: () => {
			queryClient.invalidateQueries('useQueryAnnouncements');
		},
	});

	!item?.read && readAnnouncements.mutate();

	return (
		<Screen
			preset="scroll"
			paddingVertical={theme.pads.horizontalPad}
			paddingHorizontal={theme.pads.horizontalPad}>
			<BackButton showLabel />
			<View paddingVertical={20}>
				<AnnouncementListItem fullDetails key={item._id} item={item} />
			</View>
		</Screen>
	);
}
export default AnnouncementsScreen;
