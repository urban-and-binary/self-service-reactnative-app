export const fakeAnnouncements = [
	{
		_id: '2998173',
		title: 'Slaptažodžio priminimas',
		description:
			'Sveiki, Gavome užklausą pakeisti Jūsų prisijungimo slaptažodį prie mano.civinity.lt savitarnos svetainės. Jeigu užklausą siuntėte ne Jūs, ignoruokite šį laišką. Slaptažodžio pakeitimui, spauskite šią nuorodą – https://mano.stg.civinity.lt/login/change-password \n' +
			'\n' +
			'Turėdami papildomų klausimų, kreipkitės klientų aptarnavimo tel. 8 700 55188 ar el. paštu klauskite@civinity.eu Dėkojame, kad naudojatės mūsų paslaugomis! Su geriausiais linkėjimais, Civinity',
		unitLabel: 'Rambyno g. 20 - 515, Klaipėda',
		buildingExtId: 'a0B090000044hLUEAY',
		unitExtId: 'a0Y09000002E1BqEAK',
		createdAt: '2022-05-11T14:20:18.130Z',
		read: false,
	},
	{
		_id: '2998123',
		title: 'Slaptažodžio priminimas',
		description:
			'Sveiki, Gavome užklausą pakeisti Jūsų prisijungimo slaptažodį prie mano.civinity.lt savitarnos svetainės. Jeigu užklausą siuntėte ne Jūs, ignoruokite šį laišką. Slaptažodžio pakeitimui, spauskite šią nuorodą – https://mano.stg.civinity.lt/login/change-password \n' +
			'\n' +
			'Turėdami papildomų klausimų, kreipkitės klientų aptarnavimo tel. 8 700 55188 ar el. paštu klauskite@civinity.eu Dėkojame, kad naudojatės mūsų paslaugomis! Su geriausiais linkėjimais, Civinity',
		unitLabel: 'Rambyno g. 20 - 515, Klaipėda',
		buildingExtId: 'a0B090000044hLUEAY',
		unitExtId: 'a0Y09000002E1BqEAK',
		createdAt: '2022-05-11T14:20:18.130Z',
		read: true,
	},
	{
		_id: '2998567',
		title: 'Slaptažodžio priminimas',
		description:
			'Sveiki, Gavome užklausą pakeisti Jūsų prisijungimo slaptažodį prie mano.civinity.lt savitarnos svetainės. Jeigu užklausą siuntėte ne Jūs, ignoruokite šį laišką. Slaptažodžio pakeitimui, spauskite šią nuorodą – https://mano.stg.civinity.lt/login/change-password \n' +
			'\n' +
			'Turėdami papildomų klausimų, kreipkitės klientų aptarnavimo tel. 8 700 55188 ar el. paštu klauskite@civinity.eu Dėkojame, kad naudojatės mūsų paslaugomis! Su geriausiais linkėjimais, Civinity',
		unitLabel: 'Rambyno g. 20 - 515, Klaipėda',
		buildingExtId: 'a0B090000044hLUEAY',
		unitExtId: 'a0Y09000002E1BqEAK',
		createdAt: '2022-05-11T14:20:18.130Z',
		read: true,
	},
];
