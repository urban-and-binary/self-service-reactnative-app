import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import ProfileScreen from '../Profile/ProfileScreen';
import TasksScreen from '../Tasks/TasksScreen';
import AnnouncementsScreen from '../Announcements/AnnouncementsScreen';
import TasksDetailsScreen from '../Tasks/TasksDetailsScreen';
import AdditionalInfoScreen from '../Onboarding/AdditionalInfo';
import NewUnitScreen from '../Units/NewUnitScreen';
import AnnouncementDetailsScreen from '../Announcements/AnnouncementDetailsScreen';
import PollsAndVotingsScreen from '../PollsAndVotings/PollsAndVotingsScreen';
import DocumentsScreen from '../Documents/DocumentsScreen';
import SettingsScreen from '../Settings/SettingsScreen';

const Navigation = createNativeStackNavigator();
export const drawerScreensArray = [
	<Navigation.Screen
		key="profile"
		name="profile"
		component={ProfileScreen}
	/>,
	<Navigation.Screen key="tasks" name="tasks" component={TasksScreen} />,
	<Navigation.Screen
		key="taskDetails"
		name="taskDetails"
		component={TasksDetailsScreen}
	/>,
	<Navigation.Screen
		key="announcements"
		name="announcements"
		component={AnnouncementsScreen}
	/>,
	<Navigation.Screen
		key="announcementDetails"
		name="announcementDetails"
		component={AnnouncementDetailsScreen}
	/>,
	<Navigation.Screen
		key="additionalInfo"
		name="additionalInfo"
		component={AdditionalInfoScreen}
	/>,
	<Navigation.Screen
		key="newUnit"
		name="newUnit"
		component={NewUnitScreen}
	/>,
	<Navigation.Screen
		key="pollsAndVotings"
		name="pollsAndVotings"
		component={PollsAndVotingsScreen}
	/>,
	<Navigation.Screen
		key="documents"
		name="documents"
		component={DocumentsScreen}
	/>,
	<Navigation.Screen
		key="settings"
		name="settings"
		component={SettingsScreen}
	/>,
];
