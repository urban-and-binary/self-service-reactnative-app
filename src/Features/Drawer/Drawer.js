import React, { useEffect } from 'react';
import Modal from 'react-native-modal';
import { ScrollView, StyleSheet } from 'react-native';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { inject, observer } from 'mobx-react';
import { useNavigation } from '@react-navigation/native';
import { View, CircleButton, Text } from '../theme';
import { theme } from '../theme/themeConfig';
import { useLogout } from '../Login/api';

/*
find available props here https://www.npmjs.com/package/react-native-modal
 */
export const Drawer = inject('store')(
	observer(
		({
			store: { auth, logout },
			visible,
			onRequestClose,
			children,
			...props
		}) => {
			const navigation = useNavigation();
			let actionTimeout;

			const callWithDelay = callable => {
				onRequestClose();
				actionTimeout = setTimeout(() => callable(), 800);
			};

			const signOut = useLogout({
				onSuccess: () => {
					callWithDelay(logout);
				},
			});
			const handleSignOut = () => {
				signOut.mutate();
			};
			useEffect(
				() => () => !!actionTimeout && clearTimeout(actionTimeout),
				[actionTimeout],
			);

			const navigateTo = screen => {
				navigation.navigate(screen);
				onRequestClose();
			};

			return (
				<Modal
					onDismiss={onRequestClose}
					swipeDirection="right"
					animationIn="slideInRight"
					animationOut="slideOutRight"
					isVisible={visible}
					hasBackdrop
					backdropColor="black"
					statusBarTranslucent
					animationInTiming={300}
					animationOutTiming={300}
					backdropTransitionInTiming={750}
					propagateSwipe
					backdropTransitionOutTiming={500}
					useNativeDriverForBackdrop
					style={defaultStyles.view}
					{...props}>
					<View
						height="100%"
						width="85%"
						backgroundColor={theme.palette.baseColors.base7}
						alignItems="flex-end"
						paddingTop={40}
						paddingHorizontal={theme.pads.horizontalPad}
						style={{ alignSelf: 'flex-end' }}>
						<CircleButton size={18} onPress={onRequestClose} />

						<View marginRight={-15}>
							<Ionicons
								name="person-circle"
								size={100}
								color={theme.palette.baseColors.base1}
							/>
						</View>
						<Text
							paddingBottom={40}
							variant={theme.typo.fontVariants.h3Bold}>
							{auth?.name}
						</Text>
						<ScrollView
							contentContainerStyle={{
								alignItems: 'flex-end',
							}}
							showsVerticalScrollIndicator={false}>
							<DrawerIcon
								label="Home"
								icon="home"
								onPress={() => navigateTo('home')}
							/>
							<DrawerIcon
								onPress={() => navigateTo('tasks')}
								label="Tasks"
								icon="wrench"
							/>
							<DrawerIcon
								label="Documents"
								icon="doc"
								onPress={() => navigateTo('documents')}
							/>
							<DrawerIcon
								label="Announcements"
								icon="flag"
								onPress={() => navigateTo('announcements')}
							/>
							<DrawerIcon
								onPress={() => navigateTo('pollsAndVotings')}
								label="Polls and voting"
								icon="badge"
							/>
							<DrawerIcon
								onPress={() => navigateTo('profile')}
								label="Profile"
								icon="user"
							/>
							<View height={40} />
							<DrawerIcon label="Contact support" icon="info" />
							<DrawerIcon
								label="Account Settings"
								icon="settings"
							/>
							<DrawerIcon
								onPress={handleSignOut}
								label="Log out"
								icon="logout"
							/>
						</ScrollView>
					</View>
					{children}
				</Modal>
			);
		},
	),
);

function DrawerIcon({ label, icon, onPress }) {
	return (
		<View paddingVertical={10} onPress={onPress} alignItems="center" row>
			<Text
				paddingRight={10}
				variant={theme.typo.fontVariants.baseRegular}>
				{label}
			</Text>
			<SimpleLineIcons
				size={16}
				name={icon}
				color={theme.palette.baseColors.base1}
			/>
		</View>
	);
}

const defaultStyles = StyleSheet.create({
	view: {
		justifyContent: 'center',
		margin: 0,
	},
});
