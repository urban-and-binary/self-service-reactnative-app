import React from 'react';
import { TouchableOpacity } from 'react-native';
import Feather from 'react-native-vector-icons/Feather';
import { View, Text } from '../theme';
import { theme } from '../theme/themeConfig';

const tabConfig = {
	Home: {
		icon: 'home',
	},

	Bills: {
		icon: 'credit-card',
	},

	Meters: {
		icon: 'server',
	},

	Requests: {
		icon: 'edit-3',
	},

	EasyPanda: {
		icon: 'codesandbox',
	},
};

function NavigationItem({ descriptors, state, index, navigation, route }) {
	const { options } = descriptors[route.key];
	const isFocused = state.index === index;
	const tab = tabConfig[route.name];

	const onPress = () => {
		const event = navigation.emit({
			type: 'tabPress',
			target: route.key,
		});

		if (!isFocused && !event.defaultPrevented) {
			navigation.navigate(route.name, tab.options);
		}
	};

	const onLongPress = () => {
		navigation.emit({
			type: 'tabLongPress',
			target: route.key,
		});
	};

	const color = isFocused
		? theme.palette.primaryColors.color3
		: theme.palette.baseColors.grey;

	return (
		<TouchableOpacity
			accessibilityRole="button"
			accessibilityStates={isFocused ? ['selected'] : []}
			accessibilityLabel={options.tabBarAccessibilityLabel}
			testID={options.tabBarTestID}
			onPress={onPress}
			onLongPress={onLongPress}
			style={{ flex: 1, alignItems: 'center' }}
			key={index}>
			<View center paddingVertical={20}>
				<Feather name={tab.icon} size={24} color={color} />
				<Text
					variant={theme.typo.fontVariants.xsRegular}
					size={10}
					color={color}>
					{route.name}
				</Text>
			</View>
		</TouchableOpacity>
	);
}

export function NavigationBar({ state, descriptors, navigation }) {
	return (
		<View row center backgroundColor={theme.paletteAuto.background.primary}>
			{state.routes.map((route, index) =>
				NavigationItem({
					descriptors,
					state,
					index,
					navigation,
					route,
				}),
			)}
		</View>
	);
}
