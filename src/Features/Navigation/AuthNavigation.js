import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { useRoute } from '@react-navigation/native';
import { defaultScreenOptions } from './navigationConfig';
import LoginScreen from '../Login/Login';
import ForgetPasswordSmsVerificationScreen from '../ForgetPassword/ForgetPasswordSmsVerification';
import ForgetPasswordScreen from '../ForgetPassword/ForgetPassword';
import EmailConfirmationScreen from '../Signup/EmailConfirmation';
import SignUpWithEmailScreen from '../Signup/SignupEmail';
import LoginVerificationScreen from '../Login/LoginVerification';
import OnBoarding from '../Onboarding/OnBoarding';
import ChangePasswordScreen from '../ForgetPassword/ChangePasswordScreen';
import { onBoardingStatuses } from '../Onboarding/additionalInfoConfig';

const Navigation = createNativeStackNavigator();

function AuthNavigation() {
	const {
		params: { isOnboarded },
	} = useRoute();

	const forFade = ({ current }) => ({
		cardStyle: {
			opacity: current.progress,
		},
	});

	return (
		<Navigation.Navigator screenOptions={defaultScreenOptions}>
			{isOnboarded !== onBoardingStatuses.onBoarded && (
				<Navigation.Screen
					options={{ cardStyleInterpolator: forFade }}
					name="onBoarding"
					component={OnBoarding}
				/>
			)}
			<Navigation.Screen name="login" component={LoginScreen} />
			<Navigation.Screen
				name="forgetPasswordCode"
				component={ForgetPasswordSmsVerificationScreen}
			/>
			<Navigation.Screen
				name="forgetPassword"
				component={ForgetPasswordScreen}
			/>
			<Navigation.Screen
				name="changePassword"
				component={ChangePasswordScreen}
			/>
			<Navigation.Screen
				name="emailConfirmation"
				component={EmailConfirmationScreen}
			/>
			<Navigation.Screen
				name="signUpWithEmail"
				component={SignUpWithEmailScreen}
			/>
			<Navigation.Screen
				name="loginVerification"
				component={LoginVerificationScreen}
			/>
		</Navigation.Navigator>
	);
}
export default AuthNavigation;
