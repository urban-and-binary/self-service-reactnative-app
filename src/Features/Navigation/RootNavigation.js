import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { inject, observer } from 'mobx-react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { defaultScreenOptions } from './navigationConfig';
import AdditionalInfoScreen from '../Onboarding/AdditionalInfo';
import { NavigationBar } from './NavigationBar';
import BillsScreen from '../Bills/BillsScreen';
import RequestsScreen from '../Requests/RequestsScreen';
import EasyPandaScreen from '../EasyPanda/EasyPandaScreen';
import MetersNavigation from '../Meters/MetersNavigation';
import HomeNavigation from '../Home/HomeNavigation';

const Navigation = createNativeStackNavigator();
const Tab = createBottomTabNavigator();

function TabsNavigator() {
	const renderNavigationBar = props => <NavigationBar {...props} />;
	return (
		<Tab.Navigator
			initialRouteName="Home"
			backBehavior="none"
			tabBar={renderNavigationBar}
			tabBarPosition="bottom"
			screenOptions={defaultScreenOptions}>
			<Tab.Screen name="Home" component={HomeNavigation} />
			<Tab.Screen name="Bills" component={BillsScreen} />
			<Tab.Screen name="Requests" component={RequestsScreen} />
			<Tab.Screen name="Meters" component={MetersNavigation} />
			<Tab.Screen name="EasyPanda" component={EasyPandaScreen} />
		</Tab.Navigator>
	);
}

const RootNavigation = inject('store')(
	observer(({ store: { auth } }) => {
		if (!auth.isOnboarded) {
			return (
				<Navigation.Navigator screenOptions={defaultScreenOptions}>
					<Navigation.Screen
						name="additionalInfo"
						component={AdditionalInfoScreen}
					/>
				</Navigation.Navigator>
			);
		}
		return (
			<Navigation.Navigator screenOptions={defaultScreenOptions}>
				<Navigation.Screen name="tabs" component={TabsNavigator} />
			</Navigation.Navigator>
		);
	}),
);
export default RootNavigation;
