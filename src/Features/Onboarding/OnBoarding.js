import React from 'react';
import { Image, StyleSheet } from 'react-native';
import Swiper from 'react-native-swiper';
import { useNavigation } from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { LogoText, Screen, Text, View } from '../theme';
import { theme } from '../theme/themeConfig';
import {
	ON_BOARDING_STORAGE_KEY,
	onBoardingStatuses,
} from './additionalInfoConfig';

function OnBoarding() {
	const navigation = useNavigation();
	const handleSkip = () => {
		try {
			AsyncStorage.setItem(
				ON_BOARDING_STORAGE_KEY,
				onBoardingStatuses.onBoarded,
			).then(() => navigation.navigate('login'));
		} catch (e) {
			console.warn('cannot write on-boarding status in the storage');
		}
	};
	return (
		<Screen
			translucent
			barColor="transparent"
			barStyle="light-content"
			unsafe>
			<View
				gradientColors={[
					theme.paletteAuto.gradient.start,
					theme.paletteAuto.gradient.middle,
					theme.paletteAuto.gradient.end,
				]}
				flex
				center>
				<Swiper
					loop={false}
					dotColor={theme.palette.primaryColors.color3}
					activeDotColor={theme.palette.baseColors.base7}>
					<P1 />
					<P2 />
					<P3 />
				</Swiper>
				<Text
					onPress={handleSkip}
					color={theme.palette.baseColors.base7}
					variant={theme.typo.fontVariants.xsRegular}
					paddingBottom={theme.pads.sectionVerticalPad}>
					Skip intro
				</Text>
			</View>
		</Screen>
	);
}

function P1() {
	return (
		<View center flex>
			<LogoText width="28%" style={defautlStyles.logoText} />
			<Text
				color={theme.palette.baseColors.base7}
				paddingTop={theme.pads.sectionVerticalPad}
				variant={theme.typo.fontVariants.h1Bold}>
				Welcome!
			</Text>
			<Text
				paddingVertical={10}
				textAlign="center"
				paddingHorizontal="15%"
				variant={theme.typo.fontVariants.smallMedium}
				color={theme.palette.baseColors.base7}>
				With Civinity Namai you can pay all your bills at once.
			</Text>
			<Image
				style={defautlStyles.pt1Image}
				source={require('../../assets/images/money.png')}
			/>
		</View>
	);
}

function P2() {
	return (
		<View center flex>
			<LogoText width="28%" style={defautlStyles.logoText} />
			<Text
				color={theme.palette.baseColors.base7}
				paddingTop={theme.pads.sectionVerticalPad}
				variant={theme.typo.fontVariants.h1Bold}>
				Announcements!
			</Text>
			<Text
				paddingVertical={10}
				textAlign="center"
				paddingHorizontal="15%"
				variant={theme.typo.fontVariants.smallMedium}
				color={theme.palette.baseColors.base7}>
				Find all your pending announcements in one place.
			</Text>
			<Image
				style={defautlStyles.pt1Image}
				source={require('../../assets/images/WalkingPerson.png')}
			/>
		</View>
	);
}
function P3() {
	return (
		<View center flex>
			<LogoText width="28%" style={defautlStyles.logoText} />
			<Text
				color={theme.palette.baseColors.base7}
				paddingTop={theme.pads.sectionVerticalPad}
				variant={theme.typo.fontVariants.h1Bold}>
				Requests!
			</Text>
			<Text
				paddingVertical={10}
				textAlign="center"
				paddingHorizontal="15%"
				variant={theme.typo.fontVariants.smallMedium}
				color={theme.palette.baseColors.base7}>
				Have a request? Send it right now from wherever you are.
			</Text>
			<Image
				style={defautlStyles.pt1Image}
				source={require('../../assets/images/Rocket.png')}
			/>
		</View>
	);
}

const defautlStyles = StyleSheet.create({
	logoText: {
		marginTop: 10,
		marginBottom: 20,
	},
	pt1Image: {
		resizeMode: 'contain',
		width: theme.dimensions.screenWidth / 1.5,
		height: theme.dimensions.screenHeight / 3,
	},
});

export default OnBoarding;
