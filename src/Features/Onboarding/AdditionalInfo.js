import React, { useRef, useState } from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native';
import { inject, observer } from 'mobx-react';
import { Button, Screen, Text, TextInput, View } from '../theme';
import { theme } from '../theme/themeConfig';
import { CheckBox } from '../theme/CheckBox';
import { useTranslation } from '../translation/useTranslation';
import { usePutProfileInfo } from '../Profile/api';
import HideByKeyboard from '../keyboard/HideByKeyboard';
import { TextInputEmail } from '../theme/TextInputEmail';
import { TextInputPhone } from '../theme/TextInputPhone';
import { initialAdditionalInfoValues } from './additionalInfoConfig';
import { isPhoneValidated } from '../theme/validationUtil';
import { usePutUserSettings } from '../Settings/api';
import { invoiceReceiveFormat } from '../Settings/settingsConfig';

const AdditionalInfoScreen = inject('store')(
	observer(({ store: { auth, assign } }) => {
		const [activeTab, setActiveTab] = useState(0);
		const formValues = useRef({
			...initialAdditionalInfoValues,
			profileInfo: {
				email: auth.email,
				...initialAdditionalInfoValues.profileInfo,
			},
		});
		const dispatchableRef = useRef({});
		const submitSettings = usePutUserSettings({
			onSuccess: () => {
				assign({ ...dispatchableRef.current });
			},
		});

		const submitInfo = usePutProfileInfo({
			onSuccess: ({ body }) => {
				if (body?.isOnboarded) {
					submitSettings.mutate({ ...formValues.current });
					dispatchableRef.current = {
						auth: { ...auth, ...body },
						isLoaded: false,
					};
				}
			},
		});

		const setFormValues = newValues => {
			formValues.current = { ...formValues.current, ...newValues };
		};

		const handleSubmit = values => {
			setFormValues(values);
			submitInfo.mutate({ ...values.profileInfo });
		};

		const tabConfig = [
			{
				label: '1',
				description: 'screens.additionalInfo.p1.description',
				component: (
					<ProfileEditInfo
						onFormValueChanged={setFormValues}
						additionalInfo={formValues.current}
						onChangePage={setActiveTab}
					/>
				),
			},
			{
				label: '2',
				description: '',
				component: (
					<P2
						loading={
							submitSettings.isLoading || submitInfo.isLoading
						}
						onSubmit={handleSubmit}
						onFormValueChanged={setFormValues}
						additionalInfo={formValues.current}
						onChangePage={setActiveTab}
					/>
				),
			},
		];

		return (
			<Screen
				preset="scroll"
				flex
				contentFlexGrow
				translucent={false}
				horizontalPad
				verticalPad
				barColor={theme.palette.primaryColors.color2}
				barStyle="light-content">
				<View flex justifyContent="center" alignItems="center">
					<HideByKeyboard center>
						<Text variant={theme.typo.fontVariants.h3Bold}>
							screens.additionalInfo.title
						</Text>
						<Text
							paddingTop={20}
							textAlign="center"
							variant={theme.typo.fontVariants.smallRegular}>
							{tabConfig[activeTab].description}
						</Text>
					</HideByKeyboard>
					<View paddingVertical={25} row justifyContent="center">
						{tabConfig.map((tab, index) => (
							<TabButton
								index={index}
								key={index}
								activeTab={activeTab}
								label={tabConfig[index].label}
							/>
						))}
					</View>
					<View width="100%">{tabConfig[activeTab].component}</View>
				</View>
			</Screen>
		);
	}),
);

function TabButton({ activeTab, index, label }) {
	const style =
		activeTab < index
			? defaultStyle.inactiveButton
			: activeTab === index
			? defaultStyle.activeButton
			: defaultStyle.passedButton;

	const textColor =
		activeTab < index
			? theme.palette.primaryColors.color2
			: activeTab === index
			? theme.palette.baseColors.base7
			: theme.palette.baseColors.base4;
	return (
		<TouchableOpacity style={style}>
			<Text variant={theme.typo.fontVariants.smallBold} color={textColor}>
				{label}
			</Text>
		</TouchableOpacity>
	);
}

export function ProfileEditInfo({
	onFormValueChanged,
	additionalInfo,
	onChangePage,
	isProtected,
	loading,
}) {
	const t = useTranslation();
	const [formValues, setFormValues] = useState({ ...additionalInfo });
	const [errors, setErrors] = useState({});
	const handleFormValues = (key, value) =>
		setFormValues(prevState => ({
			...prevState,
			profileInfo: { ...prevState.profileInfo, [key]: value },
		}));

	const isFormValidated = (shouldHandleErrors = false) => {
		if (shouldHandleErrors) {
			setErrors({
				name:
					!formValues?.profileInfo.name && 'Please write your name.',
			});
		}
		return (
			!!formValues?.profileInfo.name &&
			(isPhoneValidated(formValues?.profileInfo?.phone) ||
				formValues?.profileInfo.phone === '')
		);
	};
	const handleSubmit = () => {
		if (isFormValidated(true)) {
			!!onFormValueChanged && onFormValueChanged(formValues);
			!!onChangePage && onChangePage(1);
		}
	};

	return (
		<View>
			<TextInput
				marginBottom={25}
				isRequired
				value={formValues?.profileInfo.name}
				error={errors?.name}
				label="general.form.fullName"
				placeholder={t('screens.additionalInfo.p1.fullNamePlaceholder')}
				onChangeText={value => handleFormValues('name', value)}
			/>
			<TextInputEmail
				marginBottom={25}
				isRequired
				value={formValues?.profileInfo.email}
				disabled
				uneditable
				label="general.form.contactEmail"
				placeholder={t('screens.additionalInfo.p1.emailPlaceholder')}
			/>
			<TextInputPhone
				disabled={isProtected}
				uneditable={isProtected}
				marginBottom={25}
				error={errors?.phone}
				value={formValues?.profileInfo.phone}
				keyboardType="phone-pad"
				label="general.form.contactPhone"
				placeholder={t('screens.additionalInfo.p1.phonePlaceholder')}
				onChangeText={value => handleFormValues('phone', value)}
			/>
			<View row justifyContent="flex-end">
				<Button
					loading={loading}
					onPress={handleSubmit}
					disabled={!isFormValidated()}
					minWidth={isProtected ? '100%' : '47%'}
					label={
						isProtected ? 'general.form.save' : 'general.form.next'
					}
				/>
			</View>
		</View>
	);
}

function P2({
	onFormValueChanged,
	additionalInfo,
	onChangePage,
	onSubmit,
	loading,
}) {
	const t = useTranslation();
	const [formValues, setFormValues] = useState({ ...additionalInfo });

	const handleFormValues = (key, value, property = 'agreements') => {
		setFormValues(prevState => ({
			...prevState,
			[property]: {
				...prevState[property],
				[key]: value,
			},
		}));
	};
	const handleBack = () => {
		onFormValueChanged(formValues);
		onChangePage(0);
	};
	const handleNext = () => {
		onSubmit({ ...formValues, isOnboarded: true });
	};
	const isAggrementsApproved = () =>
		!!formValues?.agreements?.userRules &&
		!!formValues?.agreements?.personalData &&
		!!formValues?.agreements?.privacyPolicy;

	const isBillsFormatMatchedWithAddress = () =>
		formValues?.bills.receiveFormat === invoiceReceiveFormat.digital ||
		!!formValues?.profileInfo.address;

	const isFormValidated = () =>
		isAggrementsApproved() && !!isBillsFormatMatchedWithAddress();

	return (
		<View>
			<TextInput
				isRequired={formValues?.bills.receiveFormat === invoiceReceiveFormat.paper}
				onChangeText={value =>
					handleFormValues('address', value, 'profileInfo')
				}
				marginBottom={25}
				value={formValues?.bills.address}
				label={t(
					formValues?.bills.receiveFormat === invoiceReceiveFormat.paper
						? 'screens.additionalInfo.p2.addressRequired'
						: 'screens.additionalInfo.p2.address',
				)}
				placeholder={t('screens.additionalInfo.p2.addressPlaceholder')}
			/>
			<View paddingRight={50}>
				<CheckBox
					initialValue={formValues?.agreements.privacyPolicy}
					onChangeValue={value =>
						handleFormValues('privacyPolicy', value)
					}
					label="screens.additionalInfo.p2.privacyPolicy"
					marginBottom={20}
				/>
				<CheckBox
					initialValue={formValues?.agreements.personalData}
					onChangeValue={value =>
						handleFormValues('personalData', value)
					}
					label="screens.additionalInfo.p2.personalData"
					marginBottom={20}
				/>
				<CheckBox
					initialValue={formValues?.agreements.userRules}
					onChangeValue={value =>
						handleFormValues('userRules', value)
					}
					label="screens.additionalInfo.p2.userRules"
					marginBottom={20}
				/>
				<CheckBox
					initialValue={formValues?.bills.receiveFormat !== invoiceReceiveFormat.paper}
					onChangeValue={value =>
						handleFormValues(
							'receiveFormat',
							value ? invoiceReceiveFormat.digital : invoiceReceiveFormat.paper,
							'bills',
						)
					}
					label="screens.additionalInfo.p2.bills"
					marginBottom={20}
				/>
				<CheckBox
					initialValue={formValues?.agreements.advertisment}
					onChangeValue={value =>
						handleFormValues('advertisment', value)
					}
					label="screens.additionalInfo.p2.advertisment"
					marginBottom={20}
				/>
			</View>

			<View row justifyContent="space-between">
				<Button
					onPress={handleBack}
					outline
					minWidth="47%"
					label="general.form.back"
				/>
				<Button
					loading={loading}
					disabled={!isFormValidated()}
					onPress={handleNext}
					minWidth="47%"
					label="general.form.next"
				/>
			</View>
		</View>
	);
}

const defaultStyle = StyleSheet.create({
	activeButton: {
		backgroundColor: theme.palette.primaryColors.color2,
		borderRadius: theme.roundness.small,
		paddingVertical: 5,
		paddingHorizontal: 10,
		marginHorizontal: 5,
	},
	passedButton: {
		paddingVertical: 5,
		paddingHorizontal: 10,
		marginHorizontal: 5,
	},
	inactiveButton: {
		backgroundColor: theme.palette.primaryColors.color4,
		borderRadius: theme.roundness.small,
		paddingVertical: 5,
		paddingHorizontal: 10,
		marginHorizontal: 5,
	},
});

export default AdditionalInfoScreen;
