export const initialAdditionalInfoValues = {
	profileInfo: {
		name: '',
		phone: '',
		address: '',
		isOnboarded: true,
	},
	agreements: {
		privacyPolicy: true,
		personalData: true,
		userRules: true,
		advertisment: false,
	},
	bills: {
		receiveFormat: 'paper',
	},
};

export const ON_BOARDING_STORAGE_KEY = '@isOnboarded';
export const onBoardingStatuses = {
	onBoarded: 'true',
	notOnboarded: 'false',
};
