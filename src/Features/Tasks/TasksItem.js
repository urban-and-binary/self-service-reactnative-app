import React, { useState } from 'react';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Modal from 'react-native-modal';
import { StyleSheet } from 'react-native';
import { Button, Card, HorizontalLine, Tag, Text, View } from '../theme';
import { theme } from '../theme/themeConfig';

function TasksItem({ task, fullDetails, ...props }) {
	let tagStatus;
	switch (task?.tag) {
		case 'Planed':
			tagStatus = 'info';
			break;
		case 'Done':
			tagStatus = 'success';
			break;
		case 'Pending':
			tagStatus = 'danger';
			break;
		case 'In progress':
			tagStatus = 'warning';
			break;
		default:
			tagStatus = 'disabled';
			break;
	}

	return (
		<Card
			backgroundColor={
				task?.tag === 'Planed'
					? theme.palette.primaryColors.color5
					: theme.palette.baseColors.base7
			}
			shadow={
				task?.tag === 'Planed' ? theme.shadows.none : theme.shadows.deep
			}
			{...props}>
			<View
				row
				justifyContent="space-between"
				alignItems="flex-start"
				marginBottom={15}>
				<Text variant={theme.typo.fontVariants.smallBold} flex>
					{task?.title || 'Unknow title'}
				</Text>
				{!fullDetails && (
					<Ionicons
						size={theme.typo.fontSize.semiLarge}
						name="chevron-forward"
					/>
				)}
			</View>
			<View row>
				<Tag label={task?.tag || 'Unknown'} status={tagStatus} />
			</View>

			<Text
				paddingTop={20}
				paddingBottom={10}
				variant={theme.typo.fontVariants.smallRegular}>
				{task?.address || 'Unknown address'}
			</Text>
			<TaskDetailItem label="Start day" value={task?.from} />
			<TaskDetailItem label="End day" value={task?.to} />
			{fullDetails && (
				<>
					{task.moreItems.map(({ label, value }, index) => (
						<TaskDetailItem key={index} {...{ label, value }} />
					))}
					<HorizontalLine />
					<TaskAttachmentItem
						task={task}
						label="Atliktų darbų aktas"
						type="document"
					/>
					<TaskAttachmentItem
						label="before photo"
						isImage
						task={task}
					/>
					<TaskAttachmentItem
						label="after photo"
						isImage
						task={task}
					/>
				</>
			)}
		</Card>
	);
}

function TaskAttachmentItem({ label, isImage, task }) {
	const [showModal, setShowModal] = useState(false);
	const handleShowModal = () => {
		setShowModal(true);
	};
	const handleHideModal = () => {
		setShowModal(false);
	};
	return (
		<View row marginBottom={15}>
			<View
				onPress={handleShowModal}
				row
				paddingHorizontal={20}
				paddingVertical={8}
				borderRadius={theme.roundness.small}
				backgroundColor={theme.palette.baseColors.base6}>
				<Ionicons
					name={isImage ? 'ios-image-outline' : 'attach'}
					size={theme.typo.fontSize.medium}
				/>
				<Text paddingLeft={10}>{label}</Text>
			</View>
			<TaskDownloadModal
				task={task}
				visible={showModal}
				onRequestClose={handleHideModal}
			/>
		</View>
	);
}

function TaskDetailItem({ label, value }) {
	return (
		<View row paddingVertical={10}>
			<Text
				color={theme.palette.baseColors.base3}
				width="35%"
				variant={theme.typo.fontVariants.smallBold}>
				{label || 'Label'}:
			</Text>
			<Text flex variant={theme.typo.fontVariants.smallRegular}>
				{value || 'Value'}
			</Text>
		</View>
	);
}

function TaskDownloadModal({
	visible,
	onRequestClose,
	children,
	task,
	...props
}) {
	return (
		<Modal
			onRequestClose={onRequestClose}
			onBackButtonPress={onRequestClose}
			onBackdropPress={onRequestClose}
			onDismiss={onRequestClose}
			isVisible={visible}
			hasBackdrop
			backdropColor="black"
			statusBarTranslucent
			animationInTiming={500}
			animationOutTiming={500}
			backdropTransitionInTiming={750}
			backdropTransitionOutTiming={500}
			useNativeDriverForBackdrop
			style={defaultStyles.view}
			{...props}>
			<View paddingHorizontal={theme.pads.horizontalPad}>
				<TasksItem task={task} />
				<Button label="Download" marginVertical={20} />
				<Button label="Overview" />
			</View>
		</Modal>
	);
}

const defaultStyles = StyleSheet.create({
	view: {
		justifyContent: 'center',
		margin: 0,
	},
});

export default TasksItem;
