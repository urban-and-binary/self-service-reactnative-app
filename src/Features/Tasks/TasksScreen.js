import React from 'react';
import { useNavigation } from '@react-navigation/native';
import { Screen, Text } from '../theme';
import { theme } from '../theme/themeConfig';
import TasksItem from './TasksItem';
import { tasksList } from './tasksFakeData';

function TasksScreen() {
	const navigation = useNavigation();
	const renderTask = (item, index) => (
		<TasksItem
			key={index}
			task={item}
			onPress={() => navigation.navigate('taskDetails', { task: item })}
		/>
	);
	return (
		<Screen
			preset="scroll"
			translucent
			barStyle="dark-content"
			barColor="transparent"
			paddingHorizontal={theme.pads.horizontalPad}
			topBar>
			<Text variant={theme.typo.fontVariants.h1Bold} paddingBottom={20}>
				Tasks
			</Text>
			{tasksList.map(renderTask)}
		</Screen>
	);
}
export default TasksScreen;
