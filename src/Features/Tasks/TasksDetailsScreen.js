import React from 'react';
import { useRoute } from '@react-navigation/native';
import { BackButton, Screen } from '../theme';
import { theme } from '../theme/themeConfig';
import TasksItem from './TasksItem';

function TasksDetailsScreen() {
	const { params } = useRoute();
	return (
		<Screen
			preset="scroll"
			translucent
			barStyle="dark-content"
			barColor="transparent"
			paddingHorizontal={theme.pads.horizontalPad}>
			<BackButton showLabel marginBottom={20} />
			<TasksItem task={params.task} fullDetails />
		</Screen>
	);
}
export default TasksDetailsScreen;
