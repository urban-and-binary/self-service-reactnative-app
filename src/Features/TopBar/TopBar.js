import React, { useState } from 'react';
import { StyleSheet } from 'react-native';
import { inject, observer } from 'mobx-react';
import { useNavigation } from '@react-navigation/native';
import { CircleButton, View, DropDown } from '../theme';
import { theme } from '../theme/themeConfig';
import { Drawer } from '../Drawer/Drawer';
import { useQueryAnnouncements } from '../Announcements/api';

export const TopBar = inject('store')(
	observer(({ insetPad }) => {
		const [isDrawerVisible, setIsDrawerVisible] = useState(false);
		const navigation = useNavigation();
		const handleCloseModal = () => setIsDrawerVisible(false);
		const handleOpenModal = () => setIsDrawerVisible(true);
		const openAnnouncents = () => navigation.navigate('announcements');
		const { data: { data = [] } = {} } = useQueryAnnouncements();
		const announcements = data.filter(item => !item?.read).length;
		return (
			<View
				paddingTop={insetPad + 10}
				paddingHorizontal={theme.pads.horizontalPad}
				row
				center
				style={defaultStyles.container}>
				<View flex>
					<DropDown initialValue="All Units" />
				</View>
				<CircleButton
					onPress={openAnnouncents}
					badge={announcements}
					marginHorizontal={10}
					iconName="notifications-outline"
				/>
				<CircleButton onPress={handleOpenModal} iconName="menu" />
				<Drawer
					onRequestClose={handleCloseModal}
					onBackButtonPress={handleCloseModal}
					onBackdropPress={handleCloseModal}
					visible={isDrawerVisible}
				/>
			</View>
		);
	}),
);

const defaultStyles = StyleSheet.create({
	container: {
		...theme.shadows.deep,
		backgroundColor: theme.palette.baseColors.base7,
		borderBottomRightRadius: theme.roundness.medium,
		borderBottomLeftRadius: theme.roundness.medium,
		paddingBottom: 15,
	},
});
