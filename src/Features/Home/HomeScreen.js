import React from 'react';
import { Image } from 'react-native';
import { Button, Card, Screen, Tag, Text, View } from '../theme';
import { theme } from '../theme/themeConfig';
import { FeatureIcon } from './FeatureIcon';

export default function HomeScreen() {
	return (
		<Screen
			preset="scroll"
			topBar
			barStyle="dark-content"
			paddingHorizontal={theme.pads.horizontalPad}>
			<Text variant={theme.typo.fontVariants.h1Bold} paddingBottom={20}>
				Home page
			</Text>
			<View flex>
				<View
					borderRadius={theme.roundness.large}
					justifyContent="flex-start"
					alignItems="center"
					start={{ x: 0, y: 1 }}
					end={{ x: 1, y: 1 }}
					paddingHorizontal="5%"
					paddingTop={theme.pads.sectionVerticalPad}
					gradientColors={[
						theme.palette.primaryColors.color1,
						theme.palette.primaryColors.color4,
					]}>
					<View
						opacity={0.6}
						position="absolute"
						center
						marginTop="5%"
						width="100%"
						height="80%"
						paddingHorizontal={2}
						paddingVertical={2}
						start={{ x: 0, y: 1 }}
						end={{ x: 1, y: 1 }}
						gradientColors={[
							theme.palette.primaryColors.color3,
							theme.palette.baseColors.base6,
						]}
						borderRadius={theme.roundness.medium}>
						<View
							width="100%"
							height="100%"
							opacity={0.5}
							borderRadius={theme.roundness.medium}
							gradientColors={[
								theme.palette.primaryColors.color1,
								theme.palette.baseColors.base3,
							]}
						/>
					</View>
					<View
						paddingHorizontal={theme.pads.horizontalPad}
						paddingVertical={theme.pads.horizontalPad}
						width="100%">
						<View>
							<View center>
								<Text
									variant={theme.typo.fontVariants.baseBold}
									color={theme.palette.baseColors.base7}>
									Amount to pay
								</Text>
								<Text
									size={48}
									weight={700}
									color={theme.palette.baseColors.base7}>
									€ 329.00
								</Text>
							</View>
							<View center marginBottom={20}>
								<Tag label="Unpaid" />
							</View>
							<Button secondary label="Overview and pay" />
							<View center marginTop={10}>
								<Image
									source={require('../../assets/images/money.png')}
								/>
							</View>
						</View>
					</View>
				</View>
				<Card
					marginTop={theme.pads.sectionVerticalPad}
					title="Pending announcements">
					<HomeFeatureIcon
						iconName="home"
						title="Votings"
						description="Šiandien nuo 12:00 iki 15:00 jūsų name nebus elektros"
					/>
					<HomeFeatureIcon
						iconName="flag"
						iconBackground={theme.palette.primaryColors.color5}
						title="Announcements"
						description="Šiandien nuo 12:00 iki 15:00 jūsų name nebus elektros"
					/>
					<HomeFeatureIcon
						iconName="tool"
						iconBackground={theme.palette.statusColors.lightRed}
						title="Announcements"
						description="Šiandien nuo 12:00 iki 15:00 jūsų name nebus elektros"
					/>
					<HomeFeatureIcon
						iconName="flag"
						iconBackground={theme.palette.primaryColors.color5}
						title="Announcements"
						description="Šiandien nuo 12:00 iki 15:00 jūsų name nebus elektros"
					/>
					<HomeFeatureIcon
						iconName="flag"
						iconBackground={theme.palette.primaryColors.color5}
						title="Announcements"
						description="Šiandien nuo 12:00 iki 15:00 jūsų name nebus elektros"
					/>
					<View center paddingVertical={10}>
						<Text
							variant={theme.typo.fontVariants.baseMedium}
							onPress={() => null}>
							View all
						</Text>
					</View>
				</Card>
				<Card marginVertical={theme.pads.sectionVerticalPad}>
					<View center>
						<Text
							paddingVertical={20}
							variant={theme.typo.fontVariants.h3Bold}>
							Make a donation
						</Text>
						<Text
							variant={theme.typo.fontVariants.smallRegular}
							textAlign="center">
							Civinity jau antrus metus iš eilės remia „SOS vaikų
							kaimai“ labdaros ir paramos fondą, kuris globoja
							sudėtingose situacijose atsidūrusius vaikus ir daro
							viską, kad jie augtų laimingose ir saugiose šeimose.
							Nuo šiol tai galite padaryti ir jūs!
						</Text>
					</View>
					<Button marginVertical={20} label="Donate" />
				</Card>
			</View>
		</Screen>
	);
}

function HomeFeatureIcon({ iconName, iconBackground, title, description }) {
	return (
		<View row alignItems="center" paddingVertical={20}>
			<FeatureIcon backgroundColor={iconBackground} iconName={iconName} />
			<View paddingHorizontal={theme.pads.horizontalPad}>
				<Text variant={theme.typo.fontVariants.smallBold}>{title}</Text>
				<Text
					variant={theme.typo.fontVariants.smallRegular}
					paddingRight={theme.pads.screenHorizontalPad}>
					{description}
				</Text>
			</View>
		</View>
	);
}
