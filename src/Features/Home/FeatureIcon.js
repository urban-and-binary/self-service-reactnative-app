import Feather from 'react-native-vector-icons/Feather';
import React from 'react';
import { theme } from '../theme/themeConfig';
import { View } from '../theme';

export function FeatureIcon({
	backgroundColor = theme.palette.statusColors.lightViolet,
	fontColor = theme.palette.baseColors.base1,
	iconName = 'home',
}) {
	return (
		<View
			backgroundColor={backgroundColor}
			borderRadius={theme.roundness.medium}
			center
			width={50}
			height={50}>
			<Feather
				name={iconName}
				color={fontColor}
				size={theme.typo.fontSize.large}
			/>
		</View>
	);
}
