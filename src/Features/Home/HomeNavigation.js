import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { defaultScreenOptions } from '../Navigation/navigationConfig';
import HomeScreen from './HomeScreen';
import { drawerScreensArray } from '../Drawer/DrawerNavigation';

const Navigation = createNativeStackNavigator();

function HomeNavigation() {
	return (
		<Navigation.Navigator screenOptions={defaultScreenOptions}>
			<Navigation.Screen name="home" component={HomeScreen} />
			{drawerScreensArray.map(item => item)}
		</Navigation.Navigator>
	);
}
export default HomeNavigation;
