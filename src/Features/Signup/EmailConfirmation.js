import React from 'react';
import { Image, Platform, StyleSheet } from 'react-native';
import { inject, observer } from 'mobx-react';
import { Breadcumb, Screen, Text, View, BackButton } from '../theme';
import { theme } from '../theme/themeConfig';
import { useTranslation } from '../translation/useTranslation';

const EmailConfirmationScreen = inject('store')(
	observer(() => {
		const t = useTranslation();
		const imageSource = require('../../assets/images/saly25.png');
		return (
			<Screen
				translucent={false}
				horizontalPad
				verticalPad
				barColor={theme.palette.primaryColors.color2}
				barStyle={
					Platform.OS === 'ios' ? 'dark-content' : 'light-content'
				}>
				<Breadcumb
					leftComponent={<BackButton />}
					rightComponent={<View />}
				/>
				<Image
					style={defaultStyle.image}
					source={imageSource}
					height={80}
					resizeMode="contain"
				/>
				<View flex center>
					<Text
						paddingTop={40}
						variant={theme.typo.fontVariants.h3Bold}>
						screens.emailConfirmation.title
					</Text>
					<Text
						paddingTop={40}
						textAlign="center"
						variant={theme.typo.fontVariants.baseRegular}>
						{t('screens.emailConfirmation.description')}
						<Text
							textAlign="center"
							color={theme.palette.primaryColors.color2}
							variant={theme.typo.fontVariants.baseBold}>
							ali@sdhouse.lt
						</Text>
					</Text>

					<Text
						textAlign="center"
						variant={theme.typo.fontVariants.baseRegular}
						paddingTop={20}>
						screens.emailConfirmation.notReceived
					</Text>
					<Text
						style={defaultStyle.tryAgain}
						variant={theme.typo.fontVariants.baseBold}
						color={theme.palette.primaryColors.color2}>
						general.form.tryAgain
					</Text>
				</View>
			</Screen>
		);
	}),
);

const defaultStyle = StyleSheet.create({
	image: {
		position: 'absolute',
		width: theme.dimensions.screenWidth,
		top: theme.dimensions.screenHeight / 4,
	},
	tryAgain: {
		position: 'absolute',
		bottom: theme.dimensions.screenHeight / 8,
	},
});

export default EmailConfirmationScreen;
