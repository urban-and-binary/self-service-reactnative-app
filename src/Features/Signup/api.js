import { useMutation } from '../reactQuery/hooks';

export const useSignup = (option = {}) => useMutation('auth/signup', option);
