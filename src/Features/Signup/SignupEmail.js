import React, { useState } from 'react';
import { Platform, StyleSheet } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { Breadcumb, Button, Screen, Text, TextInput, View } from '../theme';
import { theme } from '../theme/themeConfig';
import { BreadcumbLanguage } from '../Breadcrumbs/BreadcumbLanguage';
import { CheckBox } from '../theme/CheckBox';
import { useTranslation } from '../translation/useTranslation';
import { useSignup } from './api';
import { isEmailValidated } from '../theme/validationUtil';
import { TextInputEmail } from '../theme/TextInputEmail';

function SignupWithEmailScreen() {
	const [errors, setErrors] = useState({});
	const navigation = useNavigation();
	const t = useTranslation();
	const [password, setPassword] = useState('');
	const [email, setEmail] = useState('');
	const [termsAccepted, setTermsAccepted] = useState(false);
	const handleAcceptRules = checked => {
		setErrors(prevState => ({ ...prevState, terms: '' }));
		setTermsAccepted(checked);
	};

	const signup = useSignup({
		onSuccess: () => {
			navigation.navigate('loginVerification', {
				target: email,
				password,
				isEmailConfirmation: true,
			});
		},
	});

	const formValidation = (shouldHandleErrors = false) => {
		shouldHandleErrors &&
			setErrors({
				email: !isEmailValidated(email) && 'general.errors.signUpEmail',
				password: !password && 'general.errors.signUpPassword',
				terms: !termsAccepted && 'general.errors.signUpTerms',
			});
		return isEmailValidated(email) && !!password && !!termsAccepted;
	};

	const doSignUp = () => {
		formValidation(true) &&
			signup.mutate({
				email,
				password,
				loginType: 'email',
			});
	};

	const navigateToLogin = () => navigation.navigate('login');
	return (
		<Screen
			preset="scroll"
			flex
			contentFlexGrow
			translucent={false}
			horizontalPad
			verticalPad
			barColor={theme.palette.primaryColors.color2}
			barStyle={Platform.OS === 'ios' ? 'dark-content' : 'light-content'}>
			<View>
				<Breadcumb
					leftComponent={<View />}
					rightComponent={<BreadcumbLanguage currentLanguage="en" />}
				/>
				<Text variant={theme.typo.fontVariants.h2Bold}>
					screens.signUp.title
				</Text>
				<Text variant={theme.typo.fontVariants.xsRegular}>
					screens.signUp.description
				</Text>
			</View>
			<View flex paddingTop={30}>
				<TextInputEmail
					isRequired
					onChangeText={setEmail}
					label="general.form.email"
					placeholder={t('screens.login.emailPlaceholder')}
				/>
				<TextInput
					error={errors?.password}
					marginTop={15}
					onChangeText={setPassword}
					hideUnderline
					secureTextEntry
					isRequired
					label="general.form.password"
					placeholder={t('general.form.password')}
					containerStyle={defaultStyles.passwordInput}
				/>
				<View paddingTop={10} paddingRight={30}>
					<CheckBox
						onChangeValue={handleAcceptRules}
						label={
							<Text
								color={
									errors?.terms
										? theme.palette.statusColors.danger
										: theme.paletteAuto.text.primary
								}
								size={theme.typo.fontSize.xSmall}>
								{t('screens.signUp.condition')}{' '}
								<Text
									color={theme.palette.primaryColors.color2}
									size={theme.typo.fontSize.xSmall}>
									general.form.terms
								</Text>
							</Text>
						}
					/>
				</View>

				<View flex justifyContent="flex-end">
					<Button
						disabled={!formValidation()}
						loading={signup.isLoading}
						label="general.form.signup"
						onPress={doSignUp}
					/>
					<View row paddingVertical={30} center>
						<Text variant={theme.typo.fontVariants.smallRegular}>
							screens.signUp.haveAccount
						</Text>
						<Text
							paddingHorizontal={5}
							variant={theme.typo.fontVariants.smallBold}
							onPress={navigateToLogin}>
							general.form.login
						</Text>
					</View>
				</View>
			</View>
		</Screen>
	);
}

const defaultStyles = StyleSheet.create({
	passwordInput: {
		marginTop: 20,
	},
});

export default SignupWithEmailScreen;
