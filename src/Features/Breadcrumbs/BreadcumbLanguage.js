import React from 'react';
import { TouchableOpacity } from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import { Text, View } from '../theme';
import { theme } from '../theme/themeConfig';

export function BreadcumbLanguage({ currentLanguage }) {
	return (
		<TouchableOpacity>
			<View
				paddingTop={10}
				row
				justifyContent="center"
				alignItems="center">
				<Text size={25}>🇬🇧</Text>
				<Text paddingHorizontal={7} size={17} weight={400}>
					{currentLanguage.toUpperCase()}
				</Text>
				<AntDesign
					name="right"
					size={15}
					color={theme.palette.baseColors.base1}
				/>
			</View>
		</TouchableOpacity>
	);
}
