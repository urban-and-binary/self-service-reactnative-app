import React from 'react';
import { View } from '../theme';

export function Breadcumb({ leftComponent, rightComponent }) {
	return (
		<View row alignItems="center" justifyContent="space-between">
			{leftComponent}
			{rightComponent}
		</View>
	);
}
