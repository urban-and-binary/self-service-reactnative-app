import React, { useState } from 'react';
import { useNavigation } from '@react-navigation/native';
import { Button, Card, Text, TextAction, TextInput, View } from '../theme';
import { theme } from '../theme/themeConfig';
import { usePutProfileInfo } from './api';
import { useTranslation } from '../translation/useTranslation';

export default function ProfileAddressCard({ data = {}, refetch }) {
	const [isEditing, setIsEditing] = useState(false);

	const updateAddressInfo = usePutProfileInfo({
		onSuccess: () => {
			refetch();
			setIsEditing(false);
		},
	});

	const handleSaveInfo = formValue => {
		const { country, city, address } = formValue;
		updateAddressInfo.mutate({
			country,
			city,
			address,
		});
	};

	return (
		<Card expandable defaultExpand={false} title="screens.profile.address">
			{isEditing && (
				<EditAddress
					loading={updateAddressInfo.isLoading}
					data={data}
					onFormValueChanged={handleSaveInfo}
					setIsEditing={setIsEditing}
				/>
			)}
			{!isEditing && <ReadAddress {...{ data, setIsEditing }} />}
		</Card>
	);
}

function ReadAddress({ data, setIsEditing }) {
	const t = useTranslation();
	const navigation = useNavigation();

	return (
		<View>
			<Text
				paddingBottom={15}
				variant={theme.typo.fontVariants.smallBold}>
				screens.profile.addressSubtitle
			</Text>
			{data?.country && (
				<Text
					paddingBottom={15}
					variant={theme.typo.fontVariants.smallRegular}>
					{data.country}
				</Text>
			)}
			{data?.city && (
				<Text
					paddingBottom={15}
					variant={theme.typo.fontVariants.smallRegular}>
					{data.city}
				</Text>
			)}
			{data?.address && (
				<Text
					paddingBottom={15}
					variant={theme.typo.fontVariants.smallRegular}>
					{data.address}
				</Text>
			)}
			<Text
				paddingBottom={15}
				paddingRight={5}
				color={theme.palette.baseColors.grey}
				variant={theme.typo.fontVariants.xsRegular}>
				{t('screens.profile.changeSettingsText')}
				<Text onPress={() => navigation.navigate('settings')}>
					screens.profile.addressLinkToSettings
				</Text>
			</Text>
			<View alignItems="flex-end">
				<TextAction
					onPress={() => setIsEditing(true)}
					label="general.form.edit"
					featherIcon="edit-2"
				/>
			</View>
		</View>
	);
}

function EditAddress({ data, onFormValueChanged, loading, setIsEditing }) {
	const t = useTranslation();
	const navigation = useNavigation();
	const [formValue, setFormValue] = useState({ ...data });
	const handleFormValue = (key, value) => {
		setFormValue(prevState => ({ ...prevState, [key]: value }));
	};
	const handleSave = () => {
		!!onFormValueChanged && onFormValueChanged(formValue);
	};

	return (
		<View>
			<TextInput
				marginBottom={15}
				onChangeText={value => handleFormValue('country', value)}
				value={formValue.country}
				label="general.form.country"
			/>
			<TextInput
				marginBottom={15}
				onChangeText={value => handleFormValue('city', value)}
				value={formValue.city}
				label="general.form.city"
			/>
			<TextInput
				marginBottom={15}
				onChangeText={value => handleFormValue('address', value)}
				value={formValue.address}
				label="general.form.address"
			/>

			<Text
				paddingBottom={15}
				paddingRight={5}
				color={theme.palette.baseColors.grey}
				variant={theme.typo.fontVariants.xsRegular}>
				{t('screens.profile.changeSettingsText')}
				<Text onPress={() => navigation.navigate('settings')}>
					screens.profile.addressLinkToSettings
				</Text>
			</Text>
			<Button
				onPress={handleSave}
				loading={loading}
				label="general.form.saveChange"
			/>
			<Button
				outline
				marginTop={15}
				onPress={() => setIsEditing(false)}
				label="general.form.cancel"
			/>
		</View>
	);
}
