import { useMutation, useQuery } from '../reactQuery/hooks';

export const useChangePassword = (option = {}) =>
	useMutation('users/profile/password', option, 'PUT');

// This gonna use in ../Onboarding/AdditionalInfo.js feature too
export const usePutProfileInfo = (option = {}) =>
	useMutation('users/profile/info', option, 'PUT');

export const useProfile = option => useQuery('useProfile', 'users', option);
