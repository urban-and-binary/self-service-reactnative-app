import React, { useState } from 'react';
import { Card, Text, TextAction, View } from '../theme';
import { theme } from '../theme/themeConfig';
import { usePutProfileInfo } from './api';
import { ProfileEditInfo } from '../Onboarding/AdditionalInfo';

export default function ProfileInfoCard({ data = {}, refetch }) {
	const [isEditing, setIsEditing] = useState(false);

	const updatePersonalInfo = usePutProfileInfo({
		onSuccess: () => {
			refetch();
			setIsEditing(false);
		},
	});
	const handleSaveInfo = formValue => {
		const { name, phone, email } = formValue;
		updatePersonalInfo.mutate({
			name,
			phone,
			email,
		});
	};

	return (
		<Card
			expandable
			defaultExpand={false}
			title="screens.profile.personalInformation">
			{isEditing && (
				<ProfileEditInfo
					loading={updatePersonalInfo.isLoading}
					additionalInfo={{ profileInfo: data }}
					isProtected
					onFormValueChanged={handleSaveInfo}
				/>
			)}
			{!isEditing && (
				<View>
					<Text
						paddingBottom={15}
						variant={theme.typo.fontVariants.smallRegular}>
						{data?.name}
					</Text>
					<Text
						paddingBottom={15}
						variant={theme.typo.fontVariants.smallRegular}>
						{data?.email}
					</Text>
					<Text
						paddingBottom={15}
						variant={theme.typo.fontVariants.smallRegular}>
						{data?.phone}
					</Text>
					<View alignItems="flex-end">
						<TextAction
							onPress={() => setIsEditing(true)}
							label="general.form.edit"
							featherIcon="edit-2"
						/>
					</View>
				</View>
			)}
		</Card>
	);
}
