import React from 'react';
import { Screen, View } from '../theme';
import { theme } from '../theme/themeConfig';
import ProfileUnit from '../Units/ProfileUnit';
import ProfileInfoCard from './ProfileInfoCard';
import { useProfile } from './api';
import ProfileAddressCard from './ProfileAddressCard';
import ProfilePasswordCard from './ProfilePasswordCard';

function ProfileScreen() {
	const { data = {}, refetch } = useProfile();

	return (
		<Screen
			preset="scroll"
			translucent
			barStyle="dark-content"
			barColor="transparent"
			topBar>
			<View flex marginHorizontal={theme.pads.horizontalPad}>
				<ProfileInfoCard {...{ data, refetch }} />
				<ProfileAddressCard {...{ data, refetch }} />
				<ProfilePasswordCard {...{ refetch }} />
				<ProfileUnit />
			</View>
		</Screen>
	);
}
export default ProfileScreen;
