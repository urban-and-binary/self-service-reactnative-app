import React, { useCallback, useEffect, useState } from 'react';
import { Button, Card, Text, TextAction, TextInput, View } from '../theme';
import { theme } from '../theme/themeConfig';
import { useChangePassword } from './api';
import { useTranslation } from '../translation/useTranslation';
import { CheckBox } from '../theme/CheckBox';
import { hasOneCapitalLetter, hasOneNumber } from '../theme/validationUtil';

export default function ProfilePasswordCard({ refetch }) {
	const [isEditing, setIsEditing] = useState(false);
	const [formValue, setFormValue] = useState({});
	const [validations, setValidations] = useState({});
	const changePassword = useChangePassword({
		onSuccess: () => {
			refetch();
			setIsEditing(false);
		},
	});

	const handleSaveInfo = () => {
		changePassword.mutate(formValue);
	};
	const t = useTranslation();
	const handleFormValue = (key, value) => {
		setFormValue(prevState => ({ ...prevState, [key]: value }));
	};
	const handleCancel = () => {
		setFormValue({});
		setIsEditing(false);
	};

	const isValidated = useCallback(
		(shouldUpdateStates = false) => {
			const validationObject = {
				hasCapitalLetter:
					formValue?.newPassword &&
					hasOneCapitalLetter(formValue.newPassword),
				hasNumber:
					formValue?.newPassword &&
					hasOneNumber(formValue.newPassword),
				matched:
					!!formValue.newPassword &&
					formValue?.newPassword === formValue.repeatPassword,
				currentPassword:
					!!formValue?.currentPassword &&
					formValue.currentPassword !== formValue?.newPassword,
			};
			if (shouldUpdateStates) {
				setValidations(validationObject);
			}

			return Object.values(validationObject).every(item => !!item);
		},
		[formValue],
	);
	useEffect(() => {
		isValidated(true);
	}, [isValidated]);

	return (
		<Card expandable defaultExpand={false} title="general.form.password">
			<View>
				<TextInput
					isRequired
					secureTextEntry
					disabled={!isEditing}
					uneditable={!isEditing}
					marginBottom={15}
					value={formValue.currentPassword}
					onChangeText={value =>
						handleFormValue('currentPassword', value)
					}
					label="screens.profile.currentPassword"
					placeholder={t('screens.profile.currentPassword')}
				/>
				<TextInput
					error={
						!!formValue?.currentPassword &&
						formValue?.currentPassword === formValue?.newPassword &&
						'screens.profile.newPasswordError'
					}
					isRequired
					disabled={!isEditing}
					secureTextEntry
					uneditable={!isEditing}
					marginBottom={15}
					value={formValue.newPassword}
					onChangeText={value =>
						handleFormValue('newPassword', value)
					}
					label="screens.profile.newPassword"
					placeholder={t('screens.profile.newPassword')}
				/>
				<TextInput
					isRequired
					uneditable={!isEditing}
					secureTextEntry
					disabled={!isEditing}
					marginBottom={20}
					value={formValue.repeatPassword}
					onChangeText={value =>
						handleFormValue('repeatPassword', value)
					}
					label="screens.profile.retypePassword"
					placeholder={t('screens.profile.newPassword')}
				/>
				{isEditing && (
					<>
						<Text
							paddingBottom={10}
							variant={theme.typo.fontVariants.smallRegular}>
							screens.profile.passwordRules
						</Text>
						<CheckBox
							stateless
							initialValue={validations?.hasCapitalLetter}
							label="screens.profile.passwordOneLetter"
						/>
						<CheckBox
							stateless
							initialValue={validations?.hasNumber}
							label="screens.profile.passwordOneNumber"
						/>
						<CheckBox
							stateless
							initialValue={validations?.matched}
							label="screens.profile.passwordsMustMatch"
						/>
					</>
				)}
				{!isEditing && (
					<View alignItems="flex-end">
						<TextAction
							onPress={() => setIsEditing(true)}
							label="general.form.edit"
							featherIcon="edit-2"
						/>
					</View>
				)}
				{isEditing && (
					<>
						<Button
							disabled={!isValidated()}
							marginTop={15}
							onPress={handleSaveInfo}
							loading={changePassword.isLoading}
							label="general.form.saveChange"
						/>
						<Button
							outline
							marginTop={15}
							onPress={handleCancel}
							label="general.form.cancel"
						/>
					</>
				)}
			</View>
		</Card>
	);
}
