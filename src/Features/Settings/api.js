// This gonna use in ../Onboarding/AdditionalInfo.js feature too
import { useMutation } from '../reactQuery/hooks';

export const usePutUserSettings = (option = {}) =>
	useMutation('users/settings', option, 'PUT');
