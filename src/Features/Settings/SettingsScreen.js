import React from 'react';
import { Screen, Text, View } from '../theme';

function SettingsScreen() {
	return (
		<Screen>
			<View flex center>
				<Text>Settings Screen</Text>
			</View>
		</Screen>
	);
}
export default SettingsScreen;
