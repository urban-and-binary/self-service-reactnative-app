import React from 'react';
import { StyleSheet, Image } from 'react-native';
import { Text, View, Screen, Card } from '../theme';
import { theme } from '../theme/themeConfig';
import Ghost from '../../assets/images/empty-states/Ghost';

export default class ErrorBoundary extends React.Component {
	constructor(props) {
		super(props);
		this.state = { hasError: false, message: '', description: '' };
	}

	static getDerivedStateFromError({ message, description }) {
		return { hasError: true, message, description };
	}

	render() {
		const { hasError, message, description } = this.state;
		const { children } = this.props;
		if (hasError) {
			return (
				<Screen
					translucent={false}
					paddingHorizontal={theme.pads.horizontalPad}
					verticalPad
					barColor={theme.palette.primaryColors.color2}
					barStyle="light-content">
					<View center flex paddingVertical={20}>
						<Image
							style={defaultStyles.logoImage}
							source={require('../../assets/images/errors-text-image.png')}
						/>
						<Card width="100%" center marginTop={30} flex>
							<Ghost />

							<Text variant={theme.typo.fontVariants.baseRegular}>
								{message || 'general.errors.unknown'}
							</Text>
							<Text
								variant={theme.typo.fontVariants.smallRegular}>
								{description}
							</Text>
						</Card>
					</View>
				</Screen>
			);
		}

		return children;
	}
}
const defaultStyles = StyleSheet.create({
	logoImage: {
		width: '50%',
	},
});
