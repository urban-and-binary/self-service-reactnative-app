import { useMutation } from '../reactQuery/hooks';

export const useForgetPassword = (option = {}) =>
	useMutation('auth/password-forgot', option);

export const useResetPassword = (option = {}) =>
	useMutation('auth/password-change', option);

export const useVerifyForgetPasswordOtp = (option = {}) =>
	useMutation('auth/verify-code', option);
