import React, { createRef, useEffect, useRef, useState } from 'react';
import { StyleSheet, TextInput } from 'react-native';
import { inject, observer } from 'mobx-react';
import { useNavigation, useRoute } from '@react-navigation/native';
import { Breadcumb, Screen, Text, View, BackButton } from '../theme';
import { theme } from '../theme/themeConfig';
import { useForgetPassword, useVerifyForgetPasswordOtp } from './api';

const ForgetPasswordSmsVerificationScreen = inject('store')(
	observer(({ store: { setToast } }) => {
		const [code, setCode] = useState(['', '', '', '', '', '']);
		const [otpArr, setOtpArr] = useState([]);
		const {
			params: { email, phone },
		} = useRoute();
		const verificationCode = useRef('');
		const navigation = useNavigation();

		// for focusing on next text view
		const charRefs = useRef([]);
		charRefs.current = code.map(
			(_, i) => charRefs.current[i] ?? createRef(),
		);
		const verify = useVerifyForgetPasswordOtp({
			onSuccess: () => {
				navigation.navigate('changePassword', {
					email,
					phone,
					verificationCode: verificationCode.current,
				});
			},
		});

		const handleVerify = jointCode => {
			verify.mutate({
				email: email || undefined,
				phone: phone || undefined,
				verificationCode: jointCode,
			});
		};

		const forgetPassword = useForgetPassword({
			onSuccess: () => {
				setToast(
					'screens.loginVerification.resendMessageEmail',
					5,
					'success',
				);
			},
		});

		const handleResend = () => {
			forgetPassword.mutate({
				email: email || undefined,
				phone: phone || undefined,
			});
		};
		useEffect(() => {
			otpArr.length === 0 &&
				code.forEach((item, index) => {
					const otpCharObj = {
						placeholder: '-',
						ref: charRefs.current[index],
						code: '',
					};
					setOtpArr(prevState => [...prevState, otpCharObj]);
				});
		}, [code, otpArr]);

		const handleCode = (char, index) => {
			if (char === '' && index !== code.length - 1) {
				return;
			}

			const oldCode = [...code];

			oldCode[index] = char;
			setCode(oldCode);

			if (index < code.length - 1 && char) {
				otpArr[index + 1].ref.current.focus();
			} else if (index === code.length - 1 && char) {
				const jointCode = oldCode.join('');
				verificationCode.current = jointCode;
				handleVerify(jointCode);
			}
		};

		const handleKeyPress = (index, event) => {
			const { key } = event;
			if (key === 'Backspace') {
				if (
					(index > 0 && index !== code.length - 1) ||
					(index === code.length - 1 && code[index] === '')
				) {
					otpArr[index - 1].ref.current.focus();
				}
			}
		};

		const handleFocus = index => {
			const oldCode = [...code];
			if (oldCode[index]) {
				oldCode[index] = '';
				setCode(oldCode);
			}
		};

		return (
			<Screen
				translucent={false}
				horizontalPad
				verticalPad
				barColor={theme.palette.primaryColors.color2}
				barStyle="light-content">
				<Breadcumb
					leftComponent={<BackButton />}
					rightComponent={<View />}
				/>
				<View flex center>
					<Text variant={theme.typo.fontVariants.h2Bold}>
						screens.forgetPasswordCode.title
					</Text>
					<Text
						paddingTop={40}
						textAlign="center"
						variant={theme.typo.fontVariants.xsRegular}>
						screens.forgetPasswordCode.description
					</Text>
					<Text
						textAlign="center"
						color={theme.palette.primaryColors.color2}
						variant={theme.typo.fontVariants.xsBold}>
						{email || phone}
					</Text>
					<View row paddingTop={30}>
						{otpArr.map((item, index) => (
							<TextInput
								key={index.toString()}
								value={code[index]}
								ref={item.ref}
								placeholderTextColor="white"
								style={defaultStyle.passInput}
								placeholder={item.placeholder}
								keyboardType="number-pad"
								underlineColorAndroid="transparent"
								onFocus={() => handleFocus(index)}
								caretHidden
								maxLength={1}
								onChangeText={char => handleCode(char, index)}
								onKeyPress={({ nativeEvent }) => {
									handleKeyPress(index, nativeEvent);
								}}
							/>
						))}
					</View>
					<Text
						variant={theme.typo.fontVariants.smallRegular}
						color={theme.palette.baseColors.base3}
						paddingTop={20}>
						screens.loginVerification.notReceived
					</Text>
					<Text
						onPress={handleResend}
						variant={theme.typo.fontVariants.smallRegular}
						color={theme.palette.primaryColors.color2}>
						screens.loginVerification.resend
					</Text>
				</View>
			</Screen>
		);
	}),
);

const defaultStyle = StyleSheet.create({
	passInput: {
		borderRadius: theme.roundness.small,
		color: theme.palette.baseColors.base1,
		borderColor: theme.palette.baseColors.base4,
		borderStyle: 'solid',
		borderWidth: 1,
		fontSize: theme.typo.fontSize.medium,
		marginRight: 10,
		width: 40,
		justifyContent: 'center',
		alignItems: 'center',
		textAlign: 'center',
		height: 55,
	},
});

export default ForgetPasswordSmsVerificationScreen;
