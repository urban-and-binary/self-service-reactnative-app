import React, { useCallback, useEffect, useState } from 'react';
import { inject, observer } from 'mobx-react';
import { useNavigation, useRoute } from '@react-navigation/native';
import { Breadcumb, Button, Screen, Text, TextInput, View } from '../theme';
import { theme } from '../theme/themeConfig';
import { BreadcumbLanguage } from '../Breadcrumbs/BreadcumbLanguage';
import { useTranslation } from '../translation/useTranslation';
import { hasOneCapitalLetter, hasOneNumber } from '../theme/validationUtil';
import { CheckBox } from '../theme/CheckBox';
import { useResetPassword } from './api';

const ChangePasswordScreen = inject('store')(
	observer(() => {
		const t = useTranslation();
		const { params } = useRoute();
		const { email, phone, verificationCode } = params;
		const [formValue, setFormValue] = useState({});
		const [validations, setValidations] = useState({});
		const navigation = useNavigation();
		const handleGoBack = () =>
			navigation.reset({
				index: 0,
				routes: [{ name: 'login' }],
			});
		const changePassword = useResetPassword({
			onSuccess: () => {
				navigation.reset({
					index: 0,
					routes: [{ name: 'login' }],
				});
			},
		});
		const handleFormValue = (key, value) => {
			setFormValue(prevState => ({ ...prevState, [key]: value }));
		};
		const isValidated = useCallback(
			(shouldUpdateStates = false) => {
				const validationObject = {
					hasCapitalLetter:
						formValue?.newPassword &&
						hasOneCapitalLetter(formValue.newPassword),
					hasNumber:
						formValue?.newPassword &&
						hasOneNumber(formValue.newPassword),
					matched:
						!!formValue.newPassword &&
						formValue?.newPassword === formValue.repeatPassword,
				};
				if (shouldUpdateStates) {
					setValidations(validationObject);
				}

				return Object.values(validationObject).every(item => !!item);
			},
			[formValue],
		);

		useEffect(() => {
			isValidated(true);
		}, [isValidated]);

		const handleChangePassword = () => {
			changePassword.mutate({
				password: formValue.newPassword,
				password1: formValue.repeatPassword,
				verificationCode,
				email: email || undefined,
				phone: phone || undefined,
			});
		};

		return (
			<Screen
				translucent={false}
				horizontalPad
				verticalPad
				barColor={theme.palette.primaryColors.color2}
				barStyle="light-content"
				preset="scroll"
				flex
				contentFlexGrow>
				<Breadcumb
					leftComponent={<View />}
					rightComponent={<BreadcumbLanguage currentLanguage="en" />}
				/>
				<Text paddingTop={30} variant={theme.typo.fontVariants.h2Bold}>
					screens.changePassword.title
				</Text>
				<Text
					paddingVertical={10}
					variant={theme.typo.fontVariants.smallRegular}
					color={theme.palette.baseColors.base3}>
					screens.changePassword.description
				</Text>
				<View paddingTop={30} flex>
					<TextInput
						error={
							!!formValue?.currentPassword &&
							formValue?.currentPassword ===
								formValue?.newPassword &&
							'screens.profile.newPasswordError'
						}
						isRequired
						secureTextEntry
						marginBottom={15}
						value={formValue.newPassword}
						onChangeText={value =>
							handleFormValue('newPassword', value)
						}
						label="screens.profile.newPassword"
						placeholder={t('screens.profile.newPassword')}
					/>
					<TextInput
						isRequired
						secureTextEntry
						marginBottom={20}
						value={formValue?.repeatPassword}
						onChangeText={value =>
							handleFormValue('repeatPassword', value)
						}
						label="screens.profile.retypePassword"
						placeholder={t('screens.profile.newPassword')}
					/>

					<Text
						paddingBottom={10}
						variant={theme.typo.fontVariants.smallRegular}>
						screens.profile.passwordRules
					</Text>
					<CheckBox
						stateless
						initialValue={validations?.hasCapitalLetter}
						label="screens.profile.passwordOneLetter"
					/>
					<CheckBox
						stateless
						initialValue={validations?.hasNumber}
						label="screens.profile.passwordOneNumber"
					/>
					<CheckBox
						stateless
						initialValue={validations?.matched}
						label="screens.profile.passwordsMustMatch"
					/>
					<Button
						onPress={handleChangePassword}
						disabled={!isValidated()}
						marginTop={50}
						label="screens.changePassword.title"
					/>
					<Button
						onPress={handleGoBack}
						outline
						marginTop={20}
						label="general.form.login"
					/>
				</View>
			</Screen>
		);
	}),
);

export default ChangePasswordScreen;
