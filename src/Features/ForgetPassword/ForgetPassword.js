import React, { useState } from 'react';
import { StyleSheet } from 'react-native';
import { inject, observer } from 'mobx-react';
import { useNavigation } from '@react-navigation/native';
import { Breadcumb, Button, Screen, Text, View } from '../theme';
import { theme } from '../theme/themeConfig';
import { BreadcumbLanguage } from '../Breadcrumbs/BreadcumbLanguage';
import { useTranslation } from '../translation/useTranslation';
import { TextInputPhone } from '../theme/TextInputPhone';
import { TextInputEmail } from '../theme/TextInputEmail';
import { isEmailValidated, isPhoneValidated } from '../theme/validationUtil';
import { useForgetPassword } from './api';

const ForgetPasswordScreen = inject('store')(
	observer(() => {
		const t = useTranslation();
		const [email, setEmail] = useState();
		const [phone, setPhone] = useState();
		const navigation = useNavigation();
		const handleGoBack = () =>
			navigation.reset({
				index: 0,
				routes: [{ name: 'login' }],
			});

		const forgetPassword = useForgetPassword({
			onSuccess: () => {
				navigation.navigate('forgetPasswordCode', {
					email,
					phone,
				});
			},
		});
		const handleSubmit = () => {
			forgetPassword.mutate({
				email: email || undefined,
				phone: phone || undefined,
			});
		};

		const handleEmail = value => {
			setPhone(null);
			setEmail(value);
		};

		const handlePhone = value => {
			setEmail(null);
			setPhone(value);
		};

		const isValidated = () =>
			(!!email && isEmailValidated(email)) ||
			(!!phone && isPhoneValidated(phone));

		return (
			<Screen
				translucent={false}
				horizontalPad
				verticalPad
				barColor={theme.palette.primaryColors.color2}
				barStyle="light-content"
				preset="scroll"
				flex
				contentFlexGrow>
				<Breadcumb
					leftComponent={<View />}
					rightComponent={<BreadcumbLanguage currentLanguage="en" />}
				/>
				<Text paddingTop={30} variant={theme.typo.fontVariants.h2Bold}>
					screens.forgetPassword.title
				</Text>
				<Text
					paddingVertical={10}
					variant={theme.typo.fontVariants.smallRegular}
					color={theme.palette.baseColors.base3}>
					screens.forgetPassword.description
				</Text>
				<View paddingTop={30} flex>
					<TextInputEmail
						onChangeText={handleEmail}
						autoComplete="email"
						keyboardType="email-address"
						label="general.form.email"
						value={email}
						placeholder={t('screens.login.emailPlaceholder')}
					/>
					<Text
						paddingTop={25}
						textAlign="center"
						variant={theme.typo.fontVariants.smallBold}>
						general.form.or
					</Text>
					<TextInputPhone
						value={phone}
						onChangeText={handlePhone}
						autoComplete="tel"
						keyboardType="phone-pad"
						label="general.form.phone"
						placeholder="+370"
						containerStyle={defaultStyles.passwordInput}
					/>
					<Button
						onPress={handleSubmit}
						disabled={!isValidated()}
						marginTop={50}
						label="screens.forgetPassword.verificationCode"
					/>
					<Button
						onPress={handleGoBack}
						outline
						marginTop={20}
						label="general.form.login"
					/>
				</View>
			</Screen>
		);
	}),
);

const defaultStyles = StyleSheet.create({
	passwordInput: {
		marginTop: 20,
	},
});

export default ForgetPasswordScreen;
